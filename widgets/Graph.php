<?php

namespace app\widgets;


use Yii;

class Graph
{
    public static function renderGraph($root, $tree, $depth) {
        Yii::$app->layout = 'main';
        return Yii::$app->view->render('@app/views/home/graph', [
            'root' => $root,
            'tree' => $tree,
            'depth' => $depth
        ]);
    }
}
