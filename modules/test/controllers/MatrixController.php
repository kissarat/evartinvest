<?php

namespace app\modules\test\controllers;

use app\helpers\SQL;
use app\models\User;
use app\modules\matrix\models\Node;
use app\modules\test\models\NodeGenerationForm;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * FaqController implements the CRUD actions for Faq model.
 */
class MatrixController extends Controller
{
    /*
    public function actionGenerate() {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/login']);
        }
        $user = User::findOne(['name'=>Yii::$app->user->identity]);
        if(!$user->isManager()){
            return $this->redirect(['/user/view']);
        }
        $model = new NodeGenerationForm();
        $nodes = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->count--;
//            SQL::execute('INSERT INTO generation_log(user_name, type_id, number) VALUES (:user_name, :type_id, :number)', [
//                ':user_name' => $model->first,
//                ':type_id' => $model->type_id,
//                ':number' => $model->count,
//            ]);
            if ($model->first) {
                $first = SQL::queryCell('SELECT id FROM "user" WHERE "name" = :first', [
                    ':first' => $model->first
                ]);
            }
            else {
                $user = User::find()->where('id > 1')->orderBy(['id' => SORT_ASC])->one();
                $first = $user->id;
                $model->first = $user->name;
            }
            $has = Node::find()->where(['user_name' => $model->first])->count() > 0;
            if ($has) {
                $model->count;
            }
            $users = SQL::queryColumn('SELECT u."name"
                FROM "user" u WHERE  u.id >= :first
                ORDER BY u.id LIMIT :limit', [
                ':limit' => $model->count + 1,
                ':first' => $first
            ]);
//            if (!$has) {
//                array_unshift($users, $model->first);
//            }
            $type = $model->getType();
            $nodes = [];
            $price = $type->min;
            ini_set('memory_limit', '768M');
            set_time_limit(0);
            foreach($users as $user_name) {
                $nodes[] = $type->open($user_name,$price);
            }
        }
        if (empty($model->first)) {
            $model->first = SQL::queryCell('SELECT "name" FROM "user" WHERE id > 1');
        }
        return $this->render('generate', [
            'model' => $model,
            'nodes' => $nodes
        ]);
    }
    */
}
