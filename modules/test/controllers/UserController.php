<?php

namespace app\modules\test\controllers;

use app\behaviors\Access;
use app\helpers\God;
use app\helpers\SQL;
use app\models\User;
use app\modules\test\models\UserGenerationForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * FaqController implements the CRUD actions for Faq model.
 */
class UserController extends Controller
{
    /*
    public function behaviors()
    {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'view', 'update', 'cabinet', 'line', 'history', 'visits'],
                'manager' => ['account'],
                'admin' => ['create', 'delete']
            ]
        ];
    }
    
    public function actionGenerate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/login']);
        }
        $user = User::findOne(['name'=>Yii::$app->user->identity]);
        if(!$user->isManager()){
            return $this->redirect(['/user/view']);
        }
        $model = new UserGenerationForm();
        $users = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if (is_numeric($model->count)) {
//                SQL::execute('INSERT INTO generation_log(ref_name, number) VALUES (:user_name, :number)', [
//                    ':user_name' => $model->referrals,
//                    ':number' => $model->count
//                ]);
//            } else if (is_string($model->count)) {
//                SQL::execute('INSERT INTO generation_log(ref_name, user_name) VALUES (:ref_name, :user_name)', [
//                    ':ref_name' => $model->referrals,
//                    ':user_name' => $model->count
//                ]);
//            }
            if($model->referrals == null){
                $model->referrals = 'admin';
            }
            ini_set('memory_limit', '768M');
            set_time_limit(0);
            if ('*' == trim($model->referrals)) {
                $referrals = SQL::queryColumn('SELECT "name" FROM "user"');
            } else {
                $referrals = empty($model->referrals) ? null : explode(',', $model->referrals);
            }
            if (is_numeric($model->count)) {
                $count = Yii::$app->cache->get('user_generation_count');
                if (!$count) {
                    $count = 1;
                }
                $names = [];
                for ($i = 0; $i < $model->count; $i++) {
                    $names[] = 'user' . $count;
                    $count++;
                }
                Yii::$app->cache->set('user_generation_count', $count);
            } else {
                $names = explode(',', $model->count);
                $model->referrals = $model->count;
            }
            if ($referrals) {
                $quoted = [];
                foreach ($referrals as $name) {
                    $quoted[] = "'$name'";
                }
                $quoted = implode(',', $quoted);
                $referrals_found = SQL::queryColumn('SELECT "name" FROM "user" WHERE "name" NOT IN ('
                    . $quoted . ')');
                $names = array_merge($names, array_intersect($referrals, $referrals_found));
            }
            foreach ($names as $name) {
                $name = trim($name);
                $ref_name = empty($referrals) ? null : trim($referrals[array_rand($referrals)]);
                SQL::execute('INSERT INTO "user"("name", email, account, ref_name, hash) VALUES (:name, :email, :account, :ref_name, :hash)', [
                    ':name' => $name,
                    ':email' => $name . '@yopmail.com',
                    ':account' => $model->account,
                    ':ref_name' => $name != $ref_name ? $ref_name : null,
                    ':hash' => password_hash('1', PASSWORD_DEFAULT)
                ]);
                $users[$name] = $ref_name;
                if ('*' == trim($model->referrals)) {
                    $referrals[] = $name;
                }
            }
        }
        return $this->render('generate', [
            'model' => $model,
            'users' => $users
        ]);
    }

    public function actionDeleteAll()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/login']);
        }
        $user = User::findOne(['name'=>Yii::$app->user->identity]);
        if(!$user->isManager()){
            return $this->redirect(['/user/view']);
        }
        SQL::execute("DELETE FROM \"user\"");


        Yii::$app->db->createCommand('INSERT INTO "user"("name", "email", "hash", "status", "ref_name") VALUES
            (\'admin\', \'money@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 1, NULL),
            (\'millionere\', \'money@yopmail.com\', \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\', 3, \'admin\')
            ')->execute();
//        Yii::$app->db->createCommand("SELECT setval('user_id_seq', 4)")->execute();

        SQL::execute("DELETE FROM \"matrix_node\"");

        Yii::$app->db->createCommand('INSERT INTO "matrix_node"("id", "user_name", "type_id", "parent_id", "number", "reinvest_from") VALUES
              (1, \'admin\', 2, NULL, 0, NULL),
              (2, \'millionere\', 2, 1, 0, NULL),
              (3, \'admin\', 2, 1, 1, 1),
              (4, \'admin\', 3, NULL, 0, NULL)
            ')->execute();
        Yii::$app->db->createCommand("SELECT setval('matrix_node_id_seq', 4)")->execute();

        Yii::$app->cache->delete('user_generation_count');
        return $this->redirect('generate');
    }

    public function actionSendEmail() {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/login']);
        }
        $user = User::findOne(['name'=>Yii::$app->user->identity]);
        if(!$user->isManager()){
            return $this->redirect(['/user/view']);
        }
        if ('POST' == $_SERVER['REQUEST_METHOD']) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $params = ['content' => $_POST['text']];
            return God::mail($_POST['email'], 'basic', $params);
        }
        else {
            return $this->render('send-email');
        }
    }
    */
}
