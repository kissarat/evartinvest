<?php

namespace app\modules\test\models;


use app\modules\matrix\models\Type;
use yii\base\Model;

class NodeGenerationForm extends Model {
    public $first;
    public $count;
    public $type_id;
    public $reset = false;

    public function rules() {
        return [
            [['count', 'type_id'], 'required'],
            ['first', 'string']
        ];
    }

    public function getType() {
        return Type::get($this->type_id);
    }

    public function attributeLabels() {
        return [
            'first' => 'Начиная с пользователя',
            'count' => 'Количество',
            'type_id' => 'Пакет',
            'reset' => 'Сбросить',
        ];
    }
}
