<?php

namespace app\modules\feedback\models;


use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * @property integer feedback_id
 * @property string user_name
 * @property boolean answer
 * @property string content
 * @property string time
 *
 * @property UploadedFile[]|string[] images
 * @property Feedback feedback
 */
class Message extends ActiveRecord {
    public $images;

    public static function tableName() {
        return 'feedback_message';
    }

    public function rules() {
        return [
            ['content', 'required'],
            ['content', 'string']
        ];
    }

    public function scenarios() {
        return [
            'default' => ['id', 'feedback_id', 'content'],
            'answer' => ['id', 'feedback_id', 'content', 'answer']
        ];
    }

    public function attributeLabels() {
        return [
            'content' => 'Сообщение',
            'images' => 'Скриншоты',
        ];
    }

    public function __debuginfo() {
        return json_encode([
            'attributes' => $this->getAttributes(),
            'errors' => $this->getErrors()
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    public function upload() {
        if (count($this->images) > 0) {
            $base = $this->getImagesDirectory();
            mkdir($base, 0755, true);
            $i = 1;
            foreach ($this->images as $image) {
                move_uploaded_file($image->tempName, "$base/$i." . $image->getExtension());
                $i++;
            }
        }
    }

    public function getImagesDirectory() {
        return Yii::getAlias('@webroot') . '/feedback/' . $this->feedback->ticket . '/' . $this->id;
    }

    public function getImagesUrl() {
        $base = $this->getImagesDirectory();
        $urls = [];
        if (is_dir($base)) {
            $baseURL = Yii::getAlias('@web') . '/feedback/' . $this->feedback->ticket . '/' . $this->id;
            $dir = opendir($base);
            while($file = readdir($dir)) {
                if ('.' != $file[0]) {
                    $urls[] = $baseURL . '/' . $file;
                }
            }
        }
        return $urls;
    }

    public function getFeedback() {
        return $this->hasOne(Feedback::class, ['id' => 'feedback_id']);
    }
}
