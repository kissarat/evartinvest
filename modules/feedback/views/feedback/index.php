<?php
/**
 * @link http://zenothing.com/
 */

use app\modules\feedback\models\Feedback;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\feedback\models\search\Feedback */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedbacks');
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php
    if (Yii::$app->user->getIsGuest()) {
        if (isset($_COOKIE['ticket'])) {
            echo Html::a(Yii::t('app', 'Last Ticket'), ['ticket', 'ticket' => $_COOKIE['ticket']]);
        }
    }
    else {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'summary'=>'',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['class' => 'id']
                ],
                [
                    'attribute' => 'id',
                    'label' => Yii::t('app', 'User'),
                    'format' => 'html',
                    'value' => function (Feedback $model) {
                        $first = $model->getFirst();
                        return $first->user_name ? $first->user_name : null;
                    }
                ],
                [
                    'attribute' => 'email',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->email) {
                            return Html::a($model->email, 'mailto:' . $model->email);
                        } else {
                            return Yii::t('app', 'registered');
                        }
                    }
                ],

                'subject:ntext',

                [
                    'label' => Yii::t('app', 'Actions'),
                    'format' => 'html',
                    'value' => function (Feedback $model) {
                        return Html::a(Yii::t('app','Просмотр'), ['/user/feedbackticket', 'ticket' => $model->ticket]);
                    }
                ]
            ],
        ]);
    }
    ?>
    <?php
    if (Yii::$app->user->getIsGuest() || !Yii::$app->user->identity->isManager()) {
        echo Html::a(Yii::t('app','Create Ticket'), ['/user/feedbackcreate'], ['class' => 'btn btn-success']);
    }
    ?>
</div>
