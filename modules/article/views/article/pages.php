<?php
/**
 * @link http://zenothing.com/
 */

use app\widgets\Ext;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\article\models\Article */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
?>
<div class="article-index middle">
    <?= Ext::stamp() ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
        echo Html::tag('p', Html::a(Yii::t('app', 'Create'),
            ['create', 'scenario' => 'page'],
            ['class' => 'btn btn-success']
        ));
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'title',
            ['class' => ActionColumn::class]
        ]
    ]);
    ?>

</div>
