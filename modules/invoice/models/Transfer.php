<?php

namespace app\modules\invoice\models;


use app\models\User;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Referral;
use yii\db\ActiveRecord;

/**
 * @property string sender_name
 * @property string receiver_name
 * @property number amount
 * @property string memo
 * @property string time
 * @property string ip
 * @property integer node_id
 *
 * @property Node node
 * @property User sender
 * @property User receiver
 * @property Referral referral
 * @property Matrix matrix
 */
class Transfer extends ActiveRecord {

    public static function tableName() {
        return 'transfer_journal';
    }

    public function rules() {
        return [
            [['receiver_name', 'amount'], 'required'],
            ['receiver_name', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'name',
                'message' => 'Пользователь не существует'],
            ['amount', 'integer', 'min' => 1],
            ['memo', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'sender_name' => 'Отправитель',
            'receiver_name' => 'Получатель',
            'amount' => 'Сумма',
            'memo' => 'Заметка',
            'summary' => 'Заметка',
            'time' => 'Время',
            'ip' => 'IP',
            'node_id' => 'ID матрицы'
        ];
    }

    public function scenarios() {
        return [
            'default' => ['sender_name', 'receiver_name', 'amount', 'memo']
        ];
    }

    public function __toString() {
        if ($this->memo) {
            return $this->getSummary();
        }
        else {
            return "€$this->amount $this->sender_name -> $this->receiver_name";
        }
    }

    public function getSummary() {
        return $this->memo && strlen($this->memo) > 40 ? substr($this->memo, 0, 40) . '…' : $this->memo;
    }

    public function getNode() {
        return $this->hasOne(Node::class, ['id' => 'node_id']);
    }

    public function getSender() {
        return $this->hasOne(User::class, ['name' => 'sender_name']);
    }

    public function getReceiver() {
        return $this->hasOne(User::class, ['name' => 'receiver_name']);
    }

    public function getReferral() {
        return $this->hasOne(Referral::class, [
            'id' => 'node_id',
            'root' => 'sender_name'
        ]);
    }

    public function getMatrix() {
        return $this->hasOne(Matrix::class, [
            'id' => 'node_id',
            'root' => 'receiver_name'
        ]);
    }
}
