<?php

namespace app\modules\invoice\models;


use app\helpers\God;
use app\models\User;
use Yii;

/**
 * Class KitlessRegistration
 * @property int applicationId
 * @package app\modules\invoice\models
 */
class KitlessRegistration extends User
{
    public $agree = false;
    public $idImageDoc1;
    public $politican = false;

    public function getRequiredAddInfoAttributes()
    {
        return [
            'nationality', 'city', 'zipCode', 'address1',
            'passportType', 'idNumber', 'activityAndIncome',
            'businessType', 'accountUsage', 'openingPurpose',
            'monthlyTurnOver', 'firstTopUp', 'firstTopUpConnection',
            'otherTopUp', 'otherTopUpConnection', 'phoneCommunicationPass',
            'politican', 'deliveryInformation', 'applicationId'
//            'idImageDoc1',
        ];
    }

    public function getRequiredModelRegistrationAttributes()
    {
        return ['birthday', 'cellPhoneCountry', 'cellPhone', 'phoneCommunicationPass', 'communicationLanguage'];
    }

    public function getRequiredAttributes() {
        return array_merge(
            $this->getRequiredModelRegistrationAttributes(),
            $this->getRequiredAddInfoAttributes()
        );
    }

    public function &getItems() {
        $items = God::loadObject('@app/web/data/wallet.json');
        $result = ['nationality' => God::getCountriesNames()];
        foreach($items as $name1 => $value1) {
            foreach($value1 as $name2 => $value2) {
                $result[$name1][$name2] = Yii::t('app', $value2);
            }    
        }
        return $result;
    }

    public function getDefaults()
    {
        return [
            'nationality' => 'RU',
            'communicationLanguage' => 'RU',
            'passportType' => 'PP',
            'activityAndIncome' => 'SE',
            'businessType' => 'FI',
            'accountUsage' => 'PERSONAL',
            'openingPurpose' => 'IC',
            'monthlyTurnOver' => '10',
            'firstTopUp' => 'MY',
            'firstTopUpConnection' => 'CU',
            'otherTopUp' => 'MY',
            'otherTopUpConnection' => 'CU',
            'deliveryInformation' => 'BR',
        ];
    }
    
    public function initDefaults() {
        foreach ($this->getDefaults() as $name => $value) {
            if (empty($this->$name)) {
                $this->$name = $value;
            }
        }
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['wallet'] = $this->getRequiredAttributes();
        return $scenarios;
    }

    public function rules()
    {
        $defaults = [array_keys($this->getDefaults()), 'default',
            'value' => function (KitlessRegistration $model, $attribute) {
                $defaults = $model->getDefaults();
                return isset($defaults[$attribute]) ? $defaults[$attribute] : null;
            }];

        $required = $this->getRequiredAttributes();
        $rules = array_merge(parent::rules(), [
            [$required, 'required'],
            [$required, 'trim'],
            $defaults
        ]);

        foreach ($this->getItems() as $name => $options) {
            $rules[] = [$name, 'in', 'range' => array_keys($options)];
        }

        return $rules;
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'applicationId' => Yii::t('app', 'Evart ID'),
            'communicationLanguage' => Yii::t('app', 'Communication Language'),
            'nationality' => Yii::t('app', 'Nationality'),
            'address1' => Yii::t('app', 'First Address'),
            'address2' => Yii::t('app', 'Second Address'),
            'zipCode' => Yii::t('app', 'Zip Code'),
            'passportType' => Yii::t('app', 'Passport Type'),
            'idNumber' => Yii::t('app', 'Passport Number'),
            'idImageDoc1' => Yii::t('app', 'Passport Image'),
            'activityAndIncome' => Yii::t('app', 'Activity And Income'),
            'businessType' => Yii::t('app', 'Business Type'),
            'accountUsage' => Yii::t('app', 'Account Usage'),
            'openingPurpose' => Yii::t('app', 'Opening Purpose'),
            'monthlyTurnOver' => Yii::t('app', 'Monthly Turn Over'),
            'firstTopUp' => Yii::t('app', 'First Top Up'),
            'firstTopUpConnection' => Yii::t('app', 'First Top Up Connection'),
            'otherTopUp' => Yii::t('app', 'First Top Up'),
            'otherTopUpConnection' => Yii::t('app', 'First Top Up Connection'),
            'deliveryInformation' => Yii::t('app', 'Delivery Method'),
            'phoneCommunicationPass' => Yii::t('app', 'Phone password (for payment system)')
        ]);
    }

    public function getRegistrationData()
    {
        $data = [
            'firstName' => $this->forename,
            'lastName' => $this->surname,
            'email' => $this->email,
            'dateOfBirth' => str_replace('-', '', $this->birthday),
//            'addressLine1' => $this->address1,
            'communicationLanguage' => $this->communicationLanguage,
//            'postCountry' => $this->country,
//            'postCity' => $this->city,
            'ipAddress' => '188.163.32.222',
            'phoneCommunicationPass' => $this->phoneCommunicationPass,
            'agreeToTermsAndConditions1' => true
        ];

        $codes = God::getCountriesField('code');
        sort($codes, SORT_DESC);
        foreach ($codes as $code) {
            if (strpos($this->phone, $code)) {
                $data['cellPhoneCountry'] = (string)$code;
                $data['cellPhone'] = substr($this->phone, strlen($data['cellPhoneCountry']));
            }
        }

        $this->assignAddresses($data);

        return $data;
    }

    public function getAddInfoData()
    {
        $data = [
            'companyInLatvia' => false,
            'politican' => false,
            'ipAddress' => '188.163.32.222',
            'applicationId' => $this->applicationId
        ];

        $this->assignAddresses($data);

        foreach ($this->getRequiredAddInfoAttributes() as $name) {
            $data[$name] = $this->$name;
        }

        return $data;
    }

    public function assignAddresses(&$data)
    {
        if ($this->hasAddress()) {
            if (!empty($this->address1)) {
                $data['address1'] = $this->address1;
                if (!empty($this->address2)) {
                    $data['address2'] = $this->address2;
                }
            }
        }
    }
    
    public function setCellPhoneCountry($value) {
//        throw new \Exception($value);
    }

    public function setCellPhone($value) {
//        throw new \Exception($value);
    }
}
