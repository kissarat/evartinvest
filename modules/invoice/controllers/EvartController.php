<?php
/**
 * @author Taras Labiak <kissarat@gmail.com>
 */

namespace app\modules\invoice\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\http\Request;
use app\models\Settings;
use app\models\User;
use app\modules\invoice\models\KitlessRegistration;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class EvartController extends Controller
{
    private $_client;
    const SESSION_ID = 'EVART_SESSION_ID';
    const SESSION_TIMEOUT = 120;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
//                    'success' => ['post'],
//                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
//                'plain' => [],
//                'manager' => []
            ],

//            'no_csrf' => [
//                'class' => NoTokenValidation::class,
//                'only' => ['success', 'fail'],
//            ]
        ];
    }

    public function getClient()
    {
        if (!$this->_client) {
            $this->_client = new Client([
                'baseUrl' => 'http://89.111.51.250:8090/fsc-integration',
                'requestConfig' => [
                    'class' => Request::className()
                ]
            ]);
        }
        return $this->_client;
    }

    /**
     * @param $url
     * @param $data
     * @param array $headers
     * @return \yii\httpclient\Request
     */
    private function createRequest($url, $data, $headers = [])
    {
        $headers['accept'] = 'application/json';
        $request = $this->getClient()->createRequest()
            ->setMethod($data ? 'POST' : 'GET')
            ->setUrl($url)
            ->addHeaders($headers);
        if ($data) {
            $request->setData($data);
        }
        return $request;
    }

    public function authorize()
    {
        $response = $this->createRequest('login', [
            'username' => Settings::get('payment', 'login'),
            'password' => Settings::get('payment', 'password')
        ])
            ->send();

        $sid = $response->getCookies();
        $sid = $sid['JSESSIONID']->value;
        Yii::$app->cache->set(static::SESSION_ID, $sid, static::SESSION_TIMEOUT);
        return $sid;
    }

    private function request($url, $data = null, $send = true)
    {
//        $sid = Yii::$app->cache->get(static::SESSION_ID);
//        if (!$sid) {
        $sid = $this->authorize();
//        }
        $request = $this->createRequest($url, $data)
            ->setCookies([
                [
                    'name' => 'JSESSIONID',
                    'value' => $sid
                ]
            ]);
        return $send ? $request->send() : $request;
    }

    private function appRequest($url, $data = [])
    {
//        $user = User::findOne(['name' => Yii::$app->user->identity->name]);
//        throw new \Exception(json_encode($user->attributes));
        $data['applicationId'] = Yii::$app->user->identity->applicationId;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = $this->request('counterpartyapi/' . $url, $data);
        return json_decode($response->content);
    }

    public function actionPing()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = $this->request('counterpartyapi/kitlessregistration', []);
        return $request->content;
    }

    public function setupAuth()
    {
        $_POST['applicationId'] = (string)Yii::$app->user->identity->appliactionId;
        return $_POST;
    }

    public function actionRegister()
    {
        $this->layout = false;
        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $ip = strpos($ip, ":") === false ? $ip : '127.0.0.1';
            return $this->render('register', ['ip' => $ip]);
        } else {
            $request = $_POST;
            $request['dateOfBirth'] = date('Ymd', strtotime($request['dateOfBirth']));
            foreach (['postAddressExist', 'agreeToTermsAndConditions1'] as $name) {
                $request[$name] = isset($request[$name]) && $request[$name];
            }
            $response = $this->request('counterpartyapi/kitlessregistration', $request);
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = json_decode($response->content);
            if ($data) {
                $save = false;
                if ($data->success) {
                    $user = Yii::$app->user->identity;
                    $user->app_id = $data->registrationId;
                    $save = $user->save(true, ['app_id']);
                }
                return [
                    'save' => $save,
                    'response' => $data
                ];
            } else {
                return $this->dumpResponse($response);
            }
        }
    }

    public function actionAddInfo()
    {
        $this->layout = false;
        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            return $this->render('addinfo');
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = $this->setupAuth();
            foreach (['CompanyInLatvia', 'Politican'] as $name) {
                $request[$name] = isset($request[$name]) && $request[$name];
            }
            $has_files = !empty($_FILES);
            $response = $this->request('counterpartyapi/addinfo', $request, !$has_files);
            foreach ($_FILES as $name => $file) {
                $response->addFile($name, $file['tmp_name']);
            }
            if ($has_files) {
                $response = $response->send();
            }
            $data = json_decode($response->content);
            if ($data) {
                return $data;
            } else {
                return $this->dumpResponse($data);
            }
        }
    }

    public function actionAddDocument()
    {
        $this->layout = false;
        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            return $this->render('adddocument');
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = $this->setupAuth();
            $has_files = !empty($_FILES);
            $response = $this->request('counterpartyapi/adddocument', $request, !$has_files);
            foreach ($_FILES as $name => $file) {
                $response->addFile($name, $file['tmp_name']);
            }
            if ($has_files) {
                $response = $response->send();
            }
            $data = json_decode($response->content);
            if ($data) {
                return $data;
            } else {
                return $this->dumpResponse($data);
            }
        }
    }

    public function actionState()
    {
        return $this->appRequest('checkappstate');
    }

    public function actionKit()
    {
        return $this->appRequest('addkit', [
            'kit' => Url::to(['/invoice/evart/kit', 'name' => Yii::$app->user->identity->name], true)
        ]);
    }

    public function actionBalance()
    {
        return $this->appRequest('balancerequest');
    }

    public function actionVerify()
    {
        return $this->appRequest('assignapplicationfordocumentcheck');
    }

    public function actionCheckDocument($date)
    {
        return $this->appRequest('checkdocument', [
            'idDocumentValidityPeriod' => $date,
            'description' => "$date " . time()
        ]);
    }

    public function actionChangeState($state)
    {
        return $this->appRequest('statusChange', [
            'state' => $state,
            'comment' => "$state " . time()
        ]);
    }

    public function actionMerchant()
    {
        $this->layout = false;
        $attributes = Yii::$app->evart->createRequestAmount();
        return $this->render('merchant', [
            'attributes' => $attributes
        ]);
    }

    private function dumpResponse($response)
    {
        return [
            'response' => [
                'code' => $response->statusCode,
                'cookies' => $response->cookies,
                'content' => $response->content,
                'headers' => $response->headers,
            ]
        ];
    }

    /**
     * @param string $name
     * @return KitlessRegistration
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if (($model = KitlessRegistration::findOne(['name' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreate($name = null)
    {
        Yii::$app->layout = 'cabinet';
        $model = $this->findMe($name);
        $model->scenario = 'wallet';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = UploadedFile::getInstance($model, 'idImageDoc1');
            if ($file) {
                $mime = mime_content_type($file->tempName);
                if (strpos($mime, 'image/') !== false) {
                    $filename = Yii::getAlias('@app/private/') . $model->name . '-passport';
                    $filename .= '-' . $model->idImageDoc1->name;
                    $file->saveAs($filename);
                    $model->idImageDoc1 = $filename;
                    if ($model->save(true)) {
                        return $this->redirect(['/user/view', 'name' => $name]);
                    }
                } else {
                    $model->addError('idImageDoc1', Yii::t('app', 'Invalid image format'));
                }
            }
            else if ($model->save(true)) {
                return $this->redirect(['/user/view', 'name' => $name]);
            }
        }
        $model->initDefaults();
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionProcess($name = null, $step = null)
    {
        Yii::$app->layout = 'cabinet';
        $model = $this->findMe($name);
        $model->scenario = 'wallet';
        if ($step) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            switch ($step) {
                case 'register':
                    if ($model->applicationId) {
                        $response = [
                            'success' => false,
                            'registrationId' => (int)$model->applicationId,
                            'errorDescription' => Yii::t('app', 'User already registered')
                        ];
                    } else {
                        $data = $model->getRegistrationData();
                        $response = $this->request('counterpartyapi/kitlessregistration', $data);
                        $response = json_decode($response->content, true);
                        $response['ipAddress'] = $data['ipAddress'];
                        if ($response['success']) {
                            $conditions = [
                                ':id' => (int) $response['registrationId'],
                                ':name' => $model->name
                            ];
                            if (0 >= SQL::execute('UPDATE "user" SET "applicationId" = :id WHERE "name" = :name', $conditions)) {
                                $response['errorDescription'] = Yii::t('app', 'Cannot save user');
                                $response['errors'] = $model->getErrors();
                            }
                        }
                    }
                    break;
                case 'addinfo':
                    $data = $model->getAddInfoData();
                    $data['address1'] = 'Ternopil';
                    $request = $this->request('counterpartyapi/addinfo', $data, false);
                    if ($model->idImageDoc1) {
                        $request->addFile('idImageDoc1', $model->idImageDoc1);
                    }
                    $response = json_decode($request->send()->content, true);
                    break;
                default:
                    $response = [
                        'success' => false,
                        'errorDescription' => Yii::t('app', 'Unknown step')
                    ];
                    break;
            }
            return $response;
        }
        return $this->render('process', [
            'model' => $model
        ]);
    }

    /**
     * @param null $name
     * @return KitlessRegistration
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function findMe($name = null)
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->isAdmin() && $name && $name != $user->name) {
            throw new ForbiddenHttpException();
        }
        return $this->findModel($name);
    }
}
