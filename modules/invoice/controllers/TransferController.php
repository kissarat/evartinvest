<?php

namespace app\modules\invoice\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\modules\invoice\models\Transfer;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class TransferController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'create']
            ]
        ];
    }

    public function actionIndex($mode = null, $user = null) {
        $identity = Yii::$app->user->identity;
        if (!$identity->isManager() && $user != $identity->name) {
            Yii::$app->session->addFlash('error', 'Вы можете видеть только свои переводы');
            return $this->redirect(['index', 'user' => $identity->name]);
        }

        return $this->render('index', static::index($mode, $user));
    }

    public static function index($mode = null, $user = null)  {
        $user = Yii::$app->user->identity;
        $query = Transfer::find();
        $query->andWhere('income' == $mode ? 'node_id IS NOT NULL' : 'node_id IS NULL');
        if ($user) {
            if ('income' == $mode) {
                $query
                    ->with('node')
                    ->with('referral')
                    ->andWhere('receiver_name = :name', [
                    ':name' => $user
                ]);
            } else {
                $query->andWhere('receiver_name = :name', [
                    ':name' => $user
                ]);
            }
        }

        return [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_DESC,
                        'id' => SORT_DESC
                    ]
                ]
            ]),
            'mode' => $mode,
            'user' => $user
        ];
    }

    public function actionCreate() {
        $model = new Transfer();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sender_name = Yii::$app->user->identity->name;
            if ($model->sender->account >= $model->amount) {
                $transaction = Yii::$app->db->beginTransaction();
                SQL::execute('SELECT transfer(:sender_name, :receiver_name, :amount, :memo, :ip, null, true)', [
                    ':sender_name' => $model->sender_name,
                    ':receiver_name' => $model->receiver_name,
                    ':amount' => $model->amount,
                    ':memo' => $model->memo,
                    ':ip' => $_SERVER['REMOTE_ADDR']
                ]);
                $transaction->commit();
                Yii::$app->session->addFlash('success',
                    "Пользователю $model->receiver_name начислено €$model->amount");
                return $this->redirect(['index', 'user' => $model->sender_name]);
            }
            else {
                $model->addError('amount', 'На вашем счету доступно €' . $model->sender->account);
            }
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }
}
