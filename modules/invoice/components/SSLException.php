<?php

namespace app\modules\invoice\components;


use yii\base\Exception;

class SSLException extends Exception {
    public function __construct() {
        parent::__construct(openssl_error_string());
    }
}
