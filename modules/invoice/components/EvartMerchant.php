<?php
namespace app\modules\invoice\components;
use app\models\User;
use SimpleXMLElement;
use Yii;
use yii\base\Component;

/**
 * @property $id int
 * @property $publicKey string
 * @property $privateKey string
 * @property $sslConfig array
 */
class EvartMerchant extends Component
{
    public $id;
    private static $_ssl;

    private static function &getSslConfig()
    {
        if (!static::$_ssl) {
            static::$_ssl = Yii::getAlias('@app/console/ssl.php');
        }
        return static::$_ssl;
    }

    private function getPrivateKey()
    {
        return self::getSslConfig()['keys']['private'];
    }

    private function getPublicKey()
    {
        return self::getSslConfig()['keys']['public'];
    }

    private function sign($data)
    {
        if (openssl_sign($data, $signed, $this->privateKey)) {
            return $signed;
        }
        throw new SSLException();
    }

    private function verify($data, $signed)
    {
        return openssl_verify($data, $signed, $this->publicKey);
    }

    private function encrypt($data)
    {
        if (openssl_seal($data, $encrypted, $keys, [$this->publicKey])) {
            return [
                'encrypted' => $encrypted,
                'keys' => $keys
            ];
        }
        throw new SSLException();
    }

    private function decrypt($encrypted, $key) {
        if (openssl_open($encrypted, $data, $key, $this->privateKey)) {
            return $data;
        }
        throw new SSLException();
    }

    private static function toMarkup(SimpleXMLElement $container, array $array) {
        foreach ($array as $name => $value) {
            if (is_array($value)) {
                static::toMarkup($container->addChild($name), $value);
            }
            else {
                $container->addChild($name, $value);
            }
        }
        return $container;
    }

    private function fromInput($input) {
        foreach ($input as $key => $value) {
            $input[$key] = base64_decode($input[$key]);
        }

        $data = $this->decrypt($input['REQUEST_DATA'], $input['KEY']);
        return $this->verify($data, $input['SIGNATURE'])
            ? simplexml_load_string($data)
            : false;
    }

    private function toOutput($output) {
        $xml = $this->toMarkup(new SimpleXMLElement('<data />'), $output);
        $signature = $this->sign($xml);
        $encrypted_info = $this->encrypt($xml);
        return [
            'KEY' => $encrypted_info['keys'][0],
            'SIGNATURE' => base64_encode($signature),
            'REQUEST_DATA' => base64_encode($encrypted_info['data'])
        ];
    }

    public static function getMerchantUrl() {
        return 'http://evartinvest.com/merchant';
    }

    /**
     * @param array $array array with keys of amount and [orderId]
     * @return array post data
     */
    public function createRequestData(array $array) {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $array['merchantId'] = $this->id;
        $array['transactionId'] = uuid_create();
        $array['currency'] = 'EUR';
        $array['successUrl'] = static::getMerchantUrl();
        if (empty($array['orderId'])) {
            $array['orderId'] = uuid_create();
        }
        if (empty($array['orderDescription'])) {
            $oid = $array['orderId'];
            $tid = $array['transactionId'];
            $array['orderDescription'] = "Transaction #$tid for order $oid";
        }
        $array['customerEmail'] = $user->email;
        $array['firstName'] = $user->forename;
        $array['lastName'] = $user->surname;
        $array['phone'] = $user->phone;
        $array['birthday'] = date('d/m/Y', $user->birth);
//        if (isset($array['amount']) && !is_string($array['amount'])) {
//            $array['amount'] = (string) $array['amount'];
//        }
        return $array;
    }

    /**
     * @param $amount float
     * @return array
     */
    public function createRequestAmount($amount = 0.0) {
        return $this->createRequestData(['amount' => (string) $amount]);
    }
}
