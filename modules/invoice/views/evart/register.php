<!DOCTYPE html>
<html>
<head>
    <?php include '_head.php' ?>
</head>
<body>
<form method="post">
    <fieldset>
        <legend>Registration</legend>
        <input name="firstName" required/>
        <input name="lastName" required/>
        <input name="email" type="email" maxlength="255" required/>
        <input name="dateOfBirth" type="date" required/>
        <select name="communicationLanguage" required>
            <option value="RU">Russian</option>
            <option value="EN">English</option>
            <option value="LV">Latvian</option>
        </select>
        <input name="postAddressExist" type="checkbox"/>
        <input name="postAddress1" maxlength="1000" disabled/>
        <input name="postAddress2" maxlength="1000" disabled/>
        <input name="postZipCode" pattern="^\d{5}(?:[-\s]\d{4})?$"/>
        <select name="postCountry">
            <option value="RU">Russia</option>
            <option value="US">United States</option>
            <option value="LV">Latvia</option>
        </select>
        <input name="cellPhoneCountry" pattern="^\d{1,6}$" required/>
        <input name="cellPhone" pattern="^\d{6,30}$" required/>
        <input name="phoneCommunicationPass" pattern="^\w{8,255}$" required/>
        <input name="ipAddress" value="<?= $ip ?>" required/>
        <input name="agreeToTermsAndConditions1" type="checkbox" checked required/>
    </fieldset>
    <button type="submit">Send</button>
</form>
</body>
</html>
