<?php
/** @var $attributes array */
use yii\helpers\Html;

?>
<html>
<head>
    <?php include '_head.php' ?>
</head>
<body>
<form action="http://89.111.51.250:8088/payments-new/fsc/merchant/process">
    <fieldset>
        <legend>Merchant</legend>
        <?php
        foreach ($attributes as $name => $value) {
            echo Html::input('text', $name, $value);
        }
        ?>
    </fieldset>
    <button type="submit">Pay</button>
</form>
</body>
</html>
