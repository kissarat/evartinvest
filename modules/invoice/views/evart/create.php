<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\modules\invoice\models\KitlessRegistration $model */

$items = $model->getItems();
?>
<div class="evart-create">
    <?php
    $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]);

    foreach ($model->getErrors() as $name => $message) {
        echo Html::tag('div', $name . ': ' . implode(' ', $message) . ' ## ' . $model->getCellPhoneCountry());
    }

    $select = function ($name) use ($model, $form, $items) {
        return $form->field($model, $name)->dropDownList($items[$name]);
    }
    ?>
    <?= $form->field($model, 'birthday')->input('date') ?>
    <?= $form->field($model, 'phoneCommunicationPass') ?>
    <?= $select('communicationLanguage') ?>
    <?= $select('nationality') ?>
    <?= $form->field($model, 'city') ?>
    <?= $form->field($model, 'zipCode') ?>
    <?= $form->field($model, 'address1') ?>
    <?= $select('passportType') ?>
    <?= $form->field($model, 'idNumber') ?>
    <?= $form->field($model, 'idImageDoc1')->fileInput() ?>
    <?= $select('activityAndIncome') ?>
    <?= $select('businessType') ?>
    <?= $select('accountUsage') ?>
    <?= $select('openingPurpose') ?>
    <?= $select('monthlyTurnOver') ?>
    <?= $select('firstTopUp') ?>
    <?= $select('firstTopUpConnection') ?>
    <?= $select('otherTopUp') ?>
    <?= $select('otherTopUpConnection') ?>
    <?= $form->field($model, 'politican')->hiddenInput(['value' => 0]) ?>
    <?= $select('deliveryInformation') ?>
    <?= Html::submitButton(Yii::t('app', 'Save')) ?>
    <?php ActiveForm::end() ?>
</div>
