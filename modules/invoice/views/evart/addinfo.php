<!DOCTYPE html>
<html>
<head>
    <?php include '_head.php' ?>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Add Info</legend>
        <select name="nationality" class="country" required></select>
        <input name="address1" maxlength="1000" required/>
        <input name="address2" maxlength="1000" required/>
        <input name="zipCode" minlength="3" maxlength="16" required/>
        <input name="city" maxlength="200" required/>
        <select name="passportType" required>
            <option value="ID">ID card</option>
            <option value="PP">Passport</option>
        </select>
        <input name="idNumber" required/>
        <input name="idImageDoc1" type="file" multiple required/>
        <select name="activityAndIncome">
            <option value="PE">Paid employee</option>
            <option value="EN">HouseOwner/HouseWife</option>
            <option value="SE">Self-employed</option>
            <option value="RE">Retried</option>
            <option value="ST">Student</option>
            <option value="UE">Unemployed</option>
        </select>
        <select name="businessType" required>
            <option value="ED">Education</option>
            <option value="MM">Manufacturing, mining, extraction of raw materials</option>
            <option value="EG">Electricity, gas, and heating, ventilation systems</option>
            <option value="WS">Water supply and wastewater treatment systems and recycling</option>
            <option value="CI">Construction industry</option>
            <option value="RW">Retail and wholesale trade</option>
            <option value="VR">Vehicle repair</option>
            <option value="TS">Transportation and storage of goods</option>
            <option value="HC">Hotels and catering</option>
            <option value="TT">Telecommunications, IT services</option>
            <option value="SL">Sale / lease of property</option>
            <option value="FI">Financial services and insurance services</option>
            <option value="HS">Health and social care</option>
            <option value="AR">Arts, recreation and entertainment</option>
            <option value="AS">Administrative Services</option>
            <option value="PA">Public administration and the army, social insurance</option>
            <option value="ST">Scientific and Technical Services</option>
            <option value="OT">Other</option>
        </select>
        <select name="accountUsage" required>
            <option value="PERSONAL">Personal usage</option>
            <option value="BUSINESS">Business usage</option>
        </select>
        <select name="openingPurpose" required>
            <option value="SA">Salary</option>
            <option value="CR">Income from customer referral</option>
            <option value="SG">Income from the sale of gold</option>
            <option value="IC">Income from business activities</option>
            <option value="OT">Other</option>
        </select>
        <select name="monthlyTurnOver" required>
            <option value="10">1 EUR - 5000 EUR</option>
            <option value="11">5001 EUR - 15000 EUR</option>
            <option value="20">15000 EUR - 100000 EUR</option>
            <option value="30">100000 EUR - 200000 EUR</option>
            <option value="40">more than 200000 EUR</option>
        </select>
        <select name="firstTopUp" required>
            <option value="MY">My personal account</option>
            <option value="AA">From account of another person</option>
            <option value="CC">With my CreditCard</option>
            <option value="CA">With CreditCard of another person</option>
            <option value="WM">Via WesternUnion by myself</option>
            <option value="WA">Via WesternUnion by another person</option>
        </select>
        <select name="firstTopUpConnection" required>
            <option value="FA">Family</option>
            <option value="FR">Friend</option>
            <option value="EM">Employer</option>
            <option value="CU">Customer</option>
            <option value="GT">Gold Trader</option>
        </select>
        <select name="otherTopUp" required>
            <option value="MY">My personal account</option>
            <option value="AA">From account of another person</option>
            <option value="CC">With my CreditCard</option>
            <option value="CA">With CreditCard of another person</option>
            <option value="WM">Via WesternUnion by myself</option>
            <option value="WA">Via WesternUnion by another person</option>
        </select>
        <select name="otherTopUpConnection" required>
            <option value="FA">Family</option>
            <option value="FR">Friend</option>
            <option value="EM">Employer</option>
            <option value="CU">Customer</option>
            <option value="GT">Gold Trader</option>
        </select>
        <input name="companyInLatvia" type="checkbox" />
        <input name="companyInLatviaDoc3" type="file" multiple disabled/>
        <input name="politican" type="checkbox" />
        <select name="deliveryInformation" required>
            <option value="BP">By post</option>
            <option value="BR">By registered email</option>
            <option value="BC">By courier</option>
            <option value="RB">Receive in office</option>
        </select>
        <input name="ipAddress" pattern="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$" required
    </fieldset>
    <button type="submit">Send</button>
</form>
</body>
</html>
