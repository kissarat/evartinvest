<?php
use yii\helpers\Html;

/** @var \app\modules\invoice\models\KitlessRegistration $model */
?>
<div class="evart-process">
    <?= Html::tag('noscript', Yii::t('app', 'JavaScript is disabled')) ?>
    <?= Html::tag('div', Yii::t('app', 'Registration'), [
        'class' => 'registration',
        'style' => 'display: none'
    ]) ?>
    <?= Html::tag('div', Yii::t('app', 'Your registration ID is'), [
        'class' => 'registrationID',
        'style' => 'display: none'
    ]) ?>
    <?= Html::tag('div', Yii::t('app', 'Add Info'), [
        'class' => 'addinfo',
        'style' => 'display: none'
    ]) ?>
    <?= Html::tag('div', Yii::t('app', 'Complete'), [
        'class' => 'complete',
        'style' => 'display: none'
    ]) ?>
    <div class="error"></div>
</div>
<?php
echo Html::script("var evart_user_name = '$model->name';\n"
    . file_get_contents(Yii::getAlias('@app/web/payment-system.js')));
