<style>
    fieldset {
        display: table;
    }

    fieldset > div {
        display: table-row;
    }

    fieldset > div > * {
        display: table-cell;
    }
</style>

<script src="http://underscorejs.org/underscore-min.js"></script>
<script>
    var data = <?= json_encode($_POST) ?>;

    function fields(call) {
        _.each(document.querySelectorAll('[name]'), call);
    }

    addEventListener('load', function () {
        if (!data.length && localStorage[location.pathname]) {
            data = JSON.parse(localStorage[location.pathname]);
            fields(function (input) {
                if ('file' != input.type) {
                    var value = data[input.getAttribute('name')];
                    if (value) {
                        input.value = value;
                    }
                }
            })
        }

        fields(function (input) {
            var name = input.getAttribute('name');
            name = name[0].toUpperCase() + name.slice(1);
            var div = document.createElement('div');
            var label = document.createElement('label');
            label.innerHTML = name;
            if (name in data) {
                input.value = data[name];
            }
            input.parentNode.replaceChild(div, input);
            div.appendChild(label);
            div.appendChild(input);
        });

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/countries.json');
        xhr.onload = function () {
            countries = JSON.parse(xhr.responseText);
            _.each(document.querySelectorAll('.country'), function (input) {
                _.each(countries, function (country) {
                    var option = document.createElement('option');
                    option.value = country.iso;
                    option.innerHTML = country.name;
                    input.appendChild(option);
                })
            })
        };
        xhr.send(null);
    });

    addEventListener('unload', function () {
        data = {};
        fields(function (input) {
            var value = input.value.trim();
            if (value) {
                data[input.getAttribute('name')] = value;
            }
        });

        localStorage[location.pathname] = JSON.stringify(data);
    });

    var countries = [];
</script>
