<?php
use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="transfer-create">
    <h1><?= $this->title ?></h1>
    <?php
    $form = ActiveForm::begin();
    echo implode("\n", [
        $form->field($model, 'receiver_name')->widget(AjaxComplete::class, [
            'route' => ['/user/complete']
        ]),
        $form->field($model, 'amount'),
        $form->field($model, 'memo')->textarea(),
        Html::submitButton('Перевести', ['class' => 'btn btn-primary'])
    ]);
    ActiveForm::end();
    ?>
</div>
