<?php
use app\models\User;
use app\modules\invoice\models\Transfer;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'income' == $mode ? 'Прибыль' : 'Переводы';

if ($user) {
    if ('income' == $mode) {
        $columns = [
//            'id',
//            [
//                'attribute' => 'node_id',
//                'label' => 'ID матрицы'
//            ],
            [
                'attribute' => 'node_id',
                'label' => 'Пакет',
                'value' => function(Transfer $transfer) {
                    return $transfer->node ? $transfer->node->type->name : null;
                }
            ],
//            [
//                'attribute' => 'node_id',
//                'label' => 'Уровень',
//                'value' => function(Transfer $transfer) {
//                    return $transfer->matrix ? $transfer->matrix->level : null;
//                }
//            ],
//            [
//                'attribute' => 'sender_name',
//                'label' => 'Пользователь'
//            ],
            [
                'attribute' => 'amount',
                'label' => 'Прибыль'
            ],
            'time:datetime'
        ];
    }
    else {
        $columns = [
            'id',
            'sender_name',
            'receiver_name',
            'amount',
            'memo',
            'time:datetime'
        ];
        $this->title .= ' пользователя ' . $user;
    }
}
else {
    $columns = [
        [
            'attribute' => 'sender_name',
            'format' => 'html',
            'value' => function(Transfer $transfer) use ($mode) {
                return Html::a($transfer->sender_name, [
                    '/user/view', 'name' => $transfer->sender_name]);
            }
        ],
        [
            'attribute' => 'receiver_name',
            'format' => 'html',
            'value' => function(Transfer $transfer) use ($mode) {
                return Html::a($transfer->receiver_name, [
                    'index', 'user' => $transfer->receiver_name, 'mode' => $mode]);
            }
        ],
        'amount',
        'time:datetime',
//        'ip',
        'memo'
    ];

    if ('income' == $mode) {
        array_pop($columns);
        $columns[] = [
            'attribute' => 'node_id',
            'label' => 'ID матрицы'
        ];
        $columns[] = [
            'attribute' => 'node_id',
            'label' => 'Пакет',
            'value' => function(Transfer $transfer) {
                return $transfer->node
                    ? $transfer->node->type->name : null;
            }
        ];
        $columns[] = [
            'attribute' => 'node_id',
            'label' => 'Уровень',
            'value' => function(Transfer $transfer) {
                return  $transfer->matrix
                    ? $transfer->matrix->level : null;
            }
        ];
    }
}

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>
<div class="transfer-index">
    <h1><?= $user
            ? str_replace($user, Html::a($user, ['/user/view', 'name' => $user]), $this->title)
            : $this->title; ?></h1>
    <?php if('income' != $mode):
        if ($user) {
            echo Yii::$app->view->render('@app/views/user/panel', [
                'model' => User::findOne(['name' => $user])
            ]);
            echo '<hr/>';
        }
        ?>
    <?php
    endif;

    $buttons = [];
    if ('income' != $mode) {
        $buttons[] = Html::a('Перевести', ['create'], ['class' => 'btn btn-primary']);
    }
    if (Yii::$app->user->identity->isManager()) {
        $buttons[] = 'income' == $mode
            ? Html::a('Переводы', ['/invoice/transfer/index', 'user' => $user],
                ['class' => 'btn btn-default'])
            : Html::a('Прибыль', ['index', 'mode' => 'income', 'user' => $user],
                ['class' => 'btn btn-default']);
    }
    echo Html::tag('p', implode("\n", $buttons));
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns
    ]); ?>
</div>
