<?php

use app\models\Settings;
use app\models\User;
use app\modules\invoice\models\Invoice;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\invoice\models\search\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var User $user */

$this->title = Yii::t('app', 'Invoices');
$user = Yii::$app->user->identity;

$me = !Yii::$app->user->getIsGuest() && isset($_GET['user']) && $_GET['user'] == $user->name;
if ($me) {
    $this->params['breadcrumbs'][] = ['label' => $_GET['user'],
        'url' => ['/user/view', 'name' => $_GET['user']]];
    $this->params['breadcrumbs'][] = $this->title;
}

$actions = ['class' => 'yii\grid\ActionColumn'];
if (!$user->isManager()) {
    $actions['template'] = '{view}';
}

$columns = ['id', 'number', 'batch', 'wallet'];

if (empty($_GET['user'])) {
    $columns[] =
        [
            'attribute' => 'user_name',
            'format' => 'html',
            'value' => function(Invoice $model) {
                return Html::a($model->user_name, ['index', 'user' => $model->user_name]);
            }
        ];
}

$columns[] = [
    'attribute' => 'amount',
    'value' => function(Invoice $model) {
        return isset($_GET['scenario']) ? abs($model->amount) : $model->amount;
    }
];
$columns[] = [
    'attribute' => 'status',
    'format' => 'html',
    'value' => function(Invoice $model) {
        $status = Yii::t('app', Invoice::$statuses[$model->status]);
        if ($model->amount < 0
            and 'success' != $model->status
            and ($user->isManager() || !Settings::get('common', 'withdrawal_confirmation'))) {
            $status .= ' ' .Html::a(Yii::t('app', 'Withdraw'),
                    ['withdraw', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs']);
        }
        return $status;
    }
];
$columns[] = $actions;
$payment_url = ['index', 'scenario' => 'payment'];
$withdraw_url = ['index', 'scenario' => 'withdraw'];
$index_url = ['index'];
if (isset($_GET['user'])) {
    $payment_url['user'] = $_GET['user'];
    $withdraw_url['user'] = $_GET['user'];
    $index_url['user'] = $_GET['user'];
}
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?php
        if ($user->isManager()):
            echo Html::a('Payment summary', ['summary'], ['class' => 'btn btn-default']);
        else: ?>
            <?= Html::a(Yii::t('app', 'Pay'), ['pay'], ['class' => 'btn btn-success']); ?>
            <?php
            if ($user->account > 0) {
                echo Html::a(Yii::t('app', 'Withdraw'),
                    ['create', 'scenario' => 'withdraw'], ['class' => 'btn btn-primary']);
            }
            ?>
        <?php endif ?>
    </p>

    <div class="form-group">
        <?= Yii::t('app', 'Show') ?>:
        <?= empty($_GET['scenario']) ? 'all' : Html::a('все', $index_url) ?>
        <?= isset($_GET['scenario']) && 'payment' == $_GET['scenario'] ? Yii::t('app', 'полнения')
            : Html::a(Yii::t('app', 'payments'), $payment_url) ?>
        <?= isset($_GET['scenario']) && 'withdraw' == $_GET['scenario'] ? Yii::t('app', 'выводы')
            : Html::a(Yii::t('app', 'withdraws'), $withdraw_url) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>

</div>
