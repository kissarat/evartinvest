<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer min
 * @property integer max
 * @property string name
 * @property boolean enabled
 */
class Type extends ActiveRecord {
    private static $_all;
    public static $friendlyURLs = [
        'three-month' => 1,
        'six-month' => 2,
        'twelve month' => 3,
        'stable' => 4,
        'business' => 5,
        'elite' => 6,
    ];

    public static function findFriendlyURL($id) {
        foreach(static::$friendlyURLs as $url => $val) {
            if ($id == $val) {
                return $url;
            }
        }
        return false;
    }

    public static function tableName() {
        return 'matrix_type';
    }
    public static function all()
    {
        if (!static::$_all) {
            static::$_all = [];
            foreach (Type::find()
                         ->where(['enabled' => true])
                         ->orderBy(['id' => SORT_ASC])->all() as $type) {
                static::$_all[$type->id] = $type;
            }
        }
        return static::$_all;
    }
    /**
     * @param $user_name
     * @param $price
     * @return \yii\db\ActiveQuery
     */
    public function open($user_name, $price) {
        return SQL::execute('SELECT enter(:user_name, :type_id, :ip, :sum)', [
            ':user_name' => $user_name,
            ':type_id' => $this->id,
            ':ip' => $_SERVER['REMOTE_ADDR'],
            ':sum' => $price
        ]);
    }
    public static function enum() {
        $data = ArrayHelper::map(Type::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name');
        return $data;
    }
    public static function get($id){
        return static::all()[$id];
    }
}
