<?php
use yii\helpers\Url;
use yii\widgets\Menu;
use app\helpers\MainAsset;

MainAsset::register($this);

$name = Yii::$app->user->identity->name;
$user = \app\models\User::findOne(['name'=>$name]);
$this->beginContent('@app/views/layouts/main.php');
?>
    <div id="cabinet">
        <div class="head">
            <div class="container"> 
                <div class="avatar"><?= $user->image ? '<img src="'.$user->image.'">' : ''?></div>
                <div class="header">
                    <div class="left-header">
                        <div class="namelogin"><?= $user->forename." ".$user->surname?></div>
                        <div class="status"><?= Yii::t('app',$user->getRang()['title']) ?></div>
                        <div class="reflink">
                            <input class="copy-selector" value="<?= Url::to(['/register','ref_name'=>$user->name],'http') ?>">
                            <?= chornij\zeroclipboard\Button::widget([
                                'label' => Yii::t('app', 'Copy'),
                                'encodeLabel' => false,
                                'text' => "$('input.copy-selector').attr('value')",
                            ]) ?>
                        </div>
                        <div class="ref">
                            <?= Yii::t('app','Referral') ?>
                        </div>
                        <div class="info-regeral"><?= $user->refNumber().Yii::t('app',' referrals') ?></div>
                    </div>
                    <div class="right-header">
                        <div class="info-balance"><span><?= Yii::t('app','Your account') ?></span> <br/><?= $user->account ?>E</div>
                        <button class="krug" ><a href="<?= Url::to('/invoice/invoice/pay') ?>"><?= Yii::t('app','Pay') ?></a></button> <br/>
                        <button class="krug" ><a href="<?= Url::to(['/invoice/invoice/withdraw','id'=>$user->name]) ?>"><?= Yii::t('app','Withdraw') ?></a></button>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <div class="panel">


                <div style="clear: both"></div>
                <div class="menu">
                    <?=
                    Menu::widget([
                        'items' => [
//                ['label' => Yii::t('app', 'Home'), 'url' => ['/']],
                            ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/view']],
                            ['label' => Yii::t('app', 'Edit'), 'url' => ['/user/update', 'name' => $name]],
                            ['label' => Yii::t('app', 'Structure'), 'url' => '/user/line'],
                            ['label' => Yii::t('app', 'History'), 'url' => '/invoice/invoice'],
                            ['label' => Yii::t('app', 'Deposits'), 'url' => ['/plans/' . $name]],
//                ['label' => Yii::t('app', 'Profit'), 'url' => ['/invoices/user/' . $name]],
//                ['label' => Yii::t('app', 'Transfers'), 'url' => ['/transfers/user/' . $name]],
                            ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/feedback/index']],
                            ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout']],
                        ]
                    ]); ?>
                    <div class="sponsor">
                    <?= Yii::t('app','Sponsor').":<br> ".$user->getReferral()->one()['forename']." ".$user->getReferral()->one()['surname']. ($user->getReferral()->one()['image'] ? '<img src="'.$user->getReferral()->one()['image'].'">' : '')?>
                    </div>
                </div>
            </div>


            <div class="main">
                <!-- Cabinet begin -->
                <!-- Cabinet begin -->
                <!-- Cabinet begin -->

                <?= $content ?>

                <!-- Cabinet end -->
                <!-- Cabinet end -->
                <!-- Cabinet end -->
            </div>
        </div>
    </div>
<?php
$this->endContent();
