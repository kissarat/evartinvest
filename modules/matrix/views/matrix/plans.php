<?php
/** @var array $plans */
use app\models\User;
use app\modules\matrix\models\Type;
use yii\db\Query;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Cases');
$buy = null;
if (\app\modules\matrix\models\Node::findOne(['user_name' => Yii::$app->user->identity])) {
    $buy = Html::a(Yii::t('app', 'Buy'), ['/invoice/invoice/pay']);
} else {
    $buy = Html::a(Yii::t('app', 'Buy'), ['/payapp/create', 'user' => Yii::$app->user->identity]);
}
$query = new Query();
$plus = [];
$desc = $query->select('type_id, MAX(interval),part')->from('income_schedule')->groupBy('type_id, part')->all();
$plandesc = [];
foreach ($desc as $item){
    switch ($item['max']){
        case '3 mons':
            $plandesc[$item['type_id']] = Yii::t('app', 'Трехмесячный');
            break;
        case '6 mons':
            $plandesc[$item['type_id']] = Yii::t('app', 'Шестимесячный');
            break;
        case '1 year':
            $plandesc[$item['type_id']] = Yii::t('app', 'Годовой');
            break;
    }
    if($item['part']==1){
        $plus[$item['type_id']] = '+';
    }
}
foreach (Type::find()->orderBy(['id' => SORT_ASC])->andWhere('enabled')->all() as $plan) {
    echo "<div class=\"plan\">";
    ?>
    <div class="case"><?= $plandesc[$plan->id]?></div>
    <div class="name"><?= $plan->name . ""; ?></div>
    <ul>
        <li><span class="pluss"></span><?= Yii::t('app', 'Minimal investment') . " " . $plan->min . "Є" ?></li>
        <li><span class="pluss"></span><?= Yii::t('app', 'Maximum investment') . " " . $plan->max . "Є" ?></li>
        <li><span class="pluss"></span><?= Yii::t('app', 'Profit') . " " . ($query->select('SUM(percentage)')->from('income_schedule')->where(['type_id' => $plan->id])->groupBy('type_id')->one()['sum'] * 100) . "%" ?></li>
        <li><span class="pluss"></span><?= Yii::t('app', 'Number of payments') . " " . ($query->select('COUNT(id)')->from('income_schedule')->where(['type_id' => $plan->id])->groupBy('type_id')->one()['count']).(array_key_exists($plan->id,$plus) ? $plus[$plan->id] : '')  ?></li>
    </ul>
    <?= $buy ?>

    <?php
    echo "</div>";
}
?>