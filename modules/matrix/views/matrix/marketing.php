<?php
/** @var string $marketing */
/** @var array $plans */
use app\modules\matrix\models\Type;
use yii\helpers\Html;

?>
<div class="matrix-marketing">
    <?= $marketing ?>

    <table id="plans" class="table">
        <thead>
        <tr>
            <th>Пакет</th>
            <th>Стоимость</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($plans as $plan) {
            $columns = [
                Html::tag('td', $plan['name']),
                Html::tag('td', $plan['price'])
            ];
            if (Yii::$app->user->getIsGuest()) {
                $columns[] = Html::tag('td', Html::a('Зарегистрироваться', ['/user/signup']));
            }
            else {
                if (Yii::$app->user->identity->account >= $plan['price']) {
                    $columns[] = Html::tag('td', Html::a('Открыть', ['/matrix/matrix/open', 'id' => $plan['id']]));
                }
                else {
                    $columns[] = Html::tag('td', 'Недостаточно средств');
                }
            }
            echo Html::tag('tr', implode('', $columns), [
                'data-id' => $plan['id'],
                'data-name' => Type::findFriendlyURL($plan['id'])
            ]);
        }
        ?>
        </tbody>
    </table>
</div>
