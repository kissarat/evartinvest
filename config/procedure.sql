START TRANSACTION;

CREATE OR REPLACE FUNCTION transfer(
  sender_name   VARCHAR(24),
  receiver_name VARCHAR(24),
  amount        DECIMAL(8, 2),
  memo          TEXT,
  ip            INET,
  node_id       INT,
  debit         BOOL DEFAULT TRUE)
  RETURNS INT AS $$
DECLARE
  _id INT;
BEGIN
  INSERT INTO journal ("type", "event", "user_name", ip) VALUES ('transfer', 'do', sender_name, ip)
  RETURNING id
    INTO _id;
  INSERT INTO transfer VALUES (_id, receiver_name, amount, memo, node_id);
  IF debit
  THEN
    UPDATE "user"
    SET account = account - amount
    WHERE "name" = sender_name;
  END IF;
  UPDATE "user"
  SET account = account + amount
  WHERE "name" = receiver_name;
  PERFORM changestatus(receiver_name);
  RETURN _id;
END
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION cancel(_id INT, charge BOOL DEFAULT TRUE)
  RETURNS VOID AS $$
DECLARE
  _sender_name   VARCHAR(24);
  _receiver_name VARCHAR(24);
  _amount        DECIMAL(8, 2);
BEGIN
  SELECT
    sender_name,
    receiver_name,
    amount
  FROM transfer_journal
  WHERE id = _id
  INTO _sender_name, _receiver_name, _amount;
  UPDATE "user"
  SET account = account - _amount
  WHERE "name" = _receiver_name;
  IF charge
  THEN
    UPDATE "user"
    SET account = account + _amount
    WHERE "name" = _sender_name;
  END IF;
  DELETE FROM transfer
  WHERE id = _id;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.enter(sponsor varchar, tid int2, ip inet, sum int4)
  RETURNS int4
AS
$BODY$
DECLARE
  _id INT;
  _rang INT;
BEGIN
  INSERT INTO matrix_node (user_name, type_id, price) VALUES (sponsor, tid, sum)
  RETURNING id
    INTO _id;
  CREATE TEMPORARY TABLE _transfer AS SELECT
                                        user_name,
                                        interest
                                      FROM referral
                                      WHERE root = sponsor;
  PERFORM transfer(sponsor, user_name, interest :: DECIMAL(8, 2), NULL, ip, _id, FALSE)
  FROM _transfer;
  DROP TABLE _transfer;
  SELECT rang FROM "user" WHERE "name"=sponsor
  INTO _rang;
  IF _rang = 1
  THEN
    UPDATE "user"
    SET rang = 2
    WHERE "name" = sponsor;
  END IF;
  RETURN _id;
END
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION exit(_id INT)
  RETURNS INT AS $$
DECLARE
  number INT;
BEGIN
  PERFORM cancel(id, FALSE)
  FROM _transfer;
  DELETE FROM matrix_node
  WHERE id = _id;
  SELECT count(*)
  FROM _transfer
  INTO number;
  RETURN number;
END
$$ LANGUAGE plpgsql;

-- CREATE OR REPLACE FUNCTION cron()
--   RETURNS VOID AS $$
-- DECLARE _user_name VARCHAR(24);
--   _amount DOUBLE PRECISION;
--   _node_id INTEGER;
--   _schedule_id INTEGER;
-- BEGIN
--   SELECT user_name, amount, node_id, schedule_id
--     FROM income_queue
--       INTO _user_name, _amount, _node_id, _schedule_id;
--   UPDATE "user" u
--   SET account = account + _amount
--   WHERE u.name = _user_name;
--   INSERT INTO income ("type", "event", object_id, "data", node_id, schedule_id)
--     VALUES (
--       'user',
--       '+',
--       _user_name,
--       _amount :: VARCHAR,
--       _node_id,
--       _schedule_id );
-- END;
-- $$
-- LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION cron()
  RETURNS VOID AS $$
BEGIN
  UPDATE "user" u
  SET account = account + amount FROM income_queue q
  WHERE u.name = q.user_name;
  INSERT INTO income ("type", "event", object_id, "data", node_id, schedule_id)
    SELECT
      'user',
      '+',
      user_name,
      amount :: VARCHAR,
      node_id,
      schedule_id
    FROM income_queue q;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION changestatus(_user VARCHAR(24))
  RETURNS VOID AS $$
DECLARE
  _turnover DOUBLE PRECISION;
  _sum INTEGER;
  _premium INTEGER;
BEGIN
  SELECT
    turnover,
    sum,
    premium
  FROM turnover
  WHERE user_name = _user AND sum IS NOT NULL
  INTO _turnover, _sum, _premium;
  IF _turnover >= _sum
  THEN
    UPDATE "user"
    SET rang = rang +1
    WHERE "name" = _user;
    IF _premium IS NOT NULL
    THEN
      INSERT INTO premium ("type","event","user_name","time","sum")
      VALUES ('user','premium',_user,now(),_premium);
      UPDATE "user"
      SET account = account +_premium
      WHERE "name" = _user;
    END IF;
  END IF;
END;
$$ LANGUAGE plpgsql;

COMMIT;
