CREATE VIEW "translation" AS
  SELECT
    s.id,
    "message",
    translation
  FROM source_message s
    JOIN message t ON s.id = t.id
  WHERE "language" = 'ru';

CREATE OR REPLACE VIEW transfer_journal AS
  SELECT
    j.id,
    j.user_name AS sender_name,
    receiver_name,
    amount,
    memo,
    "time",
    ip,
    node_id
  FROM journal j
    JOIN transfer t ON j.id = t.id
  UNION
    SELECT
      id,
      'premium'::VARCHAR(24) AS sender_name,
      user_name::VARCHAR(24) AS receiver_name,
      sum::NUMERIC(8,2) AS amount,
      NULL AS memo,
      "time",
      ip,
      NULL AS node_id
  FROM premium
  UNION
    SELECT
  id,
  'profit'::VARCHAR(24) AS sender_name,
      object_id::VARCHAR(24) AS user_name,
      "data"::NUMERIC(8,2) AS amount,
      NULL AS memo,
      "time",
      "ip",
      node_id
  FROM income WHERE event = '+'
;

CREATE VIEW invoice_sum AS
  SELECT
    user_name,
    payment,
    -withdraw          AS withdraw,
    payment + withdraw AS balance
  FROM
    (
      SELECT
        user_name,
        coalesce((SELECT sum(amount)
                  FROM invoice p
                  WHERE p.user_name = i.user_name AND p.amount > 0), 0) AS payment,
        coalesce((SELECT sum(amount)
                  FROM invoice p
                  WHERE p.user_name = i.user_name AND p.amount < 0), 0) AS withdraw
      FROM invoice i
      GROUP BY user_name
    ) s;

CREATE VIEW income_queue AS
  WITH queue AS (
    SELECT
      n.id AS node_id,
      s.id AS schedule_id
    FROM matrix_node n
      JOIN income_schedule s ON n.type_id = s.type_id
    WHERE n.time + s.interval <= now()
    EXCEPT
    SELECT
      node_id,
      schedule_id
    FROM income
  )
  SELECT
    node_id,
    schedule_id,
    n.price * (percentage + part) AS amount,
    n.user_name
  FROM queue q
    JOIN matrix_node n ON q.node_id = n.id
    JOIN income_schedule s ON q.schedule_id = s.id;

CREATE VIEW profit_queue AS
  SELECT
    sum(amount) AS amount,
    user_name
  FROM income_queue
  GROUP BY user_name;

CREATE VIEW matrix AS
  WITH RECURSIVE r(id, type_id, user_name, ref_name, "level", user_id, root, "time", price) AS (
    SELECT
      n.id,
      type_id,
      user_name,
      ref_name,
      0         AS "level",
      u.id      AS user_id,
      user_name AS root,
      "time",
      price
    FROM matrix_node AS n
      JOIN "user" u ON u.name = n.user_name
    UNION
    SELECT
      n.id,
      n.type_id,
      n.user_name,
      u.ref_name,
      r."level" + 1 AS "level",
      u.id          AS user_id,
      r.root,
      n."time",
      n.price
    FROM matrix_node n
      JOIN "user" u ON n.user_name = u.name
      JOIN r ON r.user_name = u.ref_name
  )
  SELECT
    r.id,
    row_number()
    OVER (PARTITION BY root, type_id
      ORDER BY user_id) AS "number",
    type_id,
    user_name,
    ref_name,
    "level",
    user_id,
    root,
    price * income      AS interest,
    "time"
  FROM r
    JOIN matrix_type t ON r.type_id = t.id
    JOIN matrix_level l ON r."level" = l.id
  WHERE "level" > 0;


-- CREATE OR REPLACE VIEW user_deposit AS
--   SELECT
--     u.id,
--     u.name,
--     u.ref_name,
--     (CASE WHEN count(n.*) > 0
--       THEN sum(n.interest)
--      ELSE 0 END) AS deposit
--   FROM "user" u LEFT JOIN referral n ON u.name = n.root
--   WHERE n.user_name = u.ref_name
--   GROUP BY u.id, u.name, u.ref_name
--   UNION
--   (
--     WITH p AS (SELECT
--                  u.id,
--                  u.name,
--                  u.ref_name,
--                  (CASE WHEN count(n.*) > 0
--                    THEN sum(n.interest)
--                   ELSE 0 END) AS deposit
--                FROM "user" u LEFT JOIN referral n ON u.name = n.root
--                GROUP BY u.id, u.name, u.ref_name
--                EXCEPT (
--                  SELECT
--                    u.id,
--                    u.name,
--                    u.ref_name,
--                    (CASE WHEN count(n.*) > 0
--                      THEN sum(n.interest)
--                     ELSE 0 END) AS deposit
--                  FROM "user" u LEFT JOIN referral n ON u.name = n.root
--                  WHERE n.user_name = u.ref_name
--                  GROUP BY u.id, u.name, u.ref_name
--                )
--     )
--     SELECT
--       id,
--       "name",
--       ref_name,
--       deposit
--     FROM p
--     WHERE deposit = 0
--   );

CREATE OR REPLACE VIEW user_matrix AS
  SELECT
    user_id                   AS id,
    "root"                    AS "name",
    (SELECT ref_name
     FROM "user" u
     WHERE u."name" = r.root) AS ref_name,
    "user_name"               AS root,
    "level",
    sum(interest)             AS "deposit"
  FROM referral r
  GROUP BY user_name, root, ref_name, level, user_id
  UNION (
    WITH RECURSIVE r(id, "name", ref_name, root, "level", deposit) AS (
      SELECT
        id,
        "name",
        "ref_name",
        "name" AS "root",
        0      AS "level",
        0      AS "deposit"
      FROM "user"
      UNION
      SELECT
        u.id,
        u.name,
        u.ref_name,
        r.root,
        r.level + 1 AS "level",
        0           AS "deposit"
      FROM "user" u
        JOIN r ON u.ref_name = r.name
    )
    SELECT
      id,
      "name",
      "ref_name",
      "root",
      "level",
      "deposit"
    FROM r
    EXCEPT (
      SELECT
        user_id   AS id,
        user_name AS "name",
        ref_name,
        root,
        "level",
        0         AS "deposit"
      FROM referral
      GROUP BY user_name, root, ref_name, level, user_id
    )

  );


CREATE OR REPLACE VIEW branch AS
  WITH r AS (
    WITH RECURSIVE base("name", "ref_name", "root", "deposit", "level", branch_ref) AS (
      SELECT
        "name",
        ref_name,
        root,
        deposit,
        1      AS "level",
        "name" AS branch_ref
      FROM user_matrix
      UNION
      SELECT
        m.name,
        m.ref_name,
        m.root,
        m.deposit,
        b.level + 1 AS "level",
        b.branch_ref
      FROM user_matrix m
        JOIN base b ON m.ref_name = b.name
    )
    SELECT
      root,
      sum(deposit) AS "deposit",
      branch_ref
    FROM base
    GROUP BY root, branch_ref
  )
  SELECT
    r.root,
    r.deposit,
    r.branch_ref
  FROM r
    JOIN "user" u ON r.branch_ref = u.name
  WHERE r.root = u.ref_name;

CREATE OR REPLACE VIEW revenue AS
  WITH
      s AS (
        SELECT
          root,
          sum(deposit)      AS total_interest,
          count(branch_ref) AS "count"
        FROM branch
        GROUP BY root
    ),
      sec AS (
        SELECT
          root,
          max(deposit) AS "value"
        FROM branch
        WHERE deposit NOT IN (SELECT max(deposit)
                              FROM branch)
        GROUP BY root
    ),
      f AS (
        SELECT
          b.root,
          max(b.deposit) AS "first",
          sec.value      AS "second"
        FROM branch b
          JOIN sec ON b.root = sec.root
        GROUP BY b.root, sec.value
    ),
      bb AS (
        SELECT
          b.root,
          (CASE WHEN f.first > s.total_interest * 0.6
            THEN total_interest * 0.6
           ELSE f.first END)  AS "part1",
          (CASE WHEN f.first > s.total_interest * 0.6 AND f.second > s.total_interest * 0.3
            THEN total_interest * 0.3
           ELSE f.second END) AS "part2"
        FROM branch b
          JOIN s ON b.root = s.root
          JOIN f ON b.root = f.root
        WHERE s."count" >= 3
    )
  SELECT DISTINCT
    bb.root                                                           AS "user",
    ((s.total_interest - (f.first + f.second)) + bb.part1 + bb.part2) AS "turnover"
  FROM bb
    JOIN s ON bb.root = s.root
    JOIN f ON bb.root = f.root;


CREATE OR REPLACE VIEW referral AS
  WITH RECURSIVE r(id, type_id, user_name, ref_name, "level", user_id, root, "time", "price", lines) AS (
    SELECT
      n.id,
      n.type_id,
      n.user_name,
      u.ref_name,
      0                    AS "level",
      u.id                 AS user_id,
      n.user_name          AS root,
      n."time",
      coalesce(n.price, 0) AS price,
      s.lines              AS lines
    FROM "user" u
      JOIN "matrix_node" n ON n.user_name = u.name
      LEFT JOIN status s ON s.id = u.rang
    UNION
    SELECT
      r.id,
      r.type_id,
      r.ref_name           AS "user_name",
      u.ref_name,
      r."level" + 1        AS "level",
      u.id                 AS user_id,
      r.root,
      r."time",
      coalesce(r.price, 0) AS price,
      s.lines
    FROM "user" u
      JOIN r ON r.ref_name = u.name
      LEFT JOIN status s ON s.id = u.rang
  )
  SELECT
    r.id,
    row_number()
    OVER (PARTITION BY root, type_id
      ORDER BY user_id) AS "number",
    r.type_id,
    r.user_name,
    r.ref_name,
    r."level",
    r.user_id,
    r.root,
    r.price * income    AS interest,
    r."time"
  FROM r
    LEFT JOIN matrix_type t ON r.type_id = t.id
    JOIN matrix_level l ON r."level" = l.id
  WHERE "level" > 0 AND "level" <= r.lines;

CREATE VIEW turnover AS
  WITH t(user_name, turnover, status_id, sum, premium) AS (
      SELECT
        u.name        AS user_name,
        r.turnover AS turnover,
        s.id          AS status_id,
        s.turnover    AS sum,
        s.premium     AS premium
      FROM "user" u
        JOIN status s ON u.rang = s.id
        LEFT JOIN revenue r ON r."user" = u.name
       )
  SELECT
    t.user_name,
    t.turnover,
    t.status_id,
    s.turnover AS sum,
    s.premium  AS premium
  FROM t
    LEFT JOIN status s ON s.id = t.status_id + 1;

CREATE VIEW "time" AS
  WITH s("count", type_id, user_name) AS (
      SELECT
        COUNT(id) AS "count",
        type_id,
        user_name
      FROM matrix_node
      GROUP BY type_id, user_name
  )
  SELECT
    s.count,
    s.type_id,
    s.user_name,
    MAX(n.time) + '1 year' :: INTERVAL AS "time"
  FROM s
    JOIN matrix_node n ON s.user_name = n.user_name
  WHERE n.type_id = s.type_id
  GROUP BY s.type_id, s.user_name, s.count;

CREATE VIEW "earnings" AS
  WITH r("user_name", "time", "type_id", "price", "data", "percentage", "interval") AS (
      SELECT
        m.user_name,
        m."time",
        m."type_id",
        m."price",
        (SELECT SUM(data :: DOUBLE PRECISION)
         FROM income
         WHERE object_id = m.user_name
         GROUP BY object_id)        AS "data",
        (SELECT SUM(percentage)
         FROM income_schedule
         WHERE type_id = m.type_id
         GROUP BY type_id)          AS "percentage",
        (SELECT MAX("interval")
         FROM income_schedule
         WHERE type_id = m.type_id) AS "interval"
      FROM matrix_node m
  )
  SELECT
    r."user_name",
    r.data,
    r.price,
    r.percentage,
    r.time,
    r.interval
  FROM r;

CREATE OR REPLACE VIEW structure AS
  WITH RECURSIVE r("user", name, ref_name, rang, lines, level) AS (
    SELECT
      u.name AS "user",
      u.name,
      u.ref_name,
      u."rang",
      s.lines,
      0      AS "level"
    FROM "user" u
      LEFT JOIN status s ON s.id = u.rang
    UNION
    SELECT
      r.user,
      u.name,
      u.ref_name,
      u.rang,
      s.lines,
      r."level" + 1 AS "level"
    FROM "user" u
      JOIN r ON r.ref_name = u.name
      LEFT JOIN status s ON s.id = u.rang
  )
  SELECT
    r.user,
    r.name,
    r.ref_name,
    r.lines,
    r.level
  FROM r
    JOIN matrix_level l ON r."level" = l.id
  WHERE "level" > 0 AND "level" <= r.lines;

CREATE VIEW "visit" AS
  SELECT
    p.id   AS id,
    agent_id,
    spend,
    user_name,
    "path",
    p."id" AS "time",
    a.ip,
    agent
  FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id;


CREATE VIEW chart AS
  SELECT
  receiver_name AS "user",
    "time",
    sum(amount) OVER (PARTITION BY receiver_name ORDER BY "time" ASC) AS amount
    FROM transfer_journal
  ORDER BY receiver_name, "time";
