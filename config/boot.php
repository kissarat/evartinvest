<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV_DEV') or define('YII_ENV_DEV', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
define('ROOT', __DIR__ . '/..');
define('PAYMENT_SYSTEM_ENABLED', false);
define('SMS_ENABLED', true);
define('IS_LOCALHOST', strpos('localhost', 'localhost') !== false);

// Fri Feb 01 2030 00:00:00 GMT+0200 (EET)
define('FOREVER', 1896127200);

require_once ROOT . '/vendor/autoload.php';
require_once ROOT . '/vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/common.php';
require_once __DIR__ . '/local.php';
