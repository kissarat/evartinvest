<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 26.03.16
 * Time: 13:09
 */
require_once 'pdo.php';

$pdo = connect();
$pdo->beginTransaction();
$pdo->exec('UPDATE "user" u
  SET account = account + amount FROM profit_queue q
  WHERE u.name = q.user_name;');
$pdo->exec('INSERT INTO income ("type", "event", object_id, "data", node_id, schedule_id)
    SELECT
      \'user\',
      \'+\',
      user_name,
      amount :: VARCHAR,
      node_id,
      schedule_id
    FROM income_queue q;');
$pdo->commit();