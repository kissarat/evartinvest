/* Yii English source translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TYPE source_message_category AS ENUM ('app');

CREATE TABLE "source_message" (
  id       SERIAL PRIMARY KEY,
  category source_message_category NOT NULL,
  message  TEXT
);
CREATE UNIQUE INDEX message_id ON "source_message" USING BTREE ("id");

CREATE TABLE "language" (
  "id"     CHAR(2)     NOT NULL PRIMARY KEY,
  "name"   VARCHAR(48) NOT NULL,
  "native" VARCHAR(96) NOT NULL,
  "order"  INT         NOT NULL DEFAULT 0
);
INSERT INTO "language" VALUES ('ru', 'Russian', 'Русский', 1);
INSERT INTO "language" VALUES ('en', 'English', 'English', 2);

/* Yii Russian target translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "message" (
  "id"          INT,
  "language"    CHAR(2),
  "translation" TEXT,
  PRIMARY KEY (id, language),
  CONSTRAINT message_source FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT message_language FOREIGN KEY ("language")
  REFERENCES language (id) ON DELETE CASCADE ON UPDATE RESTRICT
);

CREATE TABLE "status" (
  id       SERIAL PRIMARY KEY,
  title    VARCHAR(255) NOT NULL UNIQUE,
  lines    INT          NOT NULL,
  turnover INT,
  premium  INT
);

INSERT INTO status VALUES (1, 'Partner', 3, NULL, NULL);
INSERT INTO status VALUES (2, 'Investor', 3, NULL, NULL);
INSERT INTO status VALUES (3, 'Financial consultant', 5, 3000, NULL);
INSERT INTO status VALUES (4, 'Financial expert', 7, 10000, NULL);
INSERT INTO status VALUES (5, 'Regional representative', 9, 21000, NULL);
INSERT INTO status VALUES (6, 'Top manager', 9, 33000, NULL);
INSERT INTO status VALUES (7, 'Director', 9, 47000, 1500);
INSERT INTO status VALUES (8, 'Regional director', 9, 77000, 2300);
INSERT INTO status VALUES (9, 'International director', 9, 144000, 4300);
INSERT INTO status VALUES (10, 'General director', 9, 333000, 9000);
INSERT INTO status VALUES (11, 'Chairman', 9, 777000, 18000);
INSERT INTO status VALUES (12, 'President', 9, 1111111, 27000);
INSERT INTO status VALUES (13, 'President chairman', 9, 1777777, 51000);


CREATE TYPE sex AS ENUM ('male', 'female');
CREATE TYPE "communicationLanguage" AS ENUM ('RU', 'EN', 'LV');
CREATE TYPE "passportType" AS ENUM ('ID', 'PP');
CREATE TYPE "activityAndIncome" AS ENUM ('PE', 'EN', 'SE', 'RE', 'ST', 'UE');
CREATE TYPE "businessType" AS ENUM (
  'ED', 'MM', 'EG', 'WS', 'CI', 'RW',
  'VR', 'TS', 'HC', 'TT', 'SL', 'FI',
  'HS', 'AR', 'AS', 'PA', 'ST', 'OT'
);
CREATE TYPE "accountUsage" AS ENUM ('PERSONAL', 'BUSINESS');
CREATE TYPE "openingPurpose" AS ENUM ('SA', 'CR', 'SG', 'IC', 'OT');
CREATE TYPE "monthlyTurnOver" AS ENUM ('10', '11', '20', '30', '40');
CREATE TYPE "topUp" AS ENUM ('MY', 'AA', 'CC', 'CA', 'WM', 'WA');
CREATE TYPE "topUpConnection" AS ENUM ('FA', 'FR', 'EM', 'CU', 'GT');
CREATE TYPE "deliveryInformation" AS ENUM ('BP', 'BR', 'BC', 'RB');

/* User account, see app\models\User */
CREATE TABLE "user" (
  id                       SERIAL PRIMARY KEY NOT NULL,
  name                     VARCHAR(24)        NOT NULL UNIQUE,
  account                  DECIMAL(8, 2)      NOT NULL  DEFAULT '0.00',
  email                    VARCHAR(48)        NOT NULL,
  hash                     CHAR(60),
  auth                     CHAR(64) UNIQUE,
  code                     VARCHAR(64),
  status                   SMALLINT           NOT NULL  DEFAULT 2,
  last_access              TIMESTAMP                    DEFAULT CURRENT_TIMESTAMP,
  data                     BYTEA,
  ref_name                 VARCHAR(24),
  forename                 VARCHAR(48),
  surname                  VARCHAR(48),
  city                     VARCHAR(48),
  country                  CHAR(2),
  birthday                 DATE,
  phone                    VARCHAR(24),
  language                 CHAR(2),
  skype                    VARCHAR(24),
  sex                      sex,
  ip                       VARCHAR(24),
  image                    VARCHAR(255),
  created                  TIMESTAMP          NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  rang                     INT                          DEFAULT 1,
  show_phone               BOOLEAN            NOT NULL  DEFAULT TRUE,

  "applicationId"          INT,
  "communicationLanguage"  "communicationLanguage",
  "nationality"            CHAR(2),
  "address1"               VARCHAR(1000),
  "address2"               VARCHAR(1000),
  "zipCode"                VARCHAR(16),
  "passportType"           "passportType",
  "idNumber"               VARCHAR(32),
  "activityAndIncome"      "activityAndIncome",
  "businessType"           "businessType",
  "accountUsage"           "accountUsage",
  "openingPurpose"         "openingPurpose",
  "monthlyTurnOver"        "monthlyTurnOver",
  "firstTopUp"             "topUp",
  "firstTopUpConnection"   "topUpConnection",
  "otherTopUp"             "topUp",
  "otherTopUpConnection"   "topUpConnection",
  "deliveryInformation"    "deliveryInformation",
  "phoneCommunicationPass" VARCHAR(256),

  birth                    INT,
  social                   VARCHAR(256),

  FOREIGN KEY ("rang") REFERENCES status (id),
  CONSTRAINT ref_name FOREIGN KEY (ref_name) REFERENCES "user" ("name")
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "language" FOREIGN KEY ("language") REFERENCES "language" ("id")
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT skype CHECK (skype ~* '^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$')
);
CREATE UNIQUE INDEX user_id ON "user" USING BTREE ("id");
CREATE UNIQUE INDEX user_name ON "user" USING BTREE ("name");

INSERT INTO "user" (name, email, status, rang, phone, forename, surname, ref_name) VALUES
  ('admin', 'mviktorbiz@gmail.com', 1, 1, '380966905169', 'Evart', 'Invest', NULL),
  ('root', 'mviktorbiz@gmail.com', 2, 13, '380966905169', 'Evart', 'Invest', NULL),
  ('business', 'mviktorbiz@gmail.com', 2, 13, '380966905169', 'Evart', 'Invest', 'root'),
  ('evart', 'maksym.blazhkun@gmail.com', 2, 1, '37126544776', 'Evart', 'Invest', 'business'),
  ('evartworld', 'mviktorbiz@gmail.com', 2, 1, '380966905169', 'Evart', 'World', 'evart'),
  ('evartglobal', 'mviktorbiz@gmail.com', 2, 1, '380966905169', 'Evart', 'Global', 'evart'),
  ('evartinternational', 'mviktorbiz@gmail.com', 2, 1, '380966905169', 'Evart', 'International', 'evart');


/* User action log, see app\models\Record */
CREATE TABLE "journal" (
  id        SERIAL PRIMARY KEY NOT NULL,
  type      VARCHAR(16)        NOT NULL,
  event     VARCHAR(16)        NOT NULL,
  object_id VARCHAR(40),
  data      TEXT,
  user_name VARCHAR(24),
  time      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip        INET,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE transfer (
  id            INT           NOT NULL,
  receiver_name VARCHAR(24)   NOT NULL,
  amount        DECIMAL(8, 2) NOT NULL,
  memo          TEXT,
  node_id       INT,
  CONSTRAINT transfer_id FOREIGN KEY (id)
  REFERENCES "journal" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_receiver FOREIGN KEY (receiver_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT transfer_amount CHECK (amount > 0),
  CONSTRAINT transfer_memo CHECK (length(memo) >= 1 AND length(memo) < 600)
);

/* Payment (if amount > 0) and withdrawal (if amount < 0) table, see app\modules\invoice\models\Invoice */
CREATE TABLE "invoice" (
  id        UUID PRIMARY KEY NOT NULL,
  user_name VARCHAR(24)      NOT NULL,
  amount    DECIMAL(8, 2)    NOT NULL,
  "type"    VARCHAR(24)      NOT NULL,
  batch     BIGINT,
  status    VARCHAR(16) DEFAULT 'create',
  number    SERIAL           NOT NULL,
  wallet    VARCHAR(40),
  "time"    BIGINT,
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0),
  CONSTRAINT "time" CHECK ("time" > 0)
);
CREATE UNIQUE INDEX invoice_id ON "invoice" USING BTREE ("id");

CREATE TABLE "matrix_type" (
  id      SMALLINT PRIMARY KEY,
  min     INT  NOT NULL,
  max     INT  NOT NULL,
  name    VARCHAR(24),
  enabled BOOL NOT NULL
);

INSERT INTO "matrix_type" VALUES (1, 100, 5000, 'Starting Case', TRUE);
INSERT INTO "matrix_type" VALUES (2, 100, 5000, 'Optimal Case', TRUE);
INSERT INTO "matrix_type" VALUES (3, 100, 5000, 'Advanced Case', TRUE);
INSERT INTO "matrix_type" VALUES (4, 5000, 15000, 'Stable', TRUE);
INSERT INTO "matrix_type" VALUES (5, 15000, 21000, 'Business', TRUE);
INSERT INTO "matrix_type" VALUES (6, 21000, 33000, 'Elite', TRUE);

CREATE TABLE "income_schedule" (
  id         SERIAL PRIMARY KEY,
  type_id    SMALLINT NOT NULL,
  percentage FLOAT    NOT NULL DEFAULT 0,
  part       FLOAT    NOT NULL DEFAULT 0,
  interval   INTERVAL NOT NULL,
  CONSTRAINT income_schedule_type FOREIGN KEY (type_id)
  REFERENCES "matrix_type" (id)
);
INSERT INTO "income_schedule" VALUES (1, 1, 0.125, 1, INTERVAL '3 month');
INSERT INTO "income_schedule" VALUES (2, 2, 0.5 / 12, 0, INTERVAL '1 month');
INSERT INTO "income_schedule" VALUES (3, 2, 0.5 / 12, 0, INTERVAL '2 month');
INSERT INTO "income_schedule" VALUES (4, 2, 0.5 / 12, 0, INTERVAL '3 month');
INSERT INTO "income_schedule" VALUES (5, 2, 0.5 / 12, 0, INTERVAL '4 month');
INSERT INTO "income_schedule" VALUES (6, 2, 0.5 / 12, 0, INTERVAL '5 month');
INSERT INTO "income_schedule" VALUES (7, 2, 0.5 / 12, 1, INTERVAL '6 month');
INSERT INTO "income_schedule" VALUES (8, 3, 0.5 / 12, 1.0 / 12, INTERVAL '1 month');
INSERT INTO "income_schedule" VALUES (9, 3, 0.5 / 12, 1.0 / 12, INTERVAL '2 month');
INSERT INTO "income_schedule" VALUES (10, 3, 0.5 / 12, 1.0 / 12, INTERVAL '3 month');
INSERT INTO "income_schedule" VALUES (11, 3, 0.5 / 12, 1.0 / 12, INTERVAL '4 month');
INSERT INTO "income_schedule" VALUES (12, 3, 0.5 / 12, 1.0 / 12, INTERVAL '5 month');
INSERT INTO "income_schedule" VALUES (13, 3, 0.5 / 12, 1.0 / 12, INTERVAL '6 month');
INSERT INTO "income_schedule" VALUES (14, 3, 0.5 / 12, 1.0 / 12, INTERVAL '7 month');
INSERT INTO "income_schedule" VALUES (15, 3, 0.5 / 12, 1.0 / 12, INTERVAL '8 month');
INSERT INTO "income_schedule" VALUES (16, 3, 0.5 / 12, 1.0 / 12, INTERVAL '9 month');
INSERT INTO "income_schedule" VALUES (17, 3, 0.5 / 12, 1.0 / 12, INTERVAL '10 month');
INSERT INTO "income_schedule" VALUES (18, 3, 0.5 / 12, 1.0 / 12, INTERVAL '11 month');
INSERT INTO "income_schedule" VALUES (19, 3, 0.5 / 12, 1.0 / 12, INTERVAL '12 month');
INSERT INTO "income_schedule" VALUES (20, 4, 0.55 / 12, 0, INTERVAL '1 month');
INSERT INTO "income_schedule" VALUES (21, 4, 0.55 / 12, 0, INTERVAL '2 month');
INSERT INTO "income_schedule" VALUES (22, 4, 0.55 / 12, 0, INTERVAL '3 month');
INSERT INTO "income_schedule" VALUES (23, 4, 0.55 / 12, 0, INTERVAL '4 month');
INSERT INTO "income_schedule" VALUES (24, 4, 0.55 / 12, 0, INTERVAL '5 month');
INSERT INTO "income_schedule" VALUES (25, 4, 0.55 / 12, 0, INTERVAL '6 month');
INSERT INTO "income_schedule" VALUES (26, 4, 0.55 / 12, 0, INTERVAL '7 month');
INSERT INTO "income_schedule" VALUES (27, 4, 0.55 / 12, 0, INTERVAL '8 month');
INSERT INTO "income_schedule" VALUES (28, 4, 0.55 / 12, 0, INTERVAL '9 month');
INSERT INTO "income_schedule" VALUES (29, 4, 0.55 / 12, 0, INTERVAL '10 month');
INSERT INTO "income_schedule" VALUES (30, 4, 0.55 / 12, 0, INTERVAL '11 month');
INSERT INTO "income_schedule" VALUES (31, 4, 0.55 / 12, 1, INTERVAL '12 month');
INSERT INTO "income_schedule" VALUES (32, 5, 0.6 / 12, 1.0 / 12, INTERVAL '1 month');
INSERT INTO "income_schedule" VALUES (33, 5, 0.6 / 12, 1.0 / 12, INTERVAL '2 month');
INSERT INTO "income_schedule" VALUES (34, 5, 0.6 / 12, 1.0 / 12, INTERVAL '3 month');
INSERT INTO "income_schedule" VALUES (35, 5, 0.6 / 12, 1.0 / 12, INTERVAL '4 month');
INSERT INTO "income_schedule" VALUES (36, 5, 0.6 / 12, 1.0 / 12, INTERVAL '5 month');
INSERT INTO "income_schedule" VALUES (37, 5, 0.6 / 12, 1.0 / 12, INTERVAL '6 month');
INSERT INTO "income_schedule" VALUES (38, 5, 0.6 / 12, 1.0 / 12, INTERVAL '7 month');
INSERT INTO "income_schedule" VALUES (39, 5, 0.6 / 12, 1.0 / 12, INTERVAL '8 month');
INSERT INTO "income_schedule" VALUES (40, 5, 0.6 / 12, 1.0 / 12, INTERVAL '9 month');
INSERT INTO "income_schedule" VALUES (41, 5, 0.6 / 12, 1.0 / 12, INTERVAL '10 month');
INSERT INTO "income_schedule" VALUES (42, 5, 0.6 / 12, 1.0 / 12, INTERVAL '11 month');
INSERT INTO "income_schedule" VALUES (43, 5, 0.6 / 12, 1.0 / 12, INTERVAL '12 month');
INSERT INTO "income_schedule" VALUES (45, 6, 0.7 / 12, 1.0 / 12, INTERVAL '1 month');
INSERT INTO "income_schedule" VALUES (46, 6, 0.7 / 12, 1.0 / 12, INTERVAL '2 month');
INSERT INTO "income_schedule" VALUES (47, 6, 0.7 / 12, 1.0 / 12, INTERVAL '3 month');
INSERT INTO "income_schedule" VALUES (48, 6, 0.7 / 12, 1.0 / 12, INTERVAL '4 month');
INSERT INTO "income_schedule" VALUES (49, 6, 0.7 / 12, 1.0 / 12, INTERVAL '5 month');
INSERT INTO "income_schedule" VALUES (50, 6, 0.7 / 12, 1.0 / 12, INTERVAL '6 month');
INSERT INTO "income_schedule" VALUES (51, 6, 0.7 / 12, 1.0 / 12, INTERVAL '7 month');
INSERT INTO "income_schedule" VALUES (52, 6, 0.7 / 12, 1.0 / 12, INTERVAL '8 month');
INSERT INTO "income_schedule" VALUES (53, 6, 0.7 / 12, 1.0 / 12, INTERVAL '9 month');
INSERT INTO "income_schedule" VALUES (54, 6, 0.7 / 12, 1.0 / 12, INTERVAL '10 month');
INSERT INTO "income_schedule" VALUES (55, 6, 0.7 / 12, 1.0 / 12, INTERVAL '11 month');
INSERT INTO "income_schedule" VALUES (56, 6, 0.7 / 12, 1.0 / 12, INTERVAL '12 month');


CREATE TABLE "matrix_level" (
  id     SMALLINT PRIMARY KEY,
  income FLOAT NOT NULL
);

INSERT INTO "matrix_level" VALUES (1, 0.07);
INSERT INTO "matrix_level" VALUES (2, 0.04);
INSERT INTO "matrix_level" VALUES (3, 0.02);
INSERT INTO "matrix_level" VALUES (4, 0.01);
INSERT INTO "matrix_level" VALUES (5, 0.01);
INSERT INTO "matrix_level" VALUES (6, 0.01);
INSERT INTO "matrix_level" VALUES (7, 0.01);
INSERT INTO "matrix_level" VALUES (8, 0.02);
INSERT INTO "matrix_level" VALUES (9, 0.02);


CREATE TABLE "matrix_node" (
  id        SERIAL PRIMARY KEY,
  type_id   SMALLINT                            NOT NULL,
  user_name VARCHAR(24)                         NOT NULL,
  price     INT                                 NOT NULL,
  "time"    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE "income" (
  schedule_id INT NOT NULL,
  node_id     INT NOT NULL,
  CONSTRAINT income_schedule FOREIGN KEY (schedule_id)
  REFERENCES "income_schedule" (id),
  CONSTRAINT income_node FOREIGN KEY (node_id)
  REFERENCES "matrix_node" (id)
)
  INHERITS (journal);

CREATE TABLE max_revenue (
  number  SMALLINT PRIMARY KEY,
  percent FLOAT NOT NULL
);

INSERT INTO max_revenue VALUES
  (1, 0.6),
  (2, 0.3),
  (3, 0.1);

/* User and guest feedback table for app\modules\feedback\models\Feedback */
CREATE TABLE "feedback" (
  id      SERIAL PRIMARY KEY NOT NULL,
  ticket  CHAR(24) UNIQUE    NOT NULL,
  email   VARCHAR(48),
  subject VARCHAR(256)       NOT NULL,
  open    BOOL               NOT NULL DEFAULT TRUE
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING BTREE ("id");
CREATE UNIQUE INDEX feedback_ticket ON "feedback" USING BTREE ("ticket");

CREATE TABLE "feedback_message" (
  id          SERIAL PRIMARY KEY NOT NULL,
  feedback_id INT                NOT NULL,
  user_name   VARCHAR(24),
  answer      BOOL               NOT NULL DEFAULT FALSE,
  "content"   TEXT               NOT NULL,
  "time"      TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip          INET,
  CONSTRAINT feedback_user FOREIGN KEY (user_name)
  REFERENCES "user" ("name") ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE "article" (
  "id"       SERIAL PRIMARY KEY,
  "name"     VARCHAR(24),
  "title"    VARCHAR(256) NOT NULL,
  "keywords" VARCHAR(192),
  "summary"  TEXT,
  "content"  TEXT         NOT NULL
);
CREATE UNIQUE INDEX article_id ON "article" USING BTREE ("id");
INSERT INTO article (name, title, content) VALUES
  ('main', 'Evartinvest', ''),
  ('about', 'О нас', ''),
  ('marketing', 'Маркетинг', ''),
  ('rules', 'Правила', ''),
  ('contacts', 'О нас', '');


CREATE TABLE "review" (
  id        SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24)        NOT NULL,
  content   TEXT               NOT NULL,
  CONSTRAINT review_user FOREIGN KEY (user_name)
  REFERENCES "user" (name)
  ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX review_id ON "review" USING BTREE ("id");


CREATE TABLE "faq" (
  "id"       SERIAL PRIMARY KEY NOT NULL,
  "number"   SMALLINT,
  "question" VARCHAR(256)       NOT NULL,
  "answer"   TEXT               NOT NULL
);


CREATE TABLE "setting" (
  "category" VARCHAR(24) NOT NULL,
  "name"     VARCHAR(24) NOT NULL,
  "type"     VARCHAR(24),
  "value"    TEXT
);

INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'email', 'info@evartinvest.com');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'login_fails', 5);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'skype', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'google', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'yandex', '33309878');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'heart', NULL);
INSERT INTO "setting" VALUES ('common', 'withdrawal_confirmation', 'bool', TRUE);
INSERT INTO "setting" VALUES ('common', 'perfect', 'bool', TRUE);
INSERT INTO "setting" VALUES ('common', 'nix', 'bool', TRUE);
INSERT INTO "setting" VALUES ('common', 'card', 'bool', TRUE);

INSERT INTO "setting" ("category", "name", "value") VALUES ('payment', 'login', 'evart04');
INSERT INTO "setting" ("category", "name", "value") VALUES ('payment', 'password', 'Evart2016');
INSERT INTO "setting" ("category", "name", "value") VALUES ('sms', 'id', 'ACa0f702412fccdc716b91bdb1576c135f');
INSERT INTO "setting" ("category", "name", "value") VALUES ('sms', 'token', '41e6156eb10cbe9a9e744615c0890973');
INSERT INTO "setting" ("category", "name", "value") VALUES ('sms', 'number', '+12014196015');

INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'id', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'password', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('nix', 'wallet', NULL);

CREATE TABLE "payapp" (
  "username"             VARCHAR(24)  NOT NULL,
  "id"                   VARCHAR(24) PRIMARY KEY,
  "personCode"           VARCHAR(24),
  "natioonality"         VARCHAR(5)   NOT NULL,
  "address"              VARCHAR(100) NOT NULL,
  "zipCode"              VARCHAR(16)  NOT NULL,
  "city"                 VARCHAR(200) NOT NULL,
  "country"              VARCHAR(5)   NOT NULL,
  "idNumber"             VARCHAR(5)   NOT NULL,
  "passportType"         VARCHAR(5)   NOT NULL,
  "activityAndIncome"    VARCHAR(5)   NOT NULL,
  "companyName"          VARCHAR(100),
  "businessType"         VARCHAR(5)   NOT NULL,
  "businessTypeOther"    VARCHAR(100),
  "companyWebPage"       VARCHAR(255),
  "accountUsage"         VARCHAR(20)  NOT NULL,
  "openingPurpose"       VARCHAR(5)   NOT NULL,
  "openingPurposeOther"  VARCHAR(100),
  "monthlyTurnOver"      VARCHAR(5)   NOT NULL,
  "firstTopUp"           VARCHAR(5)   NOT NULL,
  "firtsTopUpConnection" VARCHAR(5)   NOT NULL,
  "otherTopUp"           VARCHAR(5)   NOT NULL,
  "otherTopUpConnection" VARCHAR(5)   NOT NULL,
  "deliveryInfo"         VARCHAR(5)   NOT NULL,
  "ip"                   VARCHAR(20)  NOT NULL,
  CONSTRAINT user_name FOREIGN KEY (username)
  REFERENCES "user" ("name")
);

CREATE TABLE "referral_visit" (
  user_name VARCHAR(24) NOT NULL,
  ip        INET        NOT NULL,
  time      TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  url       VARCHAR(160),
  CONSTRAINT "unique_visit" UNIQUE ("user_name", "ip"),
  CONSTRAINT "referral_visit_name" FOREIGN KEY ("user_name") REFERENCES "user" ("name")
  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE "visit_agent" (
  "id"    SERIAL PRIMARY KEY,
  "ip"    INET,
  "agent" TEXT,
  CONSTRAINT agent UNIQUE (ip, agent)
);
CREATE UNIQUE INDEX visit_agent_id ON "visit_agent" USING BTREE (id);
CREATE UNIQUE INDEX visitor ON "visit_agent" USING BTREE (ip, agent);

CREATE TYPE http_method AS ENUM ('GET', 'POST', 'PUT', 'PATCH', 'OPTIONS', 'HEAD');

CREATE TABLE "visit_path" (
  "id"        BIGINT PRIMARY KEY,
  "agent_id"  INT,
  "method"    http_method,
  "path"      TEXT,
  "user_name" VARCHAR(24),
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES "visit_agent" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE,
  --   CONSTRAINT user_name FOREIGN KEY (user_name)
  --   REFERENCES "user" ("name")
  --   ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "premium" (
  sum DOUBLE PRECISION NOT NULL
)
  INHERITS (journal);
