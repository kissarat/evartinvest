<?php

define('SSL_KEY_DIR', __DIR__ . '/../private');

if (!file_exists(SSL_KEY_DIR . '/private.pem')) {
    throw new \yii\base\Exception('Private file does not exists');
}

if (!file_exists(SSL_KEY_DIR . '/public.pem')) {
    throw new \yii\base\Exception('Public file does not exists');
}

return [
    'keys' => [
        'private' => openssl_get_privatekey(file_get_contents(SSL_KEY_DIR . '/private.pem')),
        'public' => openssl_get_publickey(file_get_contents(SSL_KEY_DIR . '/public.pem'))
    ]
];
