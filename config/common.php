<?php
if (empty($_COOKIE['lang'])) {
    $_COOKIE['lang'] = 'en';
    setcookie('lang', $_COOKIE['lang'], time() + 3600 * 24 * 30);
}

$admin = isset($_SERVER['HTTP_HOST']) && false !== strpos($_SERVER['HTTP_HOST'], 'vg6wa4');

$config = [
    'id' => 'evartinvest',
    'name' => 'Evart Invest',
    'timeZone' => 'Europe/Moscow',
    'basePath' => __DIR__ . '/..',
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/' . ($admin ? 'statistics' : 'index'),
    'language' => $_COOKIE['lang'],
    'charset' => 'utf-8',
    'layout' => $admin ? 'admin' : 'main',
    'components' => [
        'cache' => [
            'class' => extension_loaded('apc') ? 'yii\caching\ApcCache' : 'yii\caching\FileCache',
            'keyPrefix' => 'q'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'enabled' => false
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'sms/<user:[\w_\-\.]+>' => 'user/sms',
                'journal/user/<user_name:[\w_\-\.]+>' => 'journal/index',
                'feedback/template/<template:\w+>' => 'feedback/create',
                'invoices/user/<user:[\w_\-\.]+>' => 'invoice/invoice/index',
                'invoice/<id:[\w\-]+>' => 'invoice/invoice/view',
                'journal/<id:\d+>' => 'journal/view',
                'profile/<name:[\w_\-\.]+>' => 'user/view',
                'ticket/<ticket:\w+>' => 'feedback/feedback/ticket',
                'visit/<user_name:\w+>' => 'user/visit',
                'edit/<name:[\w_\-]+>' => 'user/update',
                'cases/<name:[\w_\-]+>' => 'user/plans',
                'history/<name:[\w_\-]+>' => 'user/history',
                'feedback/<name:[\w_\-]+>' => 'user/feedback',
                'line/<name:[\w_\-]+>/<number:\d>' => 'user/line',
                'referrals/<user:[\w_\-]+>' => 'user/referral',
                'graph/user/<parent_id:[\w_\-]+>' => 'user/graph',
                'password/<name:[\w_\-\.]+>' => 'user/password',
                'reset/<code:[\w_\-]{64}>' => 'user/password',
                'ref/<ref:[\w_\-\.]+>' => 'user/signup',
                'card/<name:[\w_\-]+>' => 'user/business-card',
                'wallet/<name:[\w_\-]+>/create' => 'invoice/evart/create',
                'process/<name:[\w_\-]+>' => 'invoice/evart/process',
                'tickets' => 'feedback/feedback/index',
                'settings' => 'setting/index',
                'login' => 'user/login',
                'signup' => 'user/signup',
                'cabinet' => 'user/view',
                'profit' => 'user/account',
                'contact-us' => 'feedback/feedback/create',
                'statistics' => 'home/statistics',
                'visits' => 'admin/visit',
                'unavailable' => 'home/unavailable',
            ],
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'enableCaching' => true
                ]
            ]
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'name' => 'auth'
        ],

        'backup' => [
            'class' => 'app\components\Backup',
        ],

        'evart' => [
            'class' => 'app\modules\invoice\components\EvartMerchant',
            'id' => 'Evartinvest1463045717175'
        ]
    ],
    'params' => null,
];
