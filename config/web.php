<?php

use app\behaviors\Journal;

$config['components']['user'] = [
    'identityClass' => 'app\models\User',
    'enableAutoLogin' => true,
    'loginUrl' => ['user/login'],
    'on afterLogin' => function($event) {
        /* @var $user \app\models\User */
        $user = Yii::$app->user->identity;
        $event->name = 'login';
        $event->sender = $user;
        Journal::writeEvent($event);
        setcookie('user', $user, FOREVER, '/');
        if ($user->isManager()) {
            setcookie('role', 'manage');
        }
        if (!empty($user->language)) {
            setcookie('lang', $user->language, FOREVER, '/');
        }
    },
    'on beforeLogout' => function($event) {
        $event->name = 'logout';
        $event->sender = Yii::$app->user->identity;
        Journal::writeEvent($event);
    },
];

$config['components']['errorHandler'] = [
    'errorAction' => 'home/error',
];

if ('main' == $config['layout']) {
    $config['components']['assetManager'] = [
        'bundles' => [
//            'yii\bootstrap\BootstrapPluginAsset' => [
//                'js' => []
//            ],
            'yii\bootstrap\BootstrapAsset' => [
                'css' => [],
            ],

        ],
    ];
}

$config['components']['formatter'] = [
    'class' => 'yii\i18n\Formatter',
    'dateFormat' => 'php:d-m-Y',
    'datetimeFormat' => 'php:d-m-Y H:i',
    'timeFormat' => 'php:H:i:s',
];

//$modules = [
//    'test' => [
//        'enabled' => false
//    ]
//];

$modules_dir = __DIR__ . '/../modules';
foreach(scandir($modules_dir) as $module) {
//    $enabled = isset($modules['test']['enabled'])
//        ? $modules['test']['enabled'] : true;
    if ('.' != $module[0]) {
        $config['modules'][$module] = "app\\modules\\$module\\Module";
        $config['bootstrap'][] = $module;
    }
}


if (YII_ENV_DEV) {
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['bootstrap'][] = 'gii';
    $config['bootstrap'][] = 'debug';
}
