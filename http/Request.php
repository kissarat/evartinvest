<?php

namespace app\http;


use Yii;

class Request extends \yii\httpclient\Request
{
    public function send()
    {
        $time = time();
        file_put_contents(Yii::getAlias("@app/log/$time.json"), $this->toString());
        return parent::send();
    }

    public function toString()
    {
        $result = [strtoupper($this->getMethod()) . ' ' . $this->getUrl()];
        if ($this->hasHeaders()) {
            $headers = $this->composeHeaderLines();
            $result[]= implode("\n", $headers);
        }
        $result = implode("\n", $result);

        $content = $this->getContent();
        if ($content !== null) {
            $result .= "\n\n" . json_encode($content);
        }

        return $result;
    }
}
