<html>
<head>
    <title>Server is available</title>
</head>
<body>
<h1>Server is available</h1>
<hr/>
<div>
    <strong>Your IP is:</strong>
    <?= $_SERVER['REMOTE_ADDR'] ?>
</div>
<div>
    <strong>Your browser is:</strong>
    <?= $_SERVER['HTTP_USER_AGENT'] ?>
</div>
<div>
    <strong>Time:</strong>
    <?= date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) ?>
</div>
<div>
    <a href="https://join.skype.com/ohM6uVX0uhBS">Skype Support</a>
</div>
</body>
</html>
