<?php

//if (!in_array($_SERVER['REMOTE_ADDR'], ['188.163.32.222', '91.198.10.204', '159.148.92.233'])) {
//	exit();
//}

if (!function_exists('uuid_create')) {
    function uuid_create() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}

define('CONFIG', __DIR__ . '/../config');

use yii\web\Application;

require_once CONFIG . '/boot.php';
require_once CONFIG . '/web.php';

$app = new Application($config);
$app->run();
