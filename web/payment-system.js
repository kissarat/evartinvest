addEventListener('load', function () {
    PaymentSystem.register();
});

var PaymentSystem = {
    register: function () {
        var registration = $('.evart-process .registration');
        registration.show();
        PaymentSystem.step('register', function (response) {
            registration.hide();
            if (response.registrationId) {
                var registrationID = $('.evart-process .registrationID');
                registrationID
                    .html(registrationID.html() + ': ' + response.registrationId)
                    .show();
            }
            if (response.success || response.registrationId) {
                PaymentSystem.addinfo();
            }
        });
    },

    addinfo: function () {
        var addinfo = $('.evart-process .addinfo');
        addinfo.show();
        PaymentSystem.step('addinfo', function (response) {
            if (response.success) {
                addinfo.html('Success <a href="' + response.extensiveInfoLink + '">'
                    + response.extensiveInfoLink + '</a>');
            }
        });
    },

    step: function (name, cb) {
        var params = $.param({
            name: evart_user_name,
            step: name
        });
        $.getJSON('/invoice/evart/process?' + params, function (response) {
            if (!response.success) {
                $('.evart-process .error').html('Error: ' + response.errorDescription);
            }
            cb(response);
        });
    }
};
