<?php
$expire = isset($_GET['expire']) ? (int) $_GET['expire'] : 1896127200; // Fri Feb 01 2030 00:00:00 GMT+0200 (EET)
setcookie($_GET['name'], $_GET['value'], $expire, '/');

if ($_GET['go']) {
    header('location: ' . $_GET['go']);
}
