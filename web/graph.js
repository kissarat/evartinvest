"use strict";

var root = null;

function build_graph(source) {
    svg = null;
    document.querySelector('article').innerHTML = '';
    var wscale = width_scale.value / 20;
    var width = Math.round(innerWidth * wscale * 2) - margin.right - margin.left;
    var height = innerHeight * 2 - margin.top - margin.bottom;

    var i = 0;

    svg = d3.select("article").append("svg")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var tree = d3.layout.tree()
        .size([height, width]);

    var diagonal = d3.svg.diagonal()
        .projection(function (d) {
            return [d.x * wscale, d.y];
        });

    // Compute the new tree layout.
    var nodes = tree.nodes(root).reverse(),
        links = tree.links(nodes);

    // Normalize for fixed-depth.
    nodes.forEach(function (d) {
        d.y = d.depth * height_scale.value;
    });

    // Declare the nodes
    var node = svg.selectAll("g.node")
        .data(nodes, function (d) {
            return d.id || (d.id = ++i);
        });

    // Enter the nodes.
    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function (d) {
            return "translate(" + (d.x * wscale) + "," + d.y + ")";
        });

    nodeEnter.append("circle")
        .attr("r", 8)
        .style("fill", function (d) {
            return d.reinvest_from ? "#f00" : '#fff';
        });

    nodeEnter.append("text")
        .attr("x", function (d) {
            return d.children || d._children ? -13 : 13;
        })
        .attr("dy", ".35em")
        .attr("text-anchor", function (d) {
            return d.children || d._children ? "end" : "start";
        })
        .text(function (d) {
            return d.name;
        })
        .style("fill-opacity", 1)
        .attr('transform', 'rotate(' + angle.value + ')');

    // Declare the links
    var link = svg.selectAll("path.link")
        .data(links, function (d) {
            return d.target.id;
        });

    // Enter the links.
    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", diagonal);

    //$each('.node', function(node) {
    //    var current = /([\d\.]+),([\d\.]+)/.exec(node.getAttribute('transform')) || ['', 0, 0];
    //    node.x = current[0];
    //    node.y = current[1];
    //});

    jQuery('circle')
        .on('mouseover', function () {
            this.parentNode.querySelector('text').style.fontWeight = 'bold';
            this.parentNode.classList.add('big');
            this.setAttribute('r', 160);
            setTimeout(little.bind(this), 600);
        })
        .on('mouseout', function () {
            this.parentNode.querySelector('text').style.removeProperty('font-weight');
            little.call(this);
        })
        .on('click', function () {
            var text = this.nextElementSibling.innerHTML;
            var id = /\((\d+)\)/.exec(text);
            if (id) {
                id = id[0];
            }
            else {
                id = text
            }
            var routes = location.pathname.split('/');
            if (id) {
                routes[routes.length - 1] = id;
                location.pathname = routes.join('/');
            }
        });

    var up = document.getElementById('up');
    var routes = location.pathname.split('/');
    up.setAttribute('href', '/' + ['graph', routes[2], up.dataset.id].join('/'));
    up.style.removeProperty('display');
}

var margin = {top: 80, right: 20, bottom: 20, left: 80};

var svg;

function render() {
    build_graph(root);
    localStorage[this.id] = this.value;
    this.parentNode.querySelector('.value').innerHTML = this.value;
}

['angle', 'width_scale', 'height_scale'].forEach(function (prop) {
    var range = window[prop];
    range.onchange = render;
    if (prop in localStorage) {
        range.value = localStorage[prop];
    }
});

function little() {
    this.parentNode.classList.add('out');
    this.parentNode.classList.remove('big');
    this.setAttribute('r', 12);
}

