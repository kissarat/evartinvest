<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'evart');

/** MySQL database username */
define('DB_USER', 'evart');

/** MySQL database password */
define('DB_PASSWORD', 'evart');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q^#<Om}@${i|)`EcXE9kh8klALzI)^KTioM#}#Nq`g5bt3>vZid9qix(}IE1#lmv');
define('SECURE_AUTH_KEY',  'snB,}*KHU8nHwV7,iO*JzXgY&EA!M3frz&SP55]R9A)@j=eHp^ENYR~HX?Kr7Dfr');
define('LOGGED_IN_KEY',    '9V)1odWl<#PXO,sz`j0u; -p>uMq6i9y,7r*W++dytrN=Q38e*Gk>!%`,kLGz90L');
define('NONCE_KEY',        '{np8eUOwIo-WzL_0abqW8AyOb;#!+sWa/)bn$ -85%t}lp&DcfATDA<}VTI_UQ2I');
define('AUTH_SALT',        'AMyX?w,A1|4w@*/>8m9WbMBo|4nb;ndU6e3|j#Ex12rpn.vkg_:)uvb#PaUv;Ak+');
define('SECURE_AUTH_SALT', ' #$=9_2H;4;H2w=.^od=Z}Ff6U])~k(IsOh4g[0~ >PHL?3s2j~U.1]mQ_+,HvS1');
define('LOGGED_IN_SALT',   '{+C.aDa)RN!Igz^LV$E@YS5b}kv]j+YZY`;1+< k/|eySvgkrPtN}z@3:SOVNp#Q');
define('NONCE_SALT',       'Tq&%dZ|vfJ{,nJDh85&O~Rm`37vdmBOy.zHiY?V?mHB~:<BJ5tte6Wo`$1f:iAM5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
