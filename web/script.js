var referral = /^\/ref\/(.*)/.exec(location.pathname);
if (referral) {
    referral = referral[1];
    var url = document.referrer;
    var options = {
        type: 'POST',
        url: '/visit/' + referral
    };
    if (!url || url.indexOf('evartinvest') < 0) {
        options.data = url;
    }
    $.ajax(options);

    localStorage.referral = referral;
}
else if (localStorage.referral && '/signup' == location.pathname) {
    location.pathname = '/ref/' + localStorage.referral;
}

if (location.pathname.indexOf('/home/index') == 0) {
    location.pathname = '/';
}

addEventListener('load', function () {

    if (localStorage.referral) {
        $('[href$="/signup"]').each(function (i, anchor) {
            anchor.setAttribute('href', anchor.getAttribute('href').replace('/signup', '/ref/' + localStorage.referral))
        });
    }

    var errorPage = document.querySelector('.home-error');
    if (errorPage && 403 == errorPage.getAttribute('data-code')) {
        errorPage.querySelector('.message').innerHTML = 'Loading cabinet...';
        location.pathname = '/login';
    }

    var signupForm = $('.user-signup form');
    signupForm.on('beforeSubmit', function () {
        var dialog = $('#confirm-sponsor-dialog');
        var agree = $('#accept-agreement').prop('checked');
        if (agree) {
            $('[data-error=accept-agreement]').hide();
            // $.get('/card/' + $('#user-ref_name').val(), function (data) {
            //     dialog
            //         .html(data)
            //         .dialog('open');
            // });
            // var isOpened = dialog.dialog('isOpen');
            // if (isOpened) {
            //     dialog.dialog('close');
            // }
            return true || isOpened;
        }
        else {
            $('[data-error=accept-agreement]').show();
        }
        return false;
    });

    $('button.submit').click(function (e) {
        var selector = e.target.dataset.form;
        $(selector).submit();
    });

    // This functionality is not available right now
    if (document.cookie.indexOf('dev') < 0) {
        $('[href*=payapp]').attr('href', '/unavailable');
        $('[href*=invoice]').attr('href', '/unavailable');
    }
    else {
        $('.dev').show();
    }

    $('.menu > .sponsor').click(function () {
        $('#business-card-dialog').show();
    });

    $('.close[data-selector]').click(function () {
        $(this.dataset.selector).hide();
    });

    var menu = $('.menu');
    if (0 == menu.find('.active').length) {
        menu.find('li a').each(function (i, anchor) {
            if (location.pathname == anchor.getAttribute('href')) {
                anchor.parentNode.classList.add('active');
            }
        })
    }

    // open ticket by id
    $('#ticket button').click(function () {
        window.location = '/ticket/' + ticket.find('input').val();
    });

    $('#image-view')
        .click(function () {
            this.style.display = 'none';
        })
        .on('onkeyup', function () {
            this.style.display = 'none';
        });

    $('.preview img').click(function () {
        var imageView = $$('#image-view img');
        imageView.setAttribute('src', this.getAttribute('src'));
        imageView.parentNode.style.removeProperty('display');
    });

    $('[type=file].preview').change(function () {
        var reader = new FileReader();
        var avatar = $id('avatar');
        avatar.style.removeProperty('display');
        avatar.setAttribute('class', 'avatar');
        avatar.innerHTML = 'Загрузка картинки...';
        reader.onload = function (e) {
            avatar.style.backgroundImage = 'url(' + e.target.result + ')';
            avatar.innerHTML = '';
        };
        reader.readAsDataURL(this.files[0]);
    });

    $(".menu-toogle").click(function () {
        $(".menu-toogle").hide();
        $(".avatar").show(500);
        $(".panel").show(500);
        $(".menu-toogle2").show(0);
        $(".panel-bg").show(0);
    });

    $(".menu-toogle2").click(function () {
        $(".menu-toogle").show();
        $(".avatar").hide(500);
        $(".panel").hide(500);
        $(".menu-toogle2").hide(0);
        $(".panel-bg").hide(0);
    });

    $('.generate-password').click(function () {
        var password = $(this.dataset.selector);
        password.value = (Math.random() * Number.MAX_SAFE_INTEGER).toString(36);
    });

    if (getTimeZone()) {
        setupMoments();
    } else {
        $.getJSON('/timezones.json', function (timezones) {
            var offset = new Date().getTimezoneOffset();
            // window.timezones = timezones;
            setTimeZone(timezones[offset], offset);
            setupMoments();
        });
    }

    var phone = document.getElementById('user-phone');
    if (phone) {
        $.getJSON('/countries.json', function (countries) {
            var codes = [];
            countries.forEach(function (country) {
                if (country.code) {
                    codes.push(country.code);
                }
            });
            codes.sort(function (a, b) {
                return b - a;
            });
            phone.addEventListener('change', function () {
                var value = phone.value.replace(/[^\d]/g, '');
                for (var i = 0; i < codes.length; i++) {
                    if (0 === value.indexOf(codes[i].toString())) {
                        return;
                    }
                }
                alert([
                    'The phone number must starts with country code',
                    'Номер телефона должен начинаться с кода страны',
                    'El número de teléfono debe comienza con código de país'
                ].join('\n'));
            })
        });
    }
});

function once(target, event, handler) {
    if ('string' == typeof target) {
        target = document.querySelector(target);
    }
    if (!target) {
        return false
    }
    var _handler = function (e) {
        target.removeEventListener(event, _handler);
        return handler(e);
    };
    return true;
}

function register(target, events, bind) {
    if ('string' == typeof target) {
        target = document.querySelector(target);
    }
    if (!target) {
        return false;
    }
    for (var name in events) {
        var handler = events[name];
        if (bind) {
            handler = handler.bind(events);
        }
        target.addEventListener(name, events[name]);
    }
    return true;
}

function confirmSponsor(value) {
    if (value) {
        $('.user-signup form').submit();
    }
    else {
        $('#confirm-sponsor-dialog').dialog('close');
    }
}

function getCookie(regex) {
    var value = regex.exec(document.cookie);
    if (value) {
        value = value[1];
    }
    return value;
}

function setCookie(name, value, expire) {
    document.cookie = name + '=' + value + '; path=/; max-age=' + expire;
}

function getTimeZone() {
    return getCookie(/timezone=(\w\/\w)/i);
}

function getLanguage() {
    return getCookie(/lang=(en|ru)/i).toLowerCase();
}

function setTimeZone(name, timezone_offset) {
    setCookie('timezone', name, 7200);
    if (timezone_offset) {
        localStorage.timezone_offset = timezone_offset;
    }
}

function setupMoments() {
    var moments = document.querySelectorAll('.moment');
    if (moments.length > 0) {
        var timezone_offset = parseInt(localStorage.timezone_offset);
        $.getScript('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js', function () {
            [].forEach.call(moments, function (m) {
                var time = moment(m.textContent, 'DD-MM-YYYY h:m');
                time.add(timezone_offset, 'minutes');
                var duration = moment.duration(time.diff());
                m.textContent = time.fromNow();
                if (duration.asHours() < 18) {
                    m.textContent = time.fromNow();
                    if (duration.asMinutes() < 5) {
                        m.classList.add('online');
                    }
                }
            });
        });
    }
}

var chooseLanguage = document.getElementById('choose-language');
if (chooseLanguage) {
    chooseLanguage.value = getLanguage();
    chooseLanguage.addEventListener('change', function () {
        setCookie('lang', this.value, 3600 * 24 * 120);
        location.reload();
    });
}

function preventDefault(e) {
    e.preventDefault();
}

var user_name = document.querySelector('#cabinet');
if (user_name) {
    user_name = user_name.getAttribute('data-user');
}
var avatarDiv = document.querySelector('.container > .avatar');
if (avatarDiv) {
    avatarDiv.addEventListener('dragenter', preventDefault);
    avatarDiv.addEventListener('dragover', preventDefault);

    avatarDiv.addEventListener('drop', function (e) {
        e.preventDefault();
        var file = e.dataTransfer.files[0];
        if (file) {
            var form = new FormData();
            form.append('file', file);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/user/image?name=' + user_name);
            xhr.onload = function () {
                if (200 == xhr.status) {
                    var response = JSON.parse(xhr.responseText);
                    if (response.success) {
                        location.reload();
                    }
                }
            };
            xhr.send(form);
        }
    }, false);
}

function micro() {
    return start + Math.round(performance.now() * 200) + Math.round(Math.random() * 10);
}

var start = Date.now() * 1000 + Math.round(1000 * Math.random());

function selectToObject() {
    var result = {};
    [].forEach.call(document.querySelectorAll('select'), function (select) {
        var s = result[select.name] = {};
        [].forEach.call(select.querySelectorAll('option'), function (option) {
            s[option.value] = option.innerHTML;
        })
    });
    return result;
}
