<?php

namespace app\helpers;


use app\models\Settings;
use Yii;
use yii\i18n\Formatter;

class God {
    public static function generateRandomLiteral($length = 32) {
        $bytes = openssl_random_pseudo_bytes($length);
        return strtr(substr(base64_encode($bytes), 0, $length), '+/', 'A_');
    }

    public static function format() {
        return new Formatter();
    }

    public static function mail($email, $template, &$params = []) {
        $params = array_merge([
            'receiver_email' => $email,
            'sender_email' => 'info@evartinvest.com', #Settings::get('common', 'email'),
            'sender_name' => Yii::$app->name,
            'subject' => Yii::$app->name
        ], $params);
//        try {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$params['sender_email'] => $params['sender_name']])
                ->setSubject($params['subject'])
                ->setHtmlBody(Yii::$app->view->render("@app/mails/$template", $params))
                ->send();
//        }
//        catch (\Exception $ex) {
//
//        }
        return $params;
    }

    public static $_cached = [];

    public static function &loadObject($filename) {
        if (empty(static::$_cached[$filename])) {
            static::$_cached[$filename] = json_decode(file_get_contents(Yii::getAlias($filename)));;
        }
        return static::$_cached[$filename];
    }

    public static function getCountries() {
        return static::loadObject('@app/web/countries.json');
    }

    public static function &getCountriesField($name) {
        $values = [];
        foreach (static::getCountries() as $country) {
            if (isset($country->$name)) {
                $values[] = $country->$name;
            }
        }
        return $values;
    }

    public static function &getCountriesNames() {
        $countries = static::getCountries();
        $names = [];
        foreach ($countries as $country) {
            $names[$country->iso] = $country->name;
        }
        return $names;
    }
}
