<?php
/**
 * @link http://zenothing.com/
 */

namespace app\behaviors;


use app\JournalException;
use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\Controller;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Access extends Behavior {
    public $admin;
    public $manager;
    public $plain;
    public $command;
    public $guest;

    public function events() {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction'
        ];
    }

    public function beforeAction(ActionEvent $event) {
        $allow = $this->check($event->action->id);
        if (!$allow) {
            throw new ForbiddenHttpException("Forbidden");
        }
        return $allow;
    }

    public function check($action) {
        $user = Yii::$app->user;
        $roles = ['admin', 'manager', 'plain', 'command', 'guest'];
        $role = null;
        foreach($roles as $role) {
            $actions = $this->$role;
            if (is_array($actions) && in_array($action, $actions)) {
                break;
            }
        }
        if ('guest' == $role) {
            return true;
        }
        if ($user->isGuest) {
            return false;
        }
        if ('plain' == $role) {
            return true;
        }
        if ('command' == $role && $user->identity->isCommand()) {
            return true;
        }
        if ('manager' == $role && $user->identity->isManager()) {
            return true;
        }
        if ($user->identity->isAdmin()) {
            return true;
        }
        return false;
    }
}
