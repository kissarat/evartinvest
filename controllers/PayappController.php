<?php

namespace app\controllers;

use app\behaviors\Access;
use app\models\Payapp;
use app\models\Settings;
use app\models\User;
use Yii;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class PayappController extends Controller
{


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/login');
        }
        $user = Yii::$app->user->identity;
        $model = new Payapp();
        $file = file_get_contents('files/countries');
        $all = explode(';', $file);
        $short = [];
        $name = [];
        foreach ($all as $key => $item) {
            $temp = explode(',', $item);

            $name[] = $temp[0];
            $short[] = $temp[1];

            unset($temp);
        }
        $countries = array_combine($short, $name);
        unset($all);
        unset($short);
        unset($name);
        if ($model->load(Yii::$app->request->post())) {
            $model->username = $user;
            $model->id = User::findOne(['name' => $user])->paymentreg;
            $model->ip = Yii::$app->request->getUserIP();
            $request = [
                'applicationId' => $model->id,
                'personCode' => $model->personCode,
                'nationality' => $model->natioonality,
                'address1' => $model->address,
                'address2' => '',
                'zipCode' => $model->zipCode,
                'city' => $model->city,
                'country' => $model->country,
                'allowContact' => $model->contact,
                'passportType' => $model->passportType,
                'idNumber' => $model->idNumber,
                'activityAndIncome' => $model->activityAndIncome,
                'companyName' => $model->companyName,
                'businessType' => $model->businessType,
                'businessTypeOther' => $model->businessTypeOther,
                'companyWebPage' => $model->companyWebPage,
                'accountUsage' => $model->accountUsage,
                'openingPurpose' => $model->openingPurpose,
                'openingPurposeExt' => $model->openingPurposeOther,
                'monthlyTurnOver' => $model->monthlyTurnOver,
                'firstTopUp' => $model->firstTopUp,
                'firstTopUpConnection' => $model->firtsTopUpConnection,
                'otherTopUp' => $model->otherTopUp,
                'otherTopUpConnection' => $model->otherTopUpConnection,
                'companyInLatvia' => $model->inLatvia,
                'politican' => $model->politican,
                'deliveryInformation' => $model->deliveryInfo,
                'ipAddress' => $model->ip,
            ];
            $request = $this->post_to_url('http://89.111.51.250:8090/fsc-integration/counterpartyapi/addinfo',$request);
            if(!$request){
                return 'Application error';
            }
            if ($model->save()) {
                return $this->render('success',[
                    'url'=>$request['extensiveInfoLink']
                ]);
            } else {
                print_r($model->getErrors());
                echo 'ErrorException';
            }
        } else {
            return $this->render('form', [
                'model' => $model,
                'countries' => $countries
            ]);
        }
    }
    private function post_to_url($url, $data)
    {
        $cookie_filename = str_replace('/', '+', base64_encode(openssl_random_pseudo_bytes(16))) . '.' . time();
        $cookie_filename = "/tmp/$cookie_filename.cookie";
        echo $cookie_filename;

        $ch = curl_init('http://89.111.51.250:8090/fsc-integration/login');
        $maindata = [
            'username' => Settings::get('payment','login'),
            'password' => Settings::get('payment','password')
        ];
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_filename);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($maindata));
        $response = curl_exec($ch);
        //  echo $response;
        curl_close($ch);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_filename);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($ch);
        $response = json_decode($response,true);
        var_dump($response);
        curl_close($ch);
        if($response['success']==1) {
            return $response;
        } else {
            return false;
        }
    }
}