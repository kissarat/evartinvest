<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;

use app\behaviors\Access;
use app\models\Record;
use app\models\search\Record as JournalSearch;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * JournalController implements the CRUD actions for Journal model.
 */
class JournalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'view']
            ],

            'cache' => [
                'class' => 'yii\filters\HttpCache',
                'cacheControlHeader' => 'must-revalidate, private',
                'enabled' => false,
                'only' => ['index'],
                'lastModified' => function ($action, $params) {
                    $query = Record::find();
                    if (isset($params['user'])) {
                        $query->where(['user_name' => $params['user']]);
                    }
                    return strtotime($query->max('time'));
                },
            ],
        ];
    }

    public function beforeAction($action)
    {
        /** @var User $me */
        $me = Yii::$app->user->identity;
        if (in_array($action, ['index', 'view']) && !$me->isManager()) {
            Yii::$app->layout = 'cabinet';
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $searchModel = new JournalSearch();

        $params = Yii::$app->request->queryParams;
//        if ($user_name) {
//            $params['user_name'] = $user_name;
//        }
        /** @var User $me */
        $me = Yii::$app->user->identity;
        if (!$me->isManager() && $params['user_name'] != $me->name) {
            return $this->redirect(['index', 'user_name' => $me->name]);
        }
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionObject($id)
    {
        $query = Record::find()->where(['object_id' => $id]);
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ])
        ]);

    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Journal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Record the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Record::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
