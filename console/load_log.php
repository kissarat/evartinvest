<?php
require_once 'pdo.php';

$db = connect();

$file = $argv[1];

$file = fopen($file, 'r');
while (($line = fgets($file)) !== false) {
    $line = explode('  ', $line);
    $path = explode(' ', $line[1]);
    $line = [
        'ip' => $line[0],
        'method' =>  $path[0],
        'path' => $path[1],
//        'spend' => $line[2],
        'id' => $line[2],
        'user_name' => $line[3],
        'agent' => $line[4],
    ];

    foreach(['method', 'path', 'agent', 'user_name'] as $field) {
        if ('-' == trim($line[$field])) {
            $line[$field] = null;
        }
    }

    $agent = [
        ':agent' => $line['agent'],
        ':ip' => $line['ip']
    ];

    $agent_id = null;

    try {
        $st = $db->prepare('INSERT INTO "visit_agent" (agent, ip) VALUES (:agent, :ip) RETURNING id');
        $st->execute($agent);
        $agent_id = $st->fetchColumn();
    }
    catch (Exception $ex) {
        $st = $db->prepare('SELECT id FROM "visit_agent" WHERE agent = :agent AND ip = :ip');
        $st->execute($agent);
        $agent_id = $st->fetchColumn();
    }

    try {
        $st = $db->prepare('INSERT INTO "visit_path" (id, agent_id, method, path, user_name) VALUES (:id, :agent_id, :method, :path, :user_name)');
        $st->execute([
            ':id' => $line['id'],
            ':agent_id' => $agent_id,
            ':method' => $line['method'],
            ':path' => $line['path'],
//            ':spend' => $line['spend'],
            ':user_name' => $line['user_name'],
        ]);
    }
    catch (Exception $ex) {
        echo $ex->getMessage();
        var_dump($line);
    }
}
