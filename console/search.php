<?php
function scan($parent) {
    $entries = scandir($parent);
    foreach ($entries as $entry) {
        if ('.' == $entry[0]) {
            continue;
        }
        $entry = realpath($parent . DIRECTORY_SEPARATOR . $entry);
        if (is_dir($entry) && 'vendor' != $entry && !is_link($entry)) {
            scan($entry);
        }
        else if (preg_match('/\.php$/', $entry)) {
            $lines = file_get_contents($entry);
            $lines = explode("\n", $lines);
            $match_lines = [];
            foreach ($lines as $i => $line) {
                if (preg_match('/\([ \'"]+app[^\)]+\)/', $line, $matches)) {
                    $match = $matches[0];
//                    $line = str_replace($match, '\033[01;31m ' . $match . ' \033[0m', $line);
                    $match_lines[] = "\t$i\t$line";
                }
            }
            if (count($match_lines) > 0) {
                echo "\n$entry\n";
                print implode("\n", $match_lines);
            }
        }
    }
}

scan(__DIR__ . '/../');
