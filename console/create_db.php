<?php

require_once __DIR__ . '/../config/pdo.php';

$socket_file = '.s.PGSQL.5432';

function create_database($socket_dir) {
    global $config;
    $name = $config['components']['db']['username'];
    $password = $config['components']['db']['password'];
    pg_connect('host=' . $socket_dir);

    try {
        pg_query("DROP DATABASE \"$name\"");
    }
    catch (Exception $ex) {
        echo $ex->getMessage();
    }

    try {
        pg_query("DROP ROLE \"$name\"");
    }
    catch (Exception $ex) {
        echo $ex->getMessage();
    }

    pg_query("CREATE ROLE \"$name\" LOGIN PASSWORD '$password'");
    pg_query("CREATE DATABASE \"$name\" OWNER \"$name\"");

    pg_close();
}


foreach(['/var/run/postgresql', '/tmp'] as $socket_dir) {
    if (file_exists("$socket_dir/$socket_file")) {
        if (function_exists('pg_connect')) {
            create_database($socket_dir);
        }
        else {
            echo "== Cannot create database\n\n";
        }
        break;
    }
}

$schema = [];
foreach(['tables', 'views', 'procedure'] as $file) {
    $file = file_get_contents(ROOT . "/config/$file.sql");
//    if (strpos($file, '$$') === false) {
//        $schema = array_merge(explode(';', $file), $schema);
//    }
//    else {
        $schema[] = $file;
//    }
}

$pdo = connect();
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$pdo->beginTransaction();
foreach($schema as $sql) {
    $sql = trim($sql);
    if (!empty($sql)) {
        $pdo->exec($sql);
    }
}
$pdo->commit();
