<?php
require_once 'pdo.php';

$db = connect();

$user = $argv[1];
$hash = password_hash($argv[2], PASSWORD_DEFAULT);

$st = $db->prepare('UPDATE "user" SET "hash" = :hash WHERE "name" = :name');
$st->execute([
    ':name' => $user,
    ':hash' => $hash
]);
