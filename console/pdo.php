<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 26.03.16
 * Time: 16:34
 */
require_once __DIR__ . '/../config/local.php';

function connect() {
    global $config;
    return new PDO($config['components']['db']['dsn'],
        $config['components']['db']['username'], $config['components']['db']['password'], [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
}