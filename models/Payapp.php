<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "payapp".
 *
 * @property string $username
 * @property string $id
 * @property string $personCode
 * @property string $natioonality
 * @property string $address
 * @property string $zipCode
 * @property string $city
 * @property string $country
 * @property string $passportType
 * @property string $idNumber
 * @property string $idImageDoc
 * @property string $activityAndIncome
 * @property string $companyName
 * @property string $businessType
 * @property string $businessTypeOther
 * @property string $companyWebPage
 * @property string $accountUsage
 * @property string $businessDoc
 * @property string $openingPurpose
 * @property string $openingPurposeOther
 * @property string $monthlyTurnOver
 * @property string $firstTopUp
 * @property string $firtsTopUpConnection
 * @property string $otherTopUp
 * @property string $otherTopUpConnection
 * @property string $latviaDoc
 * @property string $deliveryInfo
 * @property string $ip
 * @property string $document
 * @property string $documentDesc
 *
 * @property User $username0
 */
class Payapp extends ActiveRecord
{
    public $contact;
    public $inLatvia;
    public $politican;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payapp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'id', 'natioonality', 'address', 'zipCode', 'city', 'country', 'passportType', 'idNumber', 'activityAndIncome', 'businessType', 'accountUsage', 'openingPurpose', 'monthlyTurnOver', 'firstTopUp', 'firtsTopUpConnection', 'otherTopUp', 'otherTopUpConnection', 'deliveryInfo', 'ip'], 'required'],
            [['username', 'id'], 'string', 'max' => 24],
            ['personCode', 'string','min'=>13,'max'=>13],
            [['natioonality', 'country', 'passportType', 'idNumber', 'activityAndIncome', 'businessType', 'openingPurpose', 'monthlyTurnOver', 'firstTopUp', 'firtsTopUpConnection', 'otherTopUp', 'otherTopUpConnection', 'deliveryInfo'], 'string', 'max' => 5],
            [['address', 'companyName', 'businessTypeOther', 'openingPurposeOther'], 'string', 'max' => 100],
            [['zipCode'], 'string', 'max' => 16],
            [['city'], 'string', 'max' => 200],
            [[ 'companyWebPage','documentDesc'], 'string', 'max' => 255],
            [['accountUsage', 'ip'], 'string', 'max' => 20],
            [['username'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username' => 'name']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'id' => 'ID',
            'personCode' => Yii::t('app','Person Code'),
            'natioonality' => Yii::t('app','Nationality'),
            'address' => Yii::t('app','Address'),
            'zipCode' => Yii::t('app','Zip Code'),
            'city' => Yii::t('app','City'),
            'country' => Yii::t('app','Country'),
            'passportType' => Yii::t('app','Passport Type'),
            'idNumber' => Yii::t('app','Passport Number'),
            'activityAndIncome' => Yii::t('app','Activity And Income'),
            'companyName' => Yii::t('app','Company Name'),
            'businessType' => Yii::t('app','Business Type'),
            'businessTypeOther' => Yii::t('app','Business Type Other'),
            'companyWebPage' => Yii::t('app','Company Web Page'),
            'accountUsage' => Yii::t('app','Account Usage'),
            'openingPurpose' => Yii::t('app','Opening Purpose'),
            'openingPurposeOther' => Yii::t('app','Opening Purpose Other'),
            'monthlyTurnOver' => Yii::t('app','Monthly Turnover'),
            'firstTopUp' => Yii::t('app','First Top Up'),
            'firtsTopUpConnection' => Yii::t('app','Firts Top Up Connection'),
            'otherTopUp' => Yii::t('app','Other Top Up'),
            'otherTopUpConnection' => Yii::t('app','Other Top Up Connection'),
            'deliveryInfo' => Yii::t('app','Delivery Info'),
            'ip' => 'Ip',
            'inLatvia' =>Yii::t('app','Is company in Latvia?'),
            'politican' =>Yii::t('app','Are you Latvia\'s politican?'),
            'contact'=>Yii::t('app','Are you agree that company has the rights to use the information displayed in the Questionnaire to offer services?')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsername0()
    {
        return $this->hasOne(User::className(), ['name' => 'username']);
    }
}
