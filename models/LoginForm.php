<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\base\Exception;
use yii\base\Model;


/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class LoginForm extends Model {
    public $name;
    public $password;
    public $remember;
    public $duration;

    public function rules() {
        return [
            [['name', 'password'], 'required'],
            ['name', 'filter', 'filter'=>'strtolower'],
            ['name', 'filter', 'filter'=>'trim'],
            ['name', 'string', 'min' => 4, 'max' => 24],
            ['password', 'string', 'min' => 1],
            ['remember', 'boolean'],
            ['remember', 'default', 'value' => true],
            ['duration', 'number', 'min' => 60],
        ];
    }

    public function scenarios() {
        return [
            'main' => ['name', 'password'],
            'admin' => ['name', 'password', 'remember', 'duration'],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'Login / Email'),
            'password' => Yii::t('app', 'Password'),
            'remember' => Yii::t('app', 'Remember Me'),
        ];
    }

    public static function login(User $user) {
        Yii::$app->user->login($user);
    }

    /**
     * @return User
     */
    public function getUser() {
        $conditions = [];
        if (false !== strpos($this->name, '@')) {
            $conditions['email'] = strtolower($this->name);
        }
        elseif (is_numeric($this->name)) {
            $conditions['phone'] = $this->name;
        }
        else {
            $conditions['name'] = strtolower($this->name);
        }
        return User::findOne($conditions);
    }
}
