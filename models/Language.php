<?php

namespace app\models;

use app\helpers\SQL;
use PDO;
use yii\db\ActiveRecord;

class Language extends ActiveRecord
{
    const SQL = 'SELECT id, native from "language" order by "order"';

    public static function getItems() {
        return SQL::queryAll(static::SQL, null, PDO::FETCH_KEY_PAIR);
    }
}
