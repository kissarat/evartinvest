<?php

namespace app\models;


use app\behaviors\Journal;
use app\helpers\God;
use app\helpers\SQL;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\IdentityInterface;
use yii\web\User as WebUser;

/**
 * Class User
 * @property integer id
 * @property string name
 * @property string auth
 * @property string email
 * @property string code
 * @property string hash
 * @property string forename
 * @property string surname
 * @property string wallet_card
 * @property string repeat
 * @property integer status
 * @property integer sex
 * @property number account
 * @property integer last_access
 * @property integer ref_name
 * @property string vip
 * @property resource data
 * @property array bundle
 * @property string image
 * @property integer rang
 * @property string city
 * @property string country
 * @property string phone
 * @property string language
 * @property string birth
 * @property string ip
 * @property string show_phone
 * @property string address
 * @property string address1
 * @property string address2
 * @property string birthday
 * @property string zipCode
 *
 * @property User referral
 * @property User[] children
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{

    const UNVERIFIED = -1;
    const BLOCKED = 0;
    const ADMIN = 1;
    const PLAIN = 2;
    const MANAGER = 3;
    const TEAM = 4;

    const NIX = '/^E\d+$/';

    public $smsCode;
    private $_password;
    private $_info;
    public $address1;
    public $repeat;
    public $adult;
    public $agreement;
    public $file;
    public $day;
    public $month;
    public $year;

    public static function tableName()
    {
        return 'user';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public static function statuses()
    {
        return [
            User::UNVERIFIED => Yii::t('app', 'Unverified'),
            User::BLOCKED => Yii::t('app', 'Blocked'),
            User::ADMIN => Yii::t('app', 'Admin'),
            User::PLAIN => Yii::t('app', 'Registered'),
            User::MANAGER => Yii::t('app', 'Manager'),
            User::TEAM => Yii::t('app', 'Team')
        ];
    }

    public static $events = [
        'login' => 'Login',
        'logout' => 'Logout',
        'login_fail' => 'Login failed',
    ];

    public function traceable()
    {
        return ['status', 'email', 'account'];
    }

    public function url()
    {
        return ['user/view', 'name' => $this->name];
    }

    public function rules()
    {
        return [
            [['name', 'email', 'forename', 'surname', 'phone'], 'required'],
            [['name', 'email', 'forename', 'surname', 'ref_name', 'phone', 'code', 'smsCode'], 'filter', 'filter' => 'trim'],
            [['name', 'email', 'ref_name'], 'filter', 'filter' => 'strtolower'],
            [['sex', 'skype', 'language'], 'filter', 'filter' => function($value) {
                return empty($value) ? null : $value;
            }],
            ['id', 'integer'],
            ['rang', 'integer'],
            ['birth', 'integer'],
            ['birthday', 'string'],
            ['language', 'string'],
            [['phone', 'smsCode'], 'filter', 'filter' => function ($value) {return preg_replace('/[^\d]/', "",$value);}],
            [['code'], 'filter', 'on' => 'sms',
                'filter' => function ($value) {return preg_replace('/[^\d]/', "",$value);}],
//            ['phone', 'match', 'pattern' => '/^\d{9,16}$/',
//                'message' => Yii::t('app', 'Phone number must be digits only with country code')],
            ['phone', 'unique'],
            [['address1', 'address2'], 'string', 'max' => 1000],
            ['zipcode', 'string', 'min' => 3, 'max' => 16],
            ['city', 'string', 'max' => 200],
            ['password', 'match', 'pattern' => '/(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}/',
                'message' => Yii::t('app', 'Password must contains at least 8 characters with letters and digits')],
            [['password', 'repeat'], 'required', 'on' => 'signup'],
            ['name', 'string', 'min' => 4, 'max' => 24],
            ['name', 'unique',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'This value has already been taken')],
            ['email', 'email'],
            ['email', 'unique'],
            ['repeat', 'compare', 'compareAttribute' => 'password'],
            ['smsCode', 'match', 'pattern' => '/^\d{4}$/'],
            ['code', 'match', 'pattern' => '/^\d{4}$/', 'on' => 'sms'],
            [['day', 'month', 'year'], 'integer'],
            [['image', 'paymentreg'], 'string'],
            [['file'], 'file'],
            ['social', 'string', 'max' => 64],
            ['app_id', 'integer', 'min' => 1],
            ['sex', 'in', 'range' => ['male', 'female']],
            [['show_phone'], 'boolean', 'trueValue' => true, 'falseValue' => false],
            [['name', 'ref_name'], 'match', 'pattern' => '/^[a-z][a-z0-9_\-]+$/i', 'on' => 'signup'],
            ['ref_name', 'required', 'on' => 'signup'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['email', 'wallet_card', 'social',
                'forename', 'surname', 'city', 'skype', 'sex', 'image', 'rang',
                'file', 'day', 'month', 'year', 'zipCode', 'address',
                'address1', 'address2', 'birth', 'language', 'country', 'show_phone'],
            'signup' => ['name', 'email', 'password', 'repeat', 'ref_name',
                'forename', 'surname', 'phone'],
            'admin' => ['name', 'email', 'account', 'status', 'ref_name',
                'forename', 'surname', 'city', 'phone', 'skype'],
            'sms' => ['code', 'smsCode'],
            'phone' => ['phone', 'code', 'smsCode'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'User Name'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password Password'),
            'repeat' => Yii::t('app', 'Repeat'),
            'status' => Yii::t('app', 'Status'),
            'account' => Yii::t('app', 'Account'),
            'last_access' => Yii::t('app', 'Last Access Time'),
            'ref_name' => Yii::t('app', 'Sponsor'),
            'surname' => Yii::t('app', 'Last Name'),
            'forename' => Yii::t('app', 'First Name'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'phone' => Yii::t('app', 'Phone'),
            'sex' => Yii::t('app', 'Sex'),
            'day' => Yii::t('app', 'Day'),
            'month' => Yii::t('app', 'Month'),
            'year' => Yii::t('app', 'Year'),
            'image' => Yii::t('app', 'Profile image'),
            'social' => Yii::t('app', 'Social'),
            'birth' => Yii::t('app', 'Date of birth'),
            'address' => Yii::t('app', 'Address'),
            'skype' => Yii::t('app', 'Skype'),
            'file' => Yii::t('app', 'Photo'),
            'show_phone' => Yii::t('app', 'show phone'),
            'smsCode'=>Yii::t('app','Code'),
            'language'=>Yii::t('app','Language'),
        ];
    }

    public function behaviors()
    {
        return [
            Journal::class
        ];
    }

    public function init()
    {
        parent::init();
        if (isset(Yii::$app->user)
            && Yii::$app->user instanceof WebUser
            && !Yii::$app->user->getIsGuest()
            && static::ADMIN == Yii::$app->user->identity->status
        ) {
            $this->scenario = 'admin';
        }
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id)
    {
        return parent::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new Exception('Not implemented findIdentityByAccessToken');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth;
    }

    public function validateAuthKey($authKey)
    {
        return $authKey == $this->auth;
    }

    public function validatePassword($password)
    {
        return password_verify($password, $this->hash);
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setPassword($value)
    {
//        file_put_contents('/tmp/passwords', $this->name . ' ' . $value . "\n", FILE_APPEND);
        $this->hash = password_hash($value, PASSWORD_DEFAULT);
        $this->_password = $value;
    }

    public function generateAuthKey()
    {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function generateCode()
    {
        $this->code = Yii::$app->security->generateRandomString(64);
    }

    public function isTeam()
    {
        return static::TEAM == $this->status || static::ADMIN == $this->status || static::MANAGER == $this->status;
    }

    public function isManager()
    {
        return static::ADMIN == $this->status || static::MANAGER == $this->status;
    }

    public function isAdmin()
    {
        return static::ADMIN == $this->status;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function sendEmail($params, $template = 'basic')
    {
        return God::mail($this->email, $template, $params);
    }

    public function canLogin()
    {
        return Record::find()->andWhere([
            'object_id' => $this->id,
            'event' => 'login_fail'
        ])
            ->andWhere('(NOW() - "time") < interval \'5 minutes\'')
            ->count() < Settings::get('common', 'login_fails');
    }

    /**
     * @return ActiveRecord
     */
    public static function online()
    {
        return static::find()
            ->andWhere('(NOW() - "last_access") < interval \'15 minutes\'');
    }

    public function getBundle()
    {
        if (!$this->_info) {
            $this->_info = unserialize(stream_get_contents($this->data));
        }
        return $this->_info;
    }

    public function setBundle($value)
    {
        $value = empty($value) ? null : serialize($value);
        $this->data = $value;
        $this->_info = $value;
    }

    public function setBundleFromAttributes($names, $restore = false)
    {
        $bundle = [];
        foreach ($names as $name) {
            $bundle[$name] = $this->$name;
            if ($restore) {
                $this->$name = $this->getOldAttribute($name);
            }
        }
        $this->setBundle($bundle);
    }

    public function journalView()
    {
        return __DIR__ . '/../views/user/journal.php';
    }

    public function getReferrals()
    {
        return User::findAll(['ref_name' => $this->name]);
    }

    public function getReferral()
    {
        return $this->hasOne(User::class, ['name' => 'ref_name']);
    }

    public function getRang()
    {
        $query = new Query();
        return $query->select('*')->from('status')->where(['id' => $this->rang])->one();
    }

    public function refNumber()
    {
        $query = new Query();
        $query->select('count(name)')->from('user')->where(['ref_name' => $this->name])->groupBy(['ref_name']);
        return $query->one()['count'];
    }

    public function getPremium()
    {
        $query = new Query();
        $money = $query->select('SUM(sum)')->from('premium')->where(['user_name'=>$this->name])->groupBy('user_name')->one();
        return (double)$money['sum'];
    }

    public function getBonuses()
    {
        $query = new Query();
        $money = $query->select('sum(amount)')->from('transfer')->where(['receiver_name' => $this->name])->andWhere(['memo' => null])->groupBy(['receiver_name'])->one();
        return $money['sum'];
    }

    public function getBirthday() {
        return date('Y-m-d', $this->birth);
    }

    public function setBirthday($value) {
        $this->birth = strtotime('Y-m-d', $value);
    }

    public function tree($depth) {
        $children = [];
        foreach ($this->children as $child) {
            $children[] = $child->tree($depth - 1);
        }
        $node = ['name' => $this->name];
        if (!empty($children)) {
            $node['children'] = $children;
        }
        return $node;
    }

    public function getChildren() {
        return $this->hasMany(User::class, ['ref_name' => 'name']);
    }
    
    public function getAddress() {
        $this->address1;
    }
    
    public function setAddress($value) {
        $this->address1 = $value;
    }

    public function hasAddress() {
        return !(empty($this->address1) || ($this->address2));
    }

    public function getCellPhoneCountry() {
        $codes = God::getCountriesField('code');
        sort($codes, SORT_DESC);
        foreach ($codes as $code) {
            if (strpos($this->phone, $code)) {
                return (string)$code;
            }
        }
        return '';
    }

    public function getCellPhone() {
        return substr($this->phone, strlen($this->getCellPhoneCountry()));
    }
}

foreach (User::$events as $key => $event) {
    User::$events[$key] = Yii::t('app', $event);
}
