<?php
use yii\helpers\Html;

?>
<!-- Business Card -->
<div class="user-business-card">
    <div class="title">
        <div class="sponsor-img" style="background: url(<?= $model->image ? $model->image : '/images/userfase.jpg' ?>)"></div>
        <div class="name"><?= $model->forename ?> <?= $model->surname ?></div>
        <?php if ($ext): ?>
            <div class="close" data-selector="#business-card-dialog">X</div>
        <?php endif ?>
    </div>
    <?php if($model->show_phone) {
        echo Html::tag('div', $model->phone, ['class' => 'phone']);
    } ?>
    <div class="city"><?= $model->city ?></div>
    <div class="skype"><?= $model->skype ?></div>
    <?php if ($ext): ?>
        <div class="email"><?= $model->email ?></div>
    <?php endif ?>
</div>
