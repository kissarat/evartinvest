<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 07.03.16
 * Time: 23:22
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

 $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

<?= $form->field($model,'file')->fileInput(); ?>

<?= Html::submitButton(Yii::t('app', 'Upload'),['class'=>'btn btn-success','id'=>'closeModal']) ?>
<?php ActiveForm::end() ?>

