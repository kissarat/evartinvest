<?php

use app\models\User;
use dosamigos\chartjs\ChartJs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$columns = [
    'name',
    'email:email',
    'last_access:datetime',
    [
        'attribute' => 'account',
        'format' => 'html',
        'value' => Html::tag('span', $model->account) . ' ' . ($model->account > 0 ? Html::a(Yii::t('app', 'Withdraw'),
                ['invoice/invoice/create', 'amount' => floor($model->account), 'scenario' => 'withdraw'],
                ['class' => 'btn btn-success btn-xs']) : '')
    ],
    'surname',
    'forename',
    'city',
    'phone',
    'skype',
    [
        'attribute' => 'ref_name',
        'format' => 'html',
        'value' => Html::a($model->ref_name, ['view', 'ref_name' => $model->ref_name])
    ]
];

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    $columns[] = [
        'attribute' => 'status',
        'value' => User::statuses()[$model->status]
    ];
}

foreach ($model->getAttributes() as $key => $value) {
    if (0 === strpos($key, 'wallet_') && $model->$key) {
        $columns[] = $key;
    }
}

if ($last_referrals > 0) {
    $referrals = $referrals . Html::tag('span', '+' . $last_referrals, [
            'class' => 'last',
            'title' => Yii::t('app', 'Last day updates')
        ]);
}

if ($last_visits > 0) {
    $visits = $visits . Html::tag('span', '+' . $last_visits, [
            'class' => 'last',
            'title' => Yii::t('app', 'Last day updates')
        ]);
}
?>
<div class="user-view">

<div class="user-info">
    <a style="display:none" href="<?= Url::to(['/user/update', 'name' => Yii::$app->user->identity->name]) ?>"
       class="editprofile"><?= Yii::t('app', 'Update') ?></a>

    <div class="info1">
        <?= Yii::t('app', 'Referral link followings') ?>:
        <br/>
        <?= Html::tag('span', $visits) ?>
    </div> 

    <div class="info1">
        <?= Yii::t('app', 'Referrals') ?>:
        <br/>
        <?= Html::tag('span', $referrals) ?>
    </div>
    <div class="info1">
        <?= Yii::t('app', 'Referral bonuses') ?>:
        <br/>
        <?= Html::tag('span', $model->getBonuses() . ' €') ?>
    </div>

    <?= ChartJs::widget([
        'type' => 'Line',
        'options' => [
            'height' => 449,
            'width' => 575
        ],
        'data' => [
            'labels' => ["January", "February", "March", "April", "May"],
            'datasets' => [
                [
                    'fillColor' => "rgba(220,220,220,0.5)",
                    'strokeColor' => "rgba(220,220,220,1)",
                    'pointColor' => "rgba(220,220,220,1)",
                    'pointStrokeColor' => "#fff",
                    'data' => [0, 0, 0, 0, 0]
                ],

            ]
        ]
    ]);
    ?>
</div>
    <h3 class="user-info-table">Обо мне</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $columns,
    ]) ?>

    <div class="clear"></div>

    <div class="info1">
        <?= Yii::t('app', 'Premium') ?>:
        <br/>
        <?= Html::tag('span', $model->getPremium() . ' €') ?>
    </div>
</div>
