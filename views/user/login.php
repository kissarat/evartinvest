<?php

use app\models\LoginForm;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Login');
if ('admin' == Yii::$app->layout) {
    $this->title = Yii::t('app', 'Admin Panel') . ': ' . $this->title;
}
?>
<h1><?= $this->title ?></h1>
<div class="user-login">
    <?php $form = ActiveForm::begin([
        'fieldConfig' => function(LoginForm $model, $attribute) {
            return [
                'template' => "{input}\n{hint}\n{error}",
                'inputOptions' => [
                    'placeholder' => $model->attributeLabels()[$attribute]
                ]
            ];
        }
    ]); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <?php if ('admin' == Yii::$app->layout): ?>
        <div>
            <?= Html::activeCheckbox($model, 'remember') ?>
            <span id="duration" style="display: none">
            <?= Html::dropDownList('select', $model->duration, [
                0 => Yii::t('app', 'other'),
                60 => Yii::t('app', 'hour'),
                60 * 24 => Yii::t('app', 'day'),
                60 * 24 * 7 => Yii::t('app', 'week'),
                60 * 24 * 30 => Yii::t('app', 'month'),
            ]) ?>
                <span id="duration_minutes" style="display: none">
                <?= Html::activeTextInput($model, 'duration') ?>
                <?= Yii::t('app', 'minutes') ?>
            </span>
        </span>
        </div>
    <?php endif ?>

    <div class="form-group">
        <?php
        if ('main' == Yii::$app->layout) {
            echo Html::a(Yii::t('app','Forgot password?'), ['request']);
            echo Html::a(Yii::t('app','SignUp'), ['signup']);
        }
        ?>
    </div>
<div class="clear"></div>
    <div class="form-group">
        <?= Html::submitButton($this->title,['class'=>'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div><!-- user-login -->
