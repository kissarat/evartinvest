<?php
/* @var $this yii\web\View */
/* @var $model app\models\User */

use app\models\Language;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="user-edit">
    <?php
    $form = ActiveForm::begin([
        'id' => 'update-user',
        'fieldConfig' => function (User $model, $attribute) {
            return [
                'template' => "{input}\n{hint}\n{error}",
                'inputOptions' => [
                    'placeholder' => $model->attributeLabels()[$attribute]
                ]
            ];
        }
    ]) ?>
    <?= $form->field($model, 'surname') ?>
    <?= $form->field($model, 'forename') ?>
    <?=
    $form->field($model, 'sex')->dropDownList([
        'male' => Yii::t('app', 'Male'),
        'female' => Yii::t('app', 'Female')
    ],
        [
            'prompt' => 'Select Sex'
        ]) ?>
    <div class="clear"></div>
    <div class="dataro">
        <?php
        $day = [];
        $month = [];
        $year = [];
        for ($i = 1; $i < 32; $i++) {
            $day[$i] = $i;
        }
        for ($i = 1; $i < 13; $i++) {
            $d = gregoriantojd($i, 1, 1970);
            $month[$i] = Yii::t('app', jdmonthname($d, 1));
        }
        for ($i = (date('Y', time()) - 18); $i > 1931; $i--) {
            $year[$i] = $i;
        }
        ?></div>
    <div class="clear"></div>
    <?= $form->field($model, 'year')->dropDownList($year, ['prompt' => Yii::t('app', 'Year')]) ?>
    <?= $form->field($model, 'month')->dropDownList($month, ['prompt' => Yii::t('app', 'Month')]) ?>
    <?= $form->field($model, 'day')->dropDownList($day, ['prompt' => Yii::t('app', 'Day')]) ?>

    <div class="clear"></div>
    <?= $form->field($model, 'language')->dropDownList(Language::getItems()) ?>

    <?php if (Yii::$app->controller->action->id == 'update'): ?>
        <?= Html::tag('label', " +" . $model->phone,
            ['class' => 'control-label']); ?>
        <?= Html::a(Yii::t('app', '&#9998;'),
            ['/user/number', 'user' => $model->name],
            ['class' => 'btn btn-primary edit-number']); ?>
        <?php
    else:
        echo $form->field($model, 'phone')->textInput();
    endif;
    ?>
    <div class="clear"></div>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'skype') ?>
    <div class="clear"></div>
    <?= $form->field($model, 'city') ?>
    <?= $form->field($model, 'show_phone')->checkbox() ?>

    <?php if ('admin' == $model->getScenario()): ?>
        <fieldset>
            <legend><?= Yii::t('app', 'Administrating') ?></legend>
            <?= $form->field($model, 'account'); ?>
            <?= $form->field($model, 'status')->dropDownList(User::statuses()); ?>
            <?= $form->field($model, 'ref_name'); ?>
        </fieldset>
        <?php
        //            ->widget(AutoComplete::widget(), [
//            'route' => '/user/complete'
//        ]);
    endif;

    ActiveForm::end()
    ?>

    <!-- Upload avatar -->
    <form action="/user/image"
          id="myId" method="post"
          enctype="multipart/form-data"
          class="dropzone">
    </form>

    <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update'), [
        'class' => 'submit',
        'data-form' => '#update-user'
    ]) ?>
</div>
