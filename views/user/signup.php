<?php
/* @var $this yii\web\View */
/* @var \app\models\User $model */

use yii\helpers\Html;
use yii\jui\Dialog;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Signup');

if ($model->ref_name):
    ?>
    <div class="user-signup">
        <?php Dialog::begin([
            'id' => 'confirm-sponsor-dialog',
            'clientOptions' => [
                'autoOpen' => false,
                'buttons' => [
                    [
                        'text' => Yii::t('app', 'Confirm'),
                        'click' => new JsExpression('(function() {confirmSponsor(true)})')
                    ],
                    [
                        'text' => Yii::t('app', 'Cancel'),
                        'click' => new JsExpression('(function() {confirmSponsor(false)})')
                    ]
                ]
            ]
        ]);
        Dialog::end() ?>

        <?= Html::tag('h1', Yii::t('app', 'Fill the form for registration')) ?>
        <?php $form = ActiveForm::begin([
            'fieldConfig' => function ($model, $attribute) {
                return [
                    'template' => "{input}\n{hint}\n{error}",
                    'inputOptions' => [
                        'placeholder' => $model->attributeLabels()[$attribute]
                    ]
                ];
            }
        ]);
        ?>

        <?= $form->field($model, 'ref_name', ['template' => "{label}\n{input}\n{hint}\n{error}"]) ?>
        <div class="clear"></div>
        <hr>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email') ?>
        <div class="clear"></div>
        <?= $form->field($model, 'surname') ?>
        <?= $form->field($model, 'forename') ?>
        <div class="clear"></div>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'repeat')->passwordInput() ?>
        <div class="clear"></div>
        <?= $form->field($model, 'phone') ?>
        <div class="clear"></div>

        <div class="form-group politics">
            <input type="checkbox" id="accept-agreement"/>
            <label for="accept-agreement"></label>
            <p>
                <?= Yii::t('app', 'I accept') ?>
                <?= Html::a(Yii::t('app', 'Term of services'), '/page/rules', [
                    'target' => '_blank'
                ]) ?>
            </p>
            <?= Html::tag('div', Yii::t('app', 'You must accept Term of services'), [
                'data-error' => 'accept-agreement',
                'style' => 'display: none',
                'class' => 'error'
            ]) ?>
        </div>
        <div class="clear"></div>
        <div class="form-group">
            <?= Html::submitButton('Signup', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <?php
else:
    echo Html::tag('div', Yii::t('app', "You can register without sponsor's link"), ['class' => 'sponsor-link-error']);
endif;
