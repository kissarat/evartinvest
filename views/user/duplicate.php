<?php
/* @var $this yii\web\View */
/* @var $model app\models\User */

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="user-duplicate">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password') ?>
    <?= $form->field($model, 'surname') ?>
    <?= $form->field($model, 'forename') ?>
    <?=
    $form->field($model, 'sex')->dropDownList([
        '0' => Yii::t('app', 'Male'),
        '1' => Yii::t('app', 'Female')
    ],
        [
            'prompt' => 'Select Sex'
        ]) ?>
    <?= Html::button(Yii::t('app', 'Duplicate')) ?>
    <?php ActiveForm::end() ?>
</div>
