<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="user-number">
    <?php $form = ActiveForm::begin() ?>
    <?= Html::tag('h2', Yii::t('app', 'New number')) ?>
    <?= $form->field($model, 'phone', [
        'template' => '<div class="input-group"><span class="input-group-addon">+</span>{input}</div>',
    ]) ?>
    <?= Html::submitButton(Yii::t('app', 'Confirm')) ?>
    <?php ActiveForm::end() ?>
</div>

