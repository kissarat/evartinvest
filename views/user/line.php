<?php

use yii\helpers\Html;
use yii\helpers\Url;

echo Html::tag('h1', Yii::t('app', 'Referral network'));

echo '<div class="first-ref"> <div class="lines">';
$line = null;
if ('line' == Yii::$app->controller->action->id) {
    $line = $number;
}

$lines_count = $me->getRang()['lines'] + 1;
for ($i = 1; $i < $lines_count; $i++) {
    $options = [
        'class' => 'line'
    ];
    if ($i == $line) {
        $options['id'] = 'active';
    }
    $anchor = Html::a($i, ['/user/line',
        'name' => $me,
        'number' => $i
    ]);
    echo Html::tag('div', $anchor, $options);
}

echo "</div></div> <div class=\"second-ref\"> <div class=\"user2\">" . '<div class="myava" style="background: url(' . ($user->image ? $user->image : "/images/userfase.jpg") . ')"><div class="greenrefonline"></div></div>
<div class="refinfo"> 
<p>' . $user->forename . " " . $user->surname . "</p><p>" . $user->getRang()['title'];
echo '</p></div> <div class="infostrelka"> <img src="/images/ref-strela.png" alt=""/> </div></div> </div> <div class="clear"></div> <div class="referrals">';
foreach ($models as $item) {

    echo "<a href=\"" . Url::to(['referral', 'user' => $item['name']]) . "\"><div class=\"referral2\">"; ?>

    <?= '<div class="refimg" style="background: url(' . ($item['image'] ? $item['image'] : "/images/userfase.jpg") . ');"><div class="greenrefonline"></div></div>' ?>
    <div class="my-referal-info">
        <p><?= $item['forename'] . " " ?>
            <?= $item['surname'] ?></p>

        <p><?= $item['title'] ?></p>
        <?php if (Yii::$app->controller->action->id == 'referral') {
            echo Yii::t('app', 'Level') . " " . $item['level'] . "<br>";
        }?>
        <p> <?= Yii::t('app', 'Referrals') . " " . $item['count'] ?></p></div>
    </div></a>
<?php
}
?>
</div>
 