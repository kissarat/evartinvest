<?php
use yii\helpers\Html;
?>
<div class="user-visits">
    <?= Yii::t('app', 'Your sponsor URL visits') ?>
    <table>
        <thead>
        <tr>
            <th>Time</th>
            <th>URL</th>
            <th>User</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($visits as $visit):
            $visit['time'] = preg_replace('/\:\d+\.\d+$/', '', $visit['time']);
            ?>
        <tr>
            <td><?= $visit['time'] ?></td>
            <td><?= $visit['url'] ? Html::a($visit['url'], $visit['url']) : '' ?></td>
            <td><?= $visit['user_name'] ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div> 