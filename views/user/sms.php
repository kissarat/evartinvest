<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (isset($message)) {
    echo Html::tag('div', $message, ['class' => 'alert alert-danger', 'role' => 'alert']);
}
?>
<div class="user-sms">
    <h1><?= Yii::t('app', 'Your confirm code sent to +' . $model->phone) ?></h1>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'smsCode')->textInput() ?>
    <?= Html::submitButton(Yii::t('app', 'Confirm')) ?>
    <?php ActiveForm::end() ?>
</div>
<?php
