<?php

use app\helpers\MainAsset;
use app\models\Language;
use app\models\Settings;
use app\widgets\Alert;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$user_name = '';
if ($login) {
    $user_name = Yii::$app->user->identity->name;
}
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    $this->head();
    if (!Yii::$app->user->getIsGuest()) {
        $user = Yii::$app->user->identity->name;
        echo Html::script("var user = '$user'");
    }
    ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $route) ?>" data-user="<?= $user_name ?>">
<?php
$this->beginBody();

if (empty($login)) {
    echo Html::dropDownList('language', Yii::$app->language, Language::getItems(), [
       'id' => 'choose-language' 
    ]);
}

if ('cabinet' != Yii::$app->layout) {
    echo Alert::widget();
}
?>

<?= $content ?>

<?php $this->endBody() ?>

<!-- External widgets -->

<?php if (!IS_LOCALHOST && Settings::get('common', 'google')): ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?= Settings::get('common', 'google') ?>', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
<?php endif ?>

<?php if (!IS_LOCALHOST && Settings::get('common', 'yandex')):
    $yandex = Settings::get('common', 'yandex');
    ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter<?= $yandex ?> = new Ya.Metrika({
                        id: <?= $yandex ?>,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });
            var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
                n.parentNode.insertBefore(s, n);
            };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");</script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/<?= $yandex ?>"
                  style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript><!-- /Yandex.Metrika counter -->
<?php endif ?>

<?php if (!IS_LOCALHOST && Settings::get('common', 'heart')): ?>
    <script>
        (function () {
            var widget_id = <?= Settings::get('common', 'heart') ?>;
            _shcp = [{widget_id: widget_id}];
            var lang = (navigator.language || navigator.systemLanguage
            || navigator.userLanguage || "en")
                .substr(0, 2).toLowerCase();
            var url = "widget.siteheart.com/widget/sh/" + widget_id + "/" + lang + "/widget.js";
            var hcc = document.createElement("script");
            hcc.type = "text/javascript";
            hcc.async = true;
            hcc.src = ("https:" == document.location.protocol ? "https" : "http")
                + "://" + url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
    </script>
<?php endif ?>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment-with-locales.min.js"></script>-->
<!--<script src="https://raw.githubusercontent.com/moment/moment-timezone/eda36cb2c5ed62b7082593c3aeaff9b94204aa1a/moment-timezone.js"></script>-->
</body>
</html>
<?php $this->endPage() ?>
