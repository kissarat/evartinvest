<?php
use app\helpers\MainAsset;
use app\models\User;
use app\widgets\Alert;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;

MainAsset::register($this);

/** @var User $user */
$user = Yii::$app->user->identity;
if ($user->isManager() && isset($_GET['name'])) {
    $user = User::findOne(['name' => $_GET['name']]);
}
$sponsor = $user->referral;
if ($sponsor) {
    $sponsor_image = $sponsor->image;
    if (!$sponsor_image) {
        $sponsor_image = "/images/userfase.jpg";
    }
}

$user_name = isset($_GET['name']) ? $_GET['name'] : $user->name;

$this->beginContent('@app/views/layouts/main.php');
?>
    <div id="cabinet" data-user="<?= $user_name ?>">
        <?php if ($sponsor): ?>
            <div class="dialog" id="business-card-dialog" style="display: none">
                <?php
                echo Yii::$app->view->render('@app/views/user/business-card', [
                    'model' => $user->referral,
                    'ext' => true
                ]);
                ?>
            </div>
        <?php endif; ?>
        <div class="menu-toogle">
            <img src="/images/menu-view.png" alt=""/>
        </div>

        <div class="menu-toogle2">
            <img src="/images/menu-view2.png" alt=""/>
        </div>

        <div class="panel-bg"></div>
        <div class="head">
            <div class="container">
                <div class="avatar">
                    <div class="ava-fon">
                        <div class="photo"
                             style="background: url(<?= $user->image ? '' . $user->image . '' : '/images/userfase.jpg' ?>)"></div>
                    </div>
                </div>
                <div class="header">
                    <div class="left-header">
                        <div class="namelogin"><?= $user->forename . " " . $user->surname ?></div>
                        <div
                            class="status"><?= Yii::t('app', 'Status') . " " . Yii::t('app', $user->getRang()['title']) ?></div>
                        <div class="reflink">
                            <?= chornij\zeroclipboard\Button::widget([
                                'label' => Html::input('type', 'copy-selector',
                                    Url::to(['/user/signup', 'ref' => $user->name], 'http'), ['class' => 'copy-selector']),
                                'encodeLabel' => false,
                                'text' => "$('input.copy-selector').attr('value')",
                            ]) ?>
                        </div>
                        <div class="statuses">
                            <?php
                            $query = new Query();
                            $statuses = $query->select('*')->from('status')->all();
                            foreach ($statuses as $status) {
                                $options = ['class' => 'status'];
                                $status_content = Html::tag('span', $status['title']);
                                if ($user->rang == $status['id']) {
                                    $options['class'] .= ' active';
                                    $status_content .= ' ' . Html::img('/images/status-active.png');
                                }
                                echo Html::tag('div', $status_content, $options);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="right-header">
                        <div class="info-balance">
                            <?= Html::tag('span', Yii::t('app', 'Your account')) ?><br/>
                            <?= Html::tag('span', round($user->account, 0), ['class' => 'account']) ?>
                        </div>
                        <div class="buttons">
                            <div class="krug"> <?= Html::a(Yii::t('app', 'Pay'), ['/invoice/invoice/pay']) ?></div>
                            <div
                                class="krug"><?= Html::a(Yii::t('app', 'Withdraw'), ['/invoice/invoice/withdraw', 'id' => $user->name]) ?></div>
                        </div>

                        <div class="clear"></div>
                        <div class="podderjka-skype">
                            <a href="https://join.skype.com/ohM6uVX0uhBS">Онлайн-поддержка в Skype</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <?= Alert::widget(); ?>

            <div class="panel">


                <div style="clear: both"></div>
                <div class="menu">
<!--                    <div class="info-menu">--><?//= Yii::t('app', 'Profile menu') ?><!-- <img src="/images/strelka.jpg" alt=""/></div>-->
                    <?=
                    Menu::widget([
                        'items' => [
                            ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/view', 'name' => $user_name]],
                            ['label' => Yii::t('app', 'Edit'), 'url' => ['/user/update', 'name' => $user_name]],
                            ['label' => Yii::t('app', 'Structure'), 'url' => ['/user/line', 'name' => $user_name, 'number' => 1]],
                            ['label' => Yii::t('app', 'History'), 'url' => ['/user/history', 'name' => $user_name], [
                                'class' => 'dev'
                            ]],
                            ['label' => Yii::t('app', 'Deposits'), 'url' => ['/user/plans', 'name' => $user_name]],
                            ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/feedback/create', 'name' => $user_name], [
                                'class' => 'dev'
                            ]],
                            ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout', 'name' => $user_name]],
                        ]
                    ]); ?>
                    <?php if ($sponsor): ?>
                        <div class="sponsor">
                            <?= Html::tag('div', '', [
                                'class' => 'avasponsor',
                                'style' => "background: url($sponsor_image)"
                            ]) ?>
                            <?= Html::tag('span', Yii::t('app', 'Sponsor') . ':') ?>
                            <?= Html::tag('div', "$sponsor->forename $sponsor->surname", ['class' => 'name']) ?>

                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="main">
                <!-- Cabinet begin -->
                <!-- Cabinet begin -->
                <!-- Cabinet begin -->

                <?= $content ?>

                <!-- Cabinet end -->
                <!-- Cabinet end -->
                <!-- Cabinet end -->
            </div>
            <div class="clear"></div>
            <div class="footer2">
                <div class="fleft"><p>© 2015- <?= date('Y') ?> Evart Corporation. All rights reserved.</p></div>
                <div class="fright"><p><img src="/images/mastercard.png" alt=""/> Part of the <a href="/">Evart</a>
                        corporation.</p></div>
            </div>
        </div>
    </div>
<?php
$this->endContent();
