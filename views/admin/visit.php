<?php

use app\models\Visit;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Visits');
if ($user) {
    $this->title .= ': ' . $user;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visits'), 'url' => ['visit']];
    $this->params['breadcrumbs'][] = $user;
}

echo Html::tag('h1', $this->title);
echo Html::tag('div', implode("\n", [
    Html::a(Yii::t('app', 'Browsers'), ['user-agents'], ['class' => 'btn btn-primary'])
]),
    ['class' => 'form-group']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'value' => function(Visit $model) {
                return date('Y-m-d h:i:s', round($model->id / 1000000));
            }
        ],
        /*
        [
            'attribute' => 'spend',
            'format' => 'html',
            'value' => function(Visit $model) {
                $spend = $model->spend / 1000;
                return $spend > 10 ? round($spend) : $spend;
            }
        ],
        */
        [
            'attribute' => 'user_name',
            'format' => 'html',
            'value' => function(Visit $model) {
                return $model->user_name ? Html::a($model->user_name, ['visit', 'user' => $model->user_name]) : null;
            }
        ],
        [
            'attribute' => 'path',
            'format' => 'html',
            'value' => function(Visit $model) {
                return Html::a($model->path, str_replace('vg6wa4.', '', Url::to([$model->path], true)));
            }
        ],
        'ip',
        [
            'attribute' => 'agent',
            'format' => 'html',
            'contentOptions' => [
                'class' => 'small'
            ],
            'value' => function(Visit $model) {
                return Visit::getStrongUserAgent($model->agent);
            }
        ]
    ]
]);
