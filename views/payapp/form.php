<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 24.04.16
 * Time: 14:08
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->field($model, 'personCode')->textInput(['placeholder' => '########-#####']) ?>
<?= $form->field($model, 'natioonality')->dropDownList($countries, ['prompt' => 'Select from list']) ?>
<?= $form->field($model, 'address') ?>
<?= $form->field($model, 'zipCode')->textInput(); ?>
<?= $form->field($model, 'city')->textInput() ?>
<?= $form->field($model, 'country')->dropDownList($countries, ['prompt' => 'Select from list']) ?>
<?= $form->field($model, 'contact')->checkbox() ?>
<?= $form->field($model, 'passportType')->dropDownList([
    'ID' => Yii::t('app', 'ID card'),
    'PP' => Yii::t('app', 'passport')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'idNumber') ?>
<?= $form->field($model, 'activityAndIncome')->dropDownList([
    'PE' => Yii::t('app', 'Paid employee'),
    'EN' => Yii::t('app', 'HouseOwner/HouseWife'),
    'SE' => Yii::t('app', 'Self-employed'),
    'RE' => Yii::t('app', 'Retired'),
    'ST' => Yii::t('app', 'Student'),
    'UE' => Yii::t('app', 'Unemployed'),
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'companyName') ?>
<?= $form->field($model, 'businessType')->dropDownList([
    'ED' => Yii::t('app', 'Education'),
    'MM' => Yii::t('app', 'Manufacturing, mining,extraction of raw materials'),
    'EG' => Yii::t('app', 'Electricity, gas, and heating,ventilation systems'),
    'WS' => Yii::t('app', 'Water supply and waste water treatment systems and recycling'),
    'CI' => Yii::t('app', 'Construction industry'),
    'RW' => Yii::t('app', 'Retail and wholesale trade'),
    'VR' => Yii::t('app', 'Vehicle repair'),
    'TS' => Yii::t('app', 'Transportation and storage of goods'),
    'HC' => Yii::t('app', 'Hotels and catering'),
    'TT' => Yii::t('app', 'Telecommunications, IT services'),
    'SL' => Yii::t('app', 'Sale / lease of property'),
    'FI' => Yii::t('app', 'Financial services and insurance services'),
    'HS' => Yii::t('app', 'Health and social care'),
    'AR' => Yii::t('app', 'Arts, recreation and entertainment'),
    'AS' => Yii::t('app', 'Administrative Services'),
    'PA' => Yii::t('app', 'Public administration and the army, social insurance'),
    'ST' => Yii::t('app', 'Scientific and Technical Services'),
    'OT' => Yii::t('app', 'Other')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'businessTypeOther') ?>
<?= $form->field($model, 'companyWebPage') ?>
<?= $form->field($model, 'accountUsage')->dropDownList([
    'PERSONAL' => Yii::t('app', 'Personal usage'),
    'BUSINESS' => Yii::t('app', 'Business'),
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'openingPurpose')->dropDownList([
    'SA' => Yii::t('app', 'salary'),
    'CR' => Yii::t('app', 'Income from customer referral'),
    'SG' => Yii::t('app', 'Income from the sale of gold'),
    'IC' => Yii::t('app', 'income from business activities'),
    'OT' => Yii::t('app', 'other')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'openingPurposeOther') ?>
<?= $form->field($model, 'monthlyTurnOver')->dropDownList([
    '10' => Yii::t('app', '1 EUR-5 000 EUR'),
    '11' => Yii::t('app', '5 001 EUR-15 000 EUR'),
    '20' => Yii::t('app', '15 000 EUR-100 000 EUR'),
    '30' => Yii::t('app', '100 000 EUR-200 000 EUR'),
    '40' => Yii::t('app', 'more than 200 000 EUR')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'firstTopUp')->dropDownList([
    'MY' => Yii::t('app', 'my personal account'),
    'AA' => Yii::t('app', 'from account of another person'),
    'CC' => Yii::t('app', 'with my CreditCard'),
    'CA' => Yii::t('app', 'with CreditCard of another person'),
    'WM' => Yii::t('app', 'via WesternUnion by myself'),
    'WA' => Yii::t('app', 'via WesternUnion by another person')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'firtsTopUpConnection')->dropDownList([
    'FA' => Yii::t('app', 'Family'),
    'FR' => Yii::t('app', 'Friend'),
    'EM' => Yii::t('app', 'Employer'),
    'CU' => Yii::t('app', 'Customer'),
    'GT' => Yii::t('app', 'Gold Trader')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'otherTopUp')->dropDownList([
    'MY' => Yii::t('app', 'my personal account'),
    'AA' => Yii::t('app', 'from account of another person'),
    'CC' => Yii::t('app', 'with my CreditCard'),
    'CA' => Yii::t('app', 'with CreditCard of another person'),
    'WM' => Yii::t('app', 'via WesternUnion by myself'),
    'WA' => Yii::t('app', 'via WesternUnion by another person')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'otherTopUpConnection')->dropDownList([
    'FA' => Yii::t('app', 'Family'),
    'FR' => Yii::t('app', 'Friend'),
    'EM' => Yii::t('app', 'Employer'),
    'CU' => Yii::t('app', 'Customer'),
    'GT' => Yii::t('app', 'Gold Trader')
],
    ['prompt' => 'Choose from list']) ?>
<?= $form->field($model, 'inLatvia')->checkbox() ?>
<?= $form->field($model, 'politican')->checkbox() ?>
<?= $form->field($model, 'deliveryInfo')->dropDownList([
    'BP' => Yii::t('app', 'By post'),
    'BR' => Yii::t('app', 'By registered mail'),
    'BC' => Yii::t('app', 'By courier'),
    'RB' => Yii::t('app', 'Receive in office')
],
    ['prompt' => 'Choose from list']) ?>

<?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update')) ?>
<?php ActiveForm::end() ?>