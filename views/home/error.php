<?php

use yii\helpers\Html;

/* @var $exception \yii\console\Exception */

if (isset($code)):
    $this->title = $name;

    $lines = [
        Html::tag('h1', Yii::t('app', $this->title)),
        Html::tag('div', $exception->getMessage(), ['class' => 'message'])
    ];
    echo Html::tag('div', implode("\n", $lines), [
        'class' => 'home-error',
        'data-code' => $code
    ]);
else:
?>
<div class="home-error">
    <h1>Error</h1>
    <?= Html::tag('div', $exception->getMessage()) ?>
</div>
<?php
endif;
