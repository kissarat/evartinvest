#!/bin/bash
# vagrant provision

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y libapache2-mod-php5 postgresql php5-pgsql \
  php5-cli php5-readline php5-xdebug php5-apcu php5-dev php-pear \
  php5-curl php5-gd vim htop
